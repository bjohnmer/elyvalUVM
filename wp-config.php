<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_WP_C1');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$@H=nnlQG.^PE6H0=Qi>j`@y{/?W<E8mtBJTI#MOLk^hh/v6jR,+uIzvxQ:~P-[h');
define('SECURE_AUTH_KEY',  '}3oM4-pNOo*12lZ@q-jaP-`r?k[$rskt.UwlzYEQ-Yv&6k=mnjue[8sVyWrk2!|Z');
define('LOGGED_IN_KEY',    '4opI}-Tei~<At12@+VPC:Tg #nQ]roCMb-MKwB=|mr;_4}*[y0GtRtstb_])4p&E');
define('NONCE_KEY',        '>-]t{$=3n9$D-Z~,@xS9I6/b@*^9A3MnqRvDEW&]M@n=%~X+.|y9Z7J:B1GLeN[+');
define('AUTH_SALT',        'h>G 0_a%3;/:T[->s:?16mGahQ1t rs>&{|G{GQB+,#jxR/{%dR qpHL_w(sVIyK');
define('SECURE_AUTH_SALT', '(2^U2>,|/5ojjz`1|:roIIRV|CpxCXb|Meci7a($<x)F|j[+k[#fMz_D1az,}?;w');
define('LOGGED_IN_SALT',   'SNOhs2`Ut`0`bX#T%}}Vp?Q4/jt?qrD$PJ{jO|1WB-;EZj8sK49dEE|=DDs-uAg1');
define('NONCE_SALT',       'o=VHOdj@#Vd@ba:I&;-6]2EJ*fqm|`PxzOjM0=M5RWhMd C Ib;W,WPMONLX)(++');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/* Multisite */
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);


/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up Domain Mapping plugin **/
define('SUNRISE', 'on');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
