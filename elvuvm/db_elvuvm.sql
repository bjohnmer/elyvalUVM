-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 19, 2013 at 04:55 PM
-- Server version: 5.5.29
-- PHP Version: 5.3.10-1ubuntu3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_elvuvm`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_alumnos`
--

CREATE TABLE IF NOT EXISTS `tbl_alumnos` (
  `alumno_id` int(11) NOT NULL AUTO_INCREMENT,
  `alumno_cedula` varchar(12) NOT NULL,
  `alumno_nombres` varchar(75) DEFAULT NULL,
  `alumno_apellidos` varchar(75) DEFAULT NULL,
  `alumno_direccion` text,
  `alumno_telf` varchar(15) DEFAULT NULL,
  `alumno_email` varchar(75) DEFAULT NULL,
  `alumno_semestre` int(11) DEFAULT NULL,
  `carrera_id` int(11) NOT NULL,
  `alumno_disponibilidad` text,
  `alumno_estatus` enum('Asignado','No Asignado') NOT NULL DEFAULT 'No Asignado',
  `alumno_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`alumno_id`),
  KEY `carrera_id` (`carrera_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_alumnos`
--

INSERT INTO `tbl_alumnos` (`alumno_id`, `alumno_cedula`, `alumno_nombres`, `alumno_apellidos`, `alumno_direccion`, `alumno_telf`, `alumno_email`, `alumno_semestre`, `carrera_id`, `alumno_disponibilidad`, `alumno_estatus`, `alumno_time`) VALUES
(1, '123456', 'JULIAN', 'MATHEUS', 'VALERA', '1763572371', 'ALUMNO@HOTMAIL.COM', 5, 1, 'JHA DKAHJ DKJHAKJD HKJAJ HDKHAKHD KAHDHAHD KJAHKJDKJHA DKJH AKJH DKHA K', 'Asignado', '2013-03-19 20:44:46'),
(5, '43434343', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'No Asignado', '2013-03-18 17:11:49');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_carreras`
--

CREATE TABLE IF NOT EXISTS `tbl_carreras` (
  `carrera_id` int(11) NOT NULL AUTO_INCREMENT,
  `carrera_codigo` varchar(5) NOT NULL,
  `carrera_nombre` varchar(50) NOT NULL,
  `carrera_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`carrera_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_carreras`
--

INSERT INTO `tbl_carreras` (`carrera_id`, `carrera_codigo`, `carrera_nombre`, `carrera_time`) VALUES
(1, 'IS', 'INGENIERÍA DE COMPUTACIÓN', '2013-02-27 19:16:56'),
(3, 'II', 'INGENIERÍA INDUSTRIAL', '2013-02-27 19:20:12'),
(4, 'AD', 'ADMINISTRACIÓN DE EMPRESAS', '2013-03-04 21:20:49'),
(5, 'DE', 'DERECHO', '2013-03-04 21:21:15'),
(6, 'CP', 'CIENCIAS POLÍTICAS', '2013-03-04 21:21:29'),
(7, 'PU', 'CONTADURÍA PÚBLICA', '2013-03-04 21:21:49');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ci_sessions`
--

CREATE TABLE IF NOT EXISTS `tbl_ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_proyectos`
--

CREATE TABLE IF NOT EXISTS `tbl_proyectos` (
  `proyecto_id` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto_codigo` varchar(10) NOT NULL,
  `proyecto_descripcion` varchar(150) NOT NULL,
  `proyecto_ubicacion` text NOT NULL,
  `proyecto_archivo` varchar(255) DEFAULT NULL,
  `proyecto_limite` int(11) NOT NULL DEFAULT '0',
  `proyecto_asignados` int(11) NOT NULL DEFAULT '0',
  `proyecto_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`proyecto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_proyectos`
--

INSERT INTO `tbl_proyectos` (`proyecto_id`, `proyecto_codigo`, `proyecto_descripcion`, `proyecto_ubicacion`, `proyecto_archivo`, `proyecto_limite`, `proyecto_asignados`, `proyecto_time`) VALUES
(1, '125', 'PROYECTO 1 - LIMPIAR Y PINTAR CANCHA', 'Plata IV', '', 15, 0, '2013-03-19 21:24:45'),
(2, '43234', 'LIMPIAR CARRETERA DE TIERRA DE XXXXXXXXXX', 'Pampán', '', 15, 1, '2013-03-19 21:24:43');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_trabajos`
--

CREATE TABLE IF NOT EXISTS `tbl_trabajos` (
  `trabajo_id` int(11) NOT NULL AUTO_INCREMENT,
  `alumno_id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `trabajo_status` enum('Abierto','Aprobado','Reprobado','Cerrado') NOT NULL DEFAULT 'Abierto',
  `trabajo_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `trabajo_fi` date NOT NULL,
  `trabajo_fc` date NOT NULL,
  PRIMARY KEY (`trabajo_id`),
  KEY `trabajo_alumno` (`alumno_id`,`proyecto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `tbl_trabajos`
--

INSERT INTO `tbl_trabajos` (`trabajo_id`, `alumno_id`, `proyecto_id`, `trabajo_status`, `trabajo_time`, `trabajo_fi`, `trabajo_fc`) VALUES
(60, 1, 2, 'Abierto', '2013-03-19 20:35:15', '2013-03-19', '2014-03-19');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_usuario`
--

CREATE TABLE IF NOT EXISTS `tbl_usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `cedula_usuario` varchar(12) NOT NULL,
  `nombre_usuario` varchar(50) DEFAULT NULL,
  `tipo_usuario` enum('Administrador','Supervisor','Usuario') NOT NULL DEFAULT 'Usuario',
  `login` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `password_confirmacion` varchar(50) NOT NULL,
  `fregistro_usuario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_usuario`
--

INSERT INTO `tbl_usuario` (`id_usuario`, `cedula_usuario`, `nombre_usuario`, `tipo_usuario`, `login`, `password`, `password_confirmacion`, `fregistro_usuario`) VALUES
(1, '17', 'CÉSAR ALFONSO', 'Administrador', '17', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', '2012-07-23 14:49:26'),
(3, '123456', 'JULIAN MATHEUS', 'Usuario', '123456', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', '2012-07-27 15:25:00'),
(5, '43434343', 'USUARIO SUPERVISOR', 'Supervisor', '43434343', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', '2013-03-18 17:11:49');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
