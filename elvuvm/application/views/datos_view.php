<?php $this->load->view('header_view');?>

<div id="body">
	<h1 class="centrado">Escuela de Liderazgo y Valores UVM</h1>
	<p>
		<h3 class="centrado">Datos del Usuario</h3>
		<p>
			<?php if (validation_errors()): ?>
				<div class="alert alert-error">
		          	<button data-dismiss="alert" class="close" type="button">×</button>
		          	<strong>¡ERROR!</strong> 
					<p>
		          		<?=validation_errors()?>
					</p>
		        </div>
		    <?php endif ?>

			<?php if (!empty($mensaje)): ?>
				<div class="alert alert-info">
		          	<button data-dismiss="alert" class="close" type="button">×</button>
		          	<strong>INFORMACIÓN</strong> 
					<p>
		          		<?=$mensaje?>
					</p>
		        </div>
		    <?php endif ?>
		</p>
		<p>
			<form class="form-horizontal" action="<?=base_url()?>datos/edit" method="post">
					<div class="control-group">
					    <label class="control-label" for="cedula_usuario">Cédula</label>
					    <div class="controls">
					    	<input type="text" id="cedula_usuario" name="cedula_usuario" value="<?=$usuario[0]->cedula_usuario?>" >
					    </div>
				    </div>
				    <div class="control-group">
					    <label class="control-label" for="nombre_usuario">Nombres</label>
					    <div class="controls">
					    	<input type="text" id="nombre_usuario" name="nombre_usuario" value="<?=$usuario[0]->nombre_usuario?>" >
					    </div>
				    </div>
					<div class="control-group">
					    <label class="control-label" for="password">Password</label>
					    <div class="controls">
					    	<input type="password" id="password" name="password" value="<?=set_value('password')?>">
					    </div>
				    </div>
				    <div class="control-group">
					    <label class="control-label" for="password">Confirmar Password</label>
					    <div class="controls">
					    	<input type="password" id="password_confirmacion" name="password_confirmacion" value="<?=set_value('password_confirmacion')?>">
					    </div>
				    </div>
				    <div class="control-group">
					    <button id="apuntarse" class="btn btn-small btn-success" >
							<i class="icon-user icon-white"></i> 
							Modificar Datos
						</button>
				    </div>
			</form>
		</p>
	</p>
</div>

<?php $this->load->view('footer_view');?>