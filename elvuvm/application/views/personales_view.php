<?php $this->load->view('header_view');?>

<div id="body">
	<h1 class="centrado">Escuela de Liderazgo y Valores UVM</h1>
	<p>
		<h3 class="centrado">Datos del Usuario</h3>
		<p>
			<?php if (validation_errors()): ?>
				<div class="alert alert-error">
		          	<button data-dismiss="alert" class="close" type="button">×</button>
		          	<strong>¡ERROR!</strong> 
					<p>
		          		<?=validation_errors()?>
					</p>
		        </div>
		    <?php endif ?>

			<?php if (!empty($mensaje)): ?>
				<div class="alert alert-info">
		          	<button data-dismiss="alert" class="close" type="button">×</button>
		          	<strong>INFORMACIÓN</strong> 
					<p>
		          		<?=$mensaje?>
					</p>
		        </div>
		    <?php endif ?>
		</p>
		<p>
			<form class="form-horizontal" action="<?=base_url()?>personales/edit" method="post">
					<div class="control-group">
					    <label class="control-label" for="alumno_cedula">Cédula</label>
					    <div class="controls">
					    	<input type="text" id="alumno_cedula" name="alumno_cedula" value="<?=$alumno[0]->alumno_cedula?>" >
					    </div>
				    </div>
				    <div class="control-group">
					    <label class="control-label" for="alumno_nombres">Nombres</label>
					    <div class="controls">
					    	<input type="text" id="alumno_nombres" name="alumno_nombres" value="<?=$alumno[0]->alumno_nombres?>" >
					    </div>
				    </div>
				    <div class="control-group">
					    <label class="control-label" for="alumno_apellidos">Apellidos</label>
					    <div class="controls">
					    	<input type="text" id="alumno_apellidos" name="alumno_apellidos" value="<?=$alumno[0]->alumno_apellidos?>" >
					    </div>
				    </div>
				    <div class="control-group">
					    <label class="control-label" for="alumno_direccion">Dirección</label>
					    <div class="controls">
					    	<textarea type="text" id="alumno_direccion" name="alumno_direccion" ><?=$alumno[0]->alumno_direccion?></textarea>
					    </div>
				    </div>
				    <div class="control-group">
					    <label class="control-label" for="alumno_telf">Teléfono</label>
					    <div class="controls">
					    	<input type="text" id="alumno_telf" name="alumno_telf" value="<?=$alumno[0]->alumno_telf?>" >
					    </div>
				    </div>
				    <div class="control-group">
					    <label class="control-label" for="alumno_email">Correo Electrónico</label>
					    <div class="controls">
					    	<input type="text" id="alumno_email" name="alumno_email" value="<?=$alumno[0]->alumno_email?>" >
					    </div>
				    </div>
				    <div class="control-group">
					    <label class="control-label" for="alumno_semestre">Semestre</label>
					    <div class="controls">
					    	<input type="text" id="alumno_semestre" name="alumno_semestre" value="<?=$alumno[0]->alumno_semestre?>" >
					    </div>
				    </div>
				    <div class="control-group">
					    <label class="control-label" for="carrera_id">Carrera</label>
					    <div class="controls">
					    	<select name="carrera_id" id="carrera_id" class="span3">
					    		<?php if (!empty($carreras)): ?>
									<?php foreach ($carreras as $carrera): ?>
					    					<option value="<?=$carrera->carrera_id?>" <?php if ($carrera->carrera_id == $alumno[0]->carrera_id): ?>selected<?php endif ?>><?=$carrera->carrera_nombre?></option>
									<?php endforeach ?>
					    		<?php endif ?>
					    	</select>
					    </div>
				    </div>
				    <div class="control-group">
					    <label class="control-label" for="alumno_disponibilidad">Disponibilidad</label>
					    <div class="controls">
					    	<textarea type="text" id="alumno_disponibilidad" name="alumno_disponibilidad"><?=$alumno[0]->alumno_disponibilidad?></textarea>
					    </div>
				    </div>
				    <div class="control-group">
					    <label class="control-label" for="alumno_estatus">Estatus</label>
					    <div class="controls">
					    	<input type="text" id="alumno_estatus" name="alumno_estatus" value="<?=$alumno[0]->alumno_estatus?>" readonly>
					    </div>
				    </div>
					<hr>
					<div class="control-group">
					    <label class="control-label" for="password">Password</label>
					    <div class="controls">
					    	<input type="password" id="password" name="password" value="<?=set_value('password')?>">
					    </div>
				    </div>
				    <div class="control-group">
					    <label class="control-label" for="password">Confirmar Password</label>
					    <div class="controls">
					    	<input type="password" id="password_confirmacion" name="password_confirmacion" value="<?=set_value('password_confirmacion')?>">
					    </div>
				    </div>
				    <div class="control-group">
					    <button id="apuntarse" class="btn btn-small btn-success" >
							<i class="icon-user icon-white"></i> 
							Modificar Datos
						</button>
				    </div>
			</form>
		</p>
	</p>
</div>

<?php $this->load->view('footer_view');?>