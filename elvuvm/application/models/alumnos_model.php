<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alumnos_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
	}

	public function getAllNotTrabajos($order = "apellido_adulto asc, nombre_adulto asc")
	{
		$consulta = $this->db->join('tbl_trabajos', 'tbl_trabajos.alumno_id=tbl_alumnos.alumno_id')->order_by($order)->get('tbl_alumnos');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = false;
		}
		$consulta->free_result();
		return $data;
	}
	public function getAll($order = "apellido_adulto asc, nombre_adulto asc")
	{
		$consulta = $this->db->order_by($order)->get('tbl_alumnos');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = false;
		}
		$consulta->free_result();
		return $data;
	}
	public function getByCedula($cedula = null)
	{
		$consulta = $this->db->where('alumno_cedula',$cedula)->get('tbl_alumnos');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = false;
		}
		$consulta->free_result();
		return $data;
	}	

	public function updateStatus($id = null)
	{
		$datos_alumno = array(
							"alumno_estatus" => "Asignado"
						);
		$consulta = $this->db->where('alumno_id',$id)->update('tbl_alumnos',$datos_alumno);
		if ($consulta) {
			return true;
		} else {
			return false;
		}
	}

	public function create($data=array())
	{
		
		$consulta = $this->db->insert('tbl_alumnos',$data);
		if ($consulta) {
			return true;
		} else {
			return false;
		}
	}

	public function update($data)
	{
		
		$user = $this->db->where('alumno_id', $data['alumno_id'])->update('tbl_alumnos', $data);
		
		if ($user) {
			return true;
		} else {
			return false;
		}
	}

	public function updateStatusBack($id = null)
	{
		$datos_alumno = array(
							"alumno_estatus" => "No Asignado"
						);
		$consulta = $this->db->where('alumno_id',$id)->update('tbl_alumnos',$datos_alumno);
		if ($consulta) {
			return true;
		} else {
			return false;
		}
	}

	public function getByIdInTrabajos($id = null)
	{
		$consulta = $this->db->join('tbl_trabajos', 'tbl_trabajos.alumno_id=tbl_alumnos.alumno_id')->where('tbl_alumnos.alumno_id',$id)->get('tbl_alumnos');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = false;
		}
		$consulta->free_result();
		return $data;
	}

	/*public function get_all() {
		$consulta = $this->db->join('tbl_adulto', 'tbl_adulto.id_adulto=tbl_social.id_adulto')->order_by('apellido_adulto asc, nombre_adulto asc')->get('tbl_social');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = false;
		}
		$consulta->free_result();
		return $data;
	}

	public function get_byid($id_social) {
		$consulta = $this->db->join('tbl_adulto', 'tbl_adulto.id_adulto=tbl_social.id_adulto')->join('tbl_representante', 'tbl_representante.id_representante=tbl_adulto.id_representante')->join('tbl_grupo_familiar', 'tbl_grupo_familiar.id_adulto=tbl_adulto.id_adulto')->join('tbl_personal', 'tbl_personal.id_personal=tbl_social.id_personal')->where("id_social", $id_social)->order_by('apellido_grupo asc, nombre_grupo asc')->limit(6)->get('tbl_social');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = false;
		}
		$consulta->free_result();
		return $data;
	}*/
}