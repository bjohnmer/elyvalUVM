<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trabajos_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
	}

	public function getById($id = null)
	{
		$consulta = $this->db->where('trabajo_id',$id)->get('tbl_trabajos');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function getByAlumno($id = null)
	{
		$consulta = $this->db->where('alumno_id',$id)->get('tbl_trabajos');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function apuntarse($data = array(), $user = null)
	{
		$data_trabajo = array (
							'alumno_id'		=> $user,
							'proyecto_id'	=> $data['proyecto_id'],
							'trabajo_fi'	=> date('Y-m-d'),
							'trabajo_fc'	=> date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + 365 day"))
						);
		$tra = $this->db->insert('tbl_trabajos', $data_trabajo);
		if ($tra) {
			return TRUE;
		} else {
			return FALSE;
		}
		
	}

	public function updateStatus()
	{
		$data_trabajo = array (
							'trabajo_status'=> 'Cerrado'
						);
		$tra = $this->db->where('trabajo_fc <',date('Y-m-d'))->where('trabajo_status','Abierto')->update('tbl_trabajos', $data_trabajo);
		if ($tra) {
			return TRUE;
		} else {
			return FALSE;
		}
		
	}
	
}