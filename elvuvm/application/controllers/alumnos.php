<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alumnos extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		// Carga de Librería para Manejar las Sesiones
		$this->load->library('session');
		//$this->load->library('Form_validation');

		// Verifica si hay un usuario Logueado, es decir, si hay una sesión abierta
		if (!$this->session->userdata("logged_in")) {
			// Si no es correcto, redirige al usuario hasta la página principal
			redirect('/');
		}
		//fin sesion

		$this->load->model('usuarios_model', 'usuarios');

		// Carga de librerías necesarias para manejar el módulo
		$this->load->database();
		$this->load->helper('url');
		
		$this->load->library('grocery_CRUD');	
	}
	
	function index() {
		try {
			// Función principal

			// Configuración de objetos
			$crud = new grocery_CRUD();

			// Tabla de Base de Datos
			$crud->set_table('tbl_alumnos');
			
			// Definición del Indivíduo
			$crud->set_subject('Alumno');
			
			// Campos a Mostrar en la Lista
			$crud->columns('alumno_cedula', 'alumno_nombres', 'alumno_apellidos', 'alumno_telf', 'alumno_email', 'alumno_semestre', 'carrera_id', 'alumno_estatus');

			// Relación con la tabla tbl_carreras
			$crud->set_relation('carrera_id', 'tbl_carreras', 'carrera_nombre');
			
			// Ordenamiento de la Lista
			$crud->order_by('alumno_cedula','ASC');

			// Alias para desplegar los campos
			$crud->display_as('alumno_cedula', 'Cédula');
			$crud->display_as('alumno_nombres', 'Nombres');
			$crud->display_as('alumno_apellidos', 'Apellidos');
			$crud->display_as('alumno_direccion', 'Dirección');
			$crud->display_as('alumno_telf', 'Teléfono');
			$crud->display_as('alumno_email', 'Correo Electrónico');
			$crud->display_as('alumno_semestre', 'Semestre');
			$crud->display_as('carrera_id', 'Carrera');
			$crud->display_as('alumno_disponibilidad', 'Disponibilidad');
			$crud->display_as('alumno_estatus', 'Estatus');
			

			// Definición de campos que se van a mostrar en Guardar y Modificar
			$crud->fields('alumno_cedula', 'alumno_nombres', 'alumno_apellidos', 'alumno_telf', 'alumno_email', 'alumno_semestre', 'carrera_id','alumno_direccion', 'alumno_disponibilidad');

			// Reglas de Validación
			$crud->set_rules('alumno_cedula', 'Cédula del Alumno', 'required|numeric|max_length[12]');

			// Permisos de usuario
			if (($this->session->userdata("tipo_usuario")=='Supervisor') or ($this->session->userdata("tipo_usuario")=='Usuario')) {
				$crud->where('alumno_id',$this->session->userdata("id_usuario"));
				$crud->callback_update(array($this,'policia'));
				$crud->unset_add();
				// $crud->unset_edit();
				$crud->unset_delete();
			}
			
			$crud->callback_before_insert(array($this,'saveUser'));

			$crud->unset_texteditor('alumno_direccion');
			$crud->unset_texteditor('alumno_disponibilidad');
			// Renderiza la Vista
			$output = $crud->render();
			
			// Llama a la función que va a mostrar la Vista
			$this->_example_output($output);
			
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	// Función que muestra la Vista
	function _example_output($output = null) {
		$this->load->view('alumnos_view.php',$output);
	}

	function saveUser($post_array, $primary_key = null)
	{
    	if ($this->usuarios->nuevo($post_array['alumno_cedula'])) 
    	{
    		return $post_array;
    	}
	}

	function policia ($post_array, $primary_key = null)
	{
		if ($post_array['id_usuario']!=$this->session->userdata("id_usuario"))
		{
			return false;
		}
	}

}