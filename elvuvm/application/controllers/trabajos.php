<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trabajos extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		// Carga de Librería para Manejar las Sesiones
		$this->load->library('session');
		//$this->load->library('Form_validation');

		// Verifica si hay un usuario Logueado, es decir, si hay una sesión abierta
		if (!$this->session->userdata("logged_in")) {
			// Si no es correcto, redirige al usuario hasta la página principal
			redirect('/');
		}
		//fin sesion

		$this->load->model('trabajos_model', 'trabajos');
		$this->load->model('proyectos_model', 'proyectos');
		$this->load->model('alumnos_model', 'alumnos');

		// Carga de librerías necesarias para manejar el módulo
		$this->load->database();
		$this->load->helper('url');
		
		$this->load->library('grocery_CRUD');	
	}
	
	function index() {
		try {
			// Función principal

			$this->trabajos->updateStatus();
			// Configuración de objetos
			$crud = new grocery_CRUD();

			// Tabla de Base de Datos
			$crud->set_table('tbl_trabajos');
			
			// Definición del Indivíduo
			$crud->set_subject('Trabajo Comunitario');
			
			// Campos a Mostrar en la Lista
			$crud->columns('alumno_id', 'proyecto_id', 'trabajo_fi', 'trabajo_fc', 'trabajo_status');

			// Relaciones
			$crud->set_relation('alumno_id','tbl_alumnos','{alumno_cedula} {alumno_apellidos} {alumno_nombres}', 'alumno_estatus = "No Asignado" ');
			$crud->set_relation('proyecto_id','tbl_proyectos','{proyecto_codigo} {proyecto_descripcion}', 'proyecto_asignados < proyecto_limite');

			// Alias para desplegar los campos
			$crud->display_as('alumno_id','Alumno');
			$crud->display_as('proyecto_id','Proyecto');
			$crud->display_as('trabajo_fi','Fecha de inicio');
			$crud->display_as('trabajo_fc','Fecha de Culminación');
			$crud->display_as('trabajo_status','Estado');
			
			// Definición de campos que se van a mostrar en Guardar y Modificar
			$crud->fields('alumno_id', 'proyecto_id', 'trabajo_fi', 'trabajo_fc', 'trabajo_status');
			$crud->required_fields('alumno_id', 'proyecto_id', 'trabajo_fi', 'trabajo_fc', 'trabajo_status');
			
			// Función para antes de eliminar
			$crud->callback_before_delete(array($this,'restarApuntado'));
			
			//función para antes de insertar
			$crud->callback_before_insert(array($this,'sumarApuntado'));

			// Renderiza la Vista
			$output = $crud->render();
			
			// Llama a la función que va a mostrar la Vista
			$this->_example_output($output);
			
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	// Función que muestra la Vista
	function _example_output($output = null) {
		$this->load->view('trabajos_view.php',$output);
	}

	function restarApuntado ($primary_key = null)
	{
		$trabajo = $this->trabajos->getById($primary_key);
		$this->alumnos->updateStatusBack($trabajo[0]->alumno_id);
		if ($this->proyectos->restarApuntadosOnDeleteTrabajo($trabajo[0]->proyecto_id))
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	function sumarApuntado ($post_array, $primary_key = null)
	{
		$this->alumnos->updateStatus($post_array['alumno_id']);
		if ($this->proyectos->sumarApuntadosOnAddTrabajo($post_array['proyecto_id']))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}