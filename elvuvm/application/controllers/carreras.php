<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Carreras extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		// Carga de Librería para Manejar las Sesiones
		$this->load->library('session');
		//$this->load->library('Form_validation');

		// Verifica si hay un usuario Logueado, es decir, si hay una sesión abierta
		if (!$this->session->userdata("logged_in")) {
			// Si no es correcto, redirige al usuario hasta la página principal
			redirect('/');
		}
		//fin sesion

		// Carga de librerías necesarias para manejar el módulo
		$this->load->database();
		$this->load->helper('url');
		
		$this->load->library('grocery_CRUD');	
	}
	
	function index() {
		try {
			// Función principal

			// Configuración de objetos
			$crud = new grocery_CRUD();

			// Tabla de Base de Datos
			$crud->set_table('tbl_carreras');
			
			// Definición del Indivíduo
			$crud->set_subject('Carrera');
			
			// Campos a Mostrar en la Lista
			$crud->columns('carrera_codigo', 'carrera_nombre');

			// Ordenamiento de la Lista
			$crud->order_by('carrera_nombre','ASC');

			// Alias para desplegar los campos
			$crud->display_as('carrera_codigo', 'Código');
			$crud->display_as('carrera_nombre', 'Carrera');
			
			// Definición de campos que se van a mostrar en Guardar y Modificar
			$crud->fields('carrera_codigo', 'carrera_nombre');

			// Reglas de Validación
			$crud->set_rules('carrera_codigo', 'Código de la Carrera', 'required|alpha|max_length[5]');
			$crud->set_rules('carrera_nombre', 'Nombre de la Carrera', 'required|alpha_space|max_length[50]');
			
			// Renderiza la Vista
			$output = $crud->render();
			
			// Llama a la función que va a mostrar la Vista
			$this->_example_output($output);
			
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	// Función que muestra la Vista
	function _example_output($output = null) {
		$this->load->view('carreras_view.php',$output);
	}

	function policia ($post_array, $primary_key = null)
	{
		if ($post_array['id_usuario']!=$this->session->userdata("id_usuario"))
		{
			return false;
		}
	}

}