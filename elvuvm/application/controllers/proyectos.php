<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proyectos extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		// Carga de Librería para Manejar las Sesiones
		$this->load->library('session');
		//$this->load->library('Form_validation');

		// Verifica si hay un usuario Logueado, es decir, si hay una sesión abierta
		if (!$this->session->userdata("logged_in")) {
			// Si no es correcto, redirige al usuario hasta la página principal
			redirect('/');
		}
		//fin sesion

		$this->load->model('proyectos_model', 'proyectos');

		// Carga de librerías necesarias para manejar el módulo
		$this->load->database();
		$this->load->helper('url');
		
		$this->load->library('grocery_CRUD');	
	}
	
	function index() {
		try {
			// Función principal

			// Configuración de objetos
			$crud = new grocery_CRUD();

			// Tabla de Base de Datos
			$crud->set_table('tbl_proyectos');
			
			// Definición del Indivíduo
			$crud->set_subject('Proyecto');
			
			// Campos a Mostrar en la Lista

			$crud->columns('proyecto_codigo','proyecto_descripcion','proyecto_ubicacion','proyecto_limite', 'proyecto_asignados');


			// Ordenamiento de la Lista
			$crud->order_by('proyecto_descripcion','ASC');

			// Alias para desplegar los campos
			$crud->display_as('proyecto_codigo','Código');
			$crud->display_as('proyecto_descripcion','Descripción');
			$crud->display_as('proyecto_ubicacion','Ubicación');
			$crud->display_as('proyecto_archivo','Archivo');
			$crud->display_as('proyecto_limite','Límite de alumnos');
			$crud->display_as('proyecto_asignados','Alumnos Apuntados');
			
			

			// Definición de campos que se van a mostrar en Guardar y Modificar
			$crud->fields('proyecto_codigo', 'proyecto_descripcion', 'proyecto_ubicacion', 'proyecto_archivo', 'proyecto_limite');

			// Reglas de Validación

			$crud->set_rules('proyecto_codigo','Código','required|alpha_numeric');
			$crud->set_rules('proyecto_descripcion','Descripción','required');
			$crud->set_rules('proyecto_ubicacion','Ubicación','required');
			$crud->set_rules('proyecto_limite','Límite de alumnos','required|numeric');

			// Permisos de usuario
			// if (($this->session->userdata("tipo_usuario")=='Supervisor') or ($this->session->userdata("tipo_usuario")=='Usuario')) {
			// 	$crud->where('alumno_id',$this->session->userdata("id_usuario"));
			// 	$crud->callback_update(array($this,'policia'));
			// 	$crud->unset_add();
			// 	// $crud->unset_edit();
			// 	$crud->unset_delete();
			// }
			
			// Cambiar la apariencia y el tipo de campo
			$crud->set_field_upload('proyecto_archivo','assets/uploads/files');
			$crud->unset_texteditor('proyecto_ubicacion');


			// Renderiza la Vista
			$output = $crud->render();
			
			// Llama a la función que va a mostrar la Vista
			$this->_example_output($output);
			
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	// Función que muestra la Vista
	function _example_output($output = null) {
		$this->load->view('proyectos_view.php',$output);
	}

	function policia ($post_array, $primary_key = null)
	{
		if ($post_array['id_usuario']!=$this->session->userdata("id_usuario"))
		{
			return false;
		}

	}

	function test()
	{
		$id = $_POST['id'];
		$proyecto = $this->proyectos->getById($id);
		if ($proyecto[0]->proyecto_asignados < $proyecto[0]->proyecto_limite) {
			$data['mensaje'] = "Ok";
		}
		else
		{
			$data['mensaje'] = "Error: Éste proyecto ha llegado al límite de alumnos apuntados permitido";			
		}

		echo json_encode($data);
	}

}