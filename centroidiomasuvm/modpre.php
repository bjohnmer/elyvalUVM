<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE preinscripcion SET id_sub_cat=%s, nombre_pre=%s, apellido_pre=%s, cedula_pre=%s, email=%s, horario=%s, carrera=%s, fecha_pre=%s WHERE id_pre=%s",
                       GetSQLValueString($_POST['id_sub_cat'], "int"),
                       GetSQLValueString($_POST['nombre_pre'], "text"),
                       GetSQLValueString($_POST['apellido_pre'], "text"),
                       GetSQLValueString($_POST['cedula_pre'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['horario'], "text"),
                       GetSQLValueString($_POST['carrera'], "text"),
                       GetSQLValueString($_POST['fecha_pre'], "date"),
                       GetSQLValueString($_POST['id_pre'], "int"));

  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
  $Result1 = mysql_query($updateSQL, $centroidiomasuvm) or die(mysql_error());

  $updateGoTo = "preinscripcion.php?p=listpre";
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_preinscripcion = "-1";
if (isset($_GET['id_pre'])) {
  $colname_preinscripcion = $_GET['id_pre'];
}
mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_preinscripcion = sprintf("SELECT * FROM preinscripcion WHERE id_pre = %s", GetSQLValueString($colname_preinscripcion, "int"));
$preinscripcion = mysql_query($query_preinscripcion, $centroidiomasuvm) or die(mysql_error());
$row_preinscripcion = mysql_fetch_assoc($preinscripcion);
$totalRows_preinscripcion = mysql_num_rows($preinscripcion);

mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_subcategoria = "SELECT * FROM sub_categoria";
$subcategoria = mysql_query($query_subcategoria, $centroidiomasuvm) or die(mysql_error());
$row_subcategoria = mysql_fetch_assoc($subcategoria);
$totalRows_subcategoria = mysql_num_rows($subcategoria);
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/prein.png" />
<h1>Modificar Preinscripcion</h1>
<table width="60%" border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td colspan="3" align="left"><a href="preinscripcion.php?p=listpre"><img src="imagenes/atras.png" alt="Atrás" width="45" height="46" title="Atrás" /></a></td>
  </tr>
  <tr class="htdata">
    <td>&nbsp;</td>
  </tr>
<tr class="tdata">
<td><form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table align="center">
    <tr valign="baseline">
    <td nowrap align="right"><label>Subcategoria:</label></td>
    <td align="left" valign="top">
    <select name="id_sub_cat" id="id_sub_cat" title="<?php if (!empty($row_preinscripcion)) {echo $row_preinscripcion['id_sub_cat'];} ?>">
                <?php
do {  
?><option value="<?php echo $row_subcategoria['id_sub_cat']; ?>"<?php if (!(strcmp($row_subcategoria['id_sub_cat'], $row_preinscripcion['id_sub_cat']))) {echo "selected=\"selected\"";} ?>><?php echo $row_subcategoria['nombre_sub_cat']; ?></option>
                <?php
} while ($row_subcategoria = mysql_fetch_assoc($subcategoria));
  $rows = mysql_num_rows($subcategoria);
  if($rows > 0) {
      mysql_data_seek($subcategoria, 0);
	  $row_subcategoria = mysql_fetch_assoc($subcategoria);
  }
?>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Nombre:</label></td>
      <td><input type="text" name="nombre_pre" value="<?php echo htmlentities($row_preinscripcion['nombre_pre'], ENT_COMPAT, 'utf-8'); ?>" size="60" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Apellido:</label></td>
      <td><input type="text" name="apellido_pre" value="<?php echo htmlentities($row_preinscripcion['apellido_pre'], ENT_COMPAT, 'utf-8'); ?>" size="60" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Cédula de Identidad:</label></td>
      <td><input type="text" name="cedula_pre" value="<?php echo htmlentities($row_preinscripcion['cedula_pre'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Email:</label></td>
      <td><input type="text" name="email" value="<?php echo htmlentities($row_preinscripcion['email'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Horario:</label></td>
      <td>
        <select name="horario" id="horario">
          <option value="Matutino: 8:00 am - 10:00 am" <?php if ($row_preinscripcion['horario']=='Matutino: 8:00 am - 10:00 am'): ?>
            selected
          <?php endif ?>>Matutino: 8:00 am - 10:00 am</option>

          <option value="Matutino: 10:00 am - 12:00 m" <?php if ($row_preinscripcion['horario']=='Matutino: 10:00 am - 12:00 m'): ?>
            selected
          <?php endif ?>>Matutino: 10:00 am - 12:00 m</option>

          <option value="Vespertino: 02:00 pm - 04:00 pm" <?php if ($row_preinscripcion['horario']=='Vespertino: 02:00 pm - 04:00 pm'): ?>
            selected
          <?php endif ?>>Vespertino: 02:00 pm - 04:00 pm</option>

          <option value="Vespertino: 04:00 pm - 06:00 pm" <?php if ($row_preinscripcion['horario']=='Vespertino: 04:00 pm - 06:00 pm'): ?>
            selected
          <?php endif ?>>Vespertino: 04:00 pm - 06:00 pm</option>

          <option value="Nocturno: 06:00 pm - 08:00 pm" <?php if ($row_preinscripcion['horario']=='Nocturno: 06:00 pm - 08:00 pm'): ?>
            selected
          <?php endif ?>>Nocturno: 06:00 pm - 08:00 pm</option>

          <option value="Sabatino: 08:00 am - 04:15 pm" <?php if ($row_preinscripcion['horario']=='Sabatino: 08:00 am - 04:15 pm'): ?>
            selected
          <?php endif ?>>Sabatino: 08:00 am - 04:15 pm</option>
        </select>
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Carrera:</label></td>
      <td><input type="text" name="carrera" value="<?php echo htmlentities($row_preinscripcion['carrera'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" value="Modificar" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="id_pre" value="<?php echo $row_preinscripcion['id_pre']; ?>" />
</form>
</td>
</tr>
</table>
</center>
<p>&nbsp;</p>
<?php
mysql_free_result($preinscripcion);

mysql_free_result($subcategoria);
?>
