<?php require_once("sesion.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Centro de Iidiomas UVM</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/funciones.js"></script>

<?php if (isset($_SESSION['6_letters_code'])){ ?>
    <link href="menustyle.css" rel="stylesheet" type="text/css" />
    <script type='text/javascript' src='js/jquery-1.2.6.min.js'></script>
    <script type='text/javascript' src='js/kwicks.js'></script>
    <script type='text/javascript' src='js/custom.js'></script>
<?php } ?>

</head>
<body>
<div id="pagina">
	<div id="top">
        <?php require_once("menu.php"); ?>
        <div id="fondoheader">
        	<?php require_once("header.php");?>
        </div>
    </div>
    <?php
        if (!empty($_SESSION['user_name'])) { 
            echo '<div id="menucentro">';
            if ($_SESSION['tipo_user']=="Administrador") { 
                include_once("menuadmin.php");
            } 
            if ($_SESSION['tipo_user']=="Profesor") {
                include_once("menuprof.php");
            }
            if ($_SESSION['tipo_user']=="Estudiante") {
                include_once("menuestu.php");
            }
            echo "</div>";
        }
    ?>      
    <div id="menucentro">
            <?php 
				if (!empty($_GET['p'])) {
					include ($_GET['p'].'.php');
				}
			?>
   	</div>
    <div id="pie">
            <?php require_once("pie.php");?>
    </div>
</div>  
</body>
</html>