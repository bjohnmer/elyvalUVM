<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
	
  $prefijo = substr(md5(uniqid(rand())),0,6);
  $change = false;
  	if (!empty($_FILES["img_usuario"]['name'])) {		
		$imgtamano = $_FILES["img_usuario"]['size'];
		$imgtipo = $_FILES["img_usuario"]['type'];
		$imgarchivo = "";
		if ($_FILES['img_usuario']['name'] != "") {
			if (copy($_FILES['img_usuario']['tmp_name'],"uploads/img/".$prefijo."_".$_FILES["img_usuario"]['name'])) {
				$status = "Archivo subido: <b>".$imgarchivo."</b>";
				$imgarchivo = "uploads/img/".$prefijo."_".$_FILES["img_usuario"]['name'];
			} else {
				$status = "Error al subir el archivo";
			}
		} else {
			$status = "Error al subir archivo";
		}
		$change = true;
	} else {
		$imgarchivo = $_POST['img_usuario1'];
	}

  $updateSQL = sprintf("UPDATE usuarios SET nombre_usuario=%s, apellido_usuario=%s, email_usuario=%s, login_usuario=%s, clave_usuario=%s, img_usuario=%s, telefono=%s, descripcion_usuario=%s, ocupacion=%s, tipo_usuario=%s, id_sub_cat=%s WHERE id_usuario=%s",
                       GetSQLValueString($_POST['nombre_usuario'], "text"),
                       GetSQLValueString($_POST['apellido_usuario'], "text"),
                       GetSQLValueString($_POST['email_usuario'], "text"),
                       GetSQLValueString($_POST['login_usuario'], "text"),                      
					   GetSQLValueString(md5($_POST['clave_usuario']), "text"),
                       GetSQLValueString($imgarchivo, "text"),
                       GetSQLValueString($_POST['telefono'], "text"),
                       GetSQLValueString($_POST['descripcion_usuario'], "text"),
                       GetSQLValueString($_POST['ocupacion'], "text"),
                       GetSQLValueString($_POST['tipo_usuario'], "text"),
                       GetSQLValueString($_POST['id_sub_cat'], "int"),
                       GetSQLValueString($_POST['id_usuario'], "int"));
					   
     if ($change) {
		unlink($_REQUEST['img_usuario1']);
	}

  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
  $Result1 = mysql_query($updateSQL, $centroidiomasuvm) or die(mysql_error());

  $updateGoTo = "usuarios.php?p=listusuarios";
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_usuarios = "-1";
if (isset($_GET['id_usuario'])) {
  $colname_usuarios = $_GET['id_usuario'];
}
mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_usuarios = sprintf("SELECT * FROM usuarios WHERE id_usuario = %s", GetSQLValueString($colname_usuarios, "int"));
$usuarios = mysql_query($query_usuarios, $centroidiomasuvm) or die(mysql_error());
$row_usuarios = mysql_fetch_assoc($usuarios);
$totalRows_usuarios = mysql_num_rows($usuarios);

mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_subcategorias = "SELECT * FROM sub_categoria";
$subcategorias = mysql_query($query_subcategorias, $centroidiomasuvm) or die(mysql_error());
$row_subcategorias = mysql_fetch_assoc($subcategorias);
$totalRows_subcategorias = mysql_num_rows($subcategorias);
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/usua.png" />
<h1>Modificar Usuario</h1>
<table width="70%" border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td colspan="3" align="left"><a href="usuarios.php?p=listusuarios"><img src="imagenes/atras.png" alt="Atrás" width="45" height="46" title="Atrás" /></a></td>
  </tr>
  <tr class="htdata">
    <td>&nbsp;</td>
  </tr>
<tr class="tdata">
<td><form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">

  <table align="center">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Nombre:</label></td>
      <td><input type="text" name="nombre_usuario" value="<?php echo htmlentities($row_usuarios['nombre_usuario'], ENT_COMPAT, 'utf-8'); ?>" size="50" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Apellido:</label></td>
      <td><input type="text" name="apellido_usuario" value="<?php echo htmlentities($row_usuarios['apellido_usuario'], ENT_COMPAT, 'utf-8'); ?>" size="50" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Email:</label></td>
      <td><input type="text" name="email_usuario" value="<?php echo htmlentities($row_usuarios['email_usuario'], ENT_COMPAT, 'utf-8'); ?>" size="50" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Login:</label></td>
      <td><input type="text" name="login_usuario" value="<?php echo htmlentities($row_usuarios['login_usuario'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Clave:</label></td>
      <td><input type="password" name="clave_usuario"  size="32" />
      </br>Si deseas cambiar la clave, escribe aquí la nueva. En caso contrario, deja las casillas como están.
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Imagen de Pérfil:</label></td>
      <td><input type="file" name="img_usuario" value="" size="32" />
      <p><img src="<?php if (file_exists($row_usuarios['img_usuario'])) {echo $row_usuarios['img_usuario'];} else { echo "imagenes/user.png";}?>" alt="" width="169" height="169" /></p>
      Esta sera tu imagen de perfil.   Formatos Permitidos: jpeg, gif, png.</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Telefono:</label></td>
      <td><input type="text" name="telefono" value="<?php echo htmlentities($row_usuarios['telefono'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Descripcion:</label></td>
      <td><textarea name="descripcion_usuario" cols="60" rows="7"><?php echo htmlentities($row_usuarios['descripcion_usuario'], ENT_COMPAT, 'utf-8'); ?></textarea>
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" type="text/javascript"></script>
                    <script src="js/jquery.textareaCounter.plugin.js" type="text/javascript"></script>
<script type="text/javascript">
                        var info;
                        $(document).ready(function(){
                            var options2 = {
                                    'maxCharacterSize': 220,
                                    'originalStyle': 'originalTextareaInfo',
                                    'warningStyle' : 'warningTextareaInfo',
                                    'warningNumber': 40,
                                    'displayFormat' : '#input/#max caracteres | #words palabras'
                            };
                            $('#descripcion_usuario').textareaCount(options2);
                        });
                    </script>
                          Incluye alguna información biográfica en tu perfil. Podrá mostrarse públicamente.
              </td>
            </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Ocupacion:</label></td>
      <td><input type="text" name="ocupacion" value="<?php echo htmlentities($row_usuarios['ocupacion'], ENT_COMPAT, 'utf-8'); ?>" size="50" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Tipo de Usuario:</label></td>
      <td><select name="tipo_usuario" id="tipo_usuario">
                  <?php if ($_SESSION['tipo_user']=='Administrador') { ?><option value="Administrador" <?php if (!(strcmp("Administrador", $row_usuarios['tipo_usuario']))) {echo "selected=\"selected\"";} ?>>Administrador</option><?php } ?>
                  <?php if ($_SESSION['tipo_user']=='Administrador' || $_SESSION['tipo_user']=='Profesor') { ?><option value="Profesor" <?php if (!(strcmp("Profesor", $row_usuarios['tipo_usuario']))) {echo "selected=\"selected\"";} ?>>Profesor</option><?php } ?>
              </select></td>
    </tr>
    <tr valign="baseline">
              <td nowrap align="right"><label>Subcategoria:</label></td>
              <td align="left" valign="top">
              <select name="id_sub_cat" id="id_sub_cat" title="<?php if (!empty($row_usuarios)) {echo $row_usuarios['id_sub_cat'];} ?>">
                <?php
do {  
?><option value="<?php echo $row_subcategorias['id_sub_cat']; ?>"<?php if (!(strcmp($row_subcategorias['id_sub_cat'], $row_usuarios['id_sub_cat']))) {echo "selected=\"selected\"";} ?>><?php echo $row_subcategorias['nombre_sub_cat']; ?></option>
                <?php
} while ($row_subcategorias = mysql_fetch_assoc($subcategorias));
  $rows = mysql_num_rows($subcategorias);
  if($rows > 0) {
      mysql_data_seek($subcategorias, 0);
	  $row_subcategorias = mysql_fetch_assoc($subcategorias);
  }
?>
              </select></td>
            </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" value="Modificar" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="id_usuario" value="<?php echo $row_usuarios['id_usuario']; ?>" />
  <input name="img_usuario1" type="hidden" id="img_usuario1" value="<?php echo $row_usuarios['img_usuario']; ?>" />
</form>
</td>
</tr>
</table>
</center>
<p>&nbsp;</p>
<?php
mysql_free_result($usuarios);

mysql_free_result($subcategorias);
?>
