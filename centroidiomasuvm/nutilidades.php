<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
 	$prefijo = substr(md5(uniqid(rand())),0,6);
	$imgtamano = $_FILES["img_utilidades"]['size'];
	$imgtipo = $_FILES["img_utilidades"]['type'];
	$imgarchivo = "";
	
	$doctamano = $_FILES["doc_utilidades"]['size'];
	$doctipo = $_FILES["doc_utilidades"]['type'];
	$docarchivo = "";
		
	if ($_FILES['img_utilidades']['name'] != "") {
		// guardamos el archivo a la carpeta files
		if (copy($_FILES['img_utilidades']['tmp_name'],"uploads/img/".$prefijo."_".$_FILES["img_utilidades"]['name'])) {
			$status = "Archivo subido: <b>".$imgarchivo."</b>";
			$imgarchivo = "uploads/img/".$prefijo."_".$_FILES["img_utilidades"]['name'];
		} else {
			$status = "Error al subir el archivo";
		}
	} else {
		$status = "Error al subir archivo";
	}
	
	
	if ($_FILES['doc_utilidades']['name'] != "") {
		// guardamos el archivo a la carpeta files
		if (copy($_FILES['doc_utilidades']['tmp_name'],"uploads/docs/".$prefijo."_".$_FILES["doc_utilidades"]['name'])) {
			$status = "Archivo subido: <b>".$docarchivo."</b>";
			$docarchivo = "uploads/docs/".$prefijo."_".$_FILES["doc_utilidades"]['name'];
		} else {
			$status = "Error al subir el archivo";
		}
	} else {
		$status = "Error al subir archivo";
	}

  $insertSQL = sprintf("INSERT INTO utilidades (id_utilidades, titulo_utilidades, resumen_utilidades, link_utilidades, img_utilidades, doc_utilidades, video_utilidades) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['id_utilidades'], "int"),
                       GetSQLValueString($_POST['titulo_utilidades'], "text"),
                       GetSQLValueString($_POST['resumen_utilidades'], "text"),
                       GetSQLValueString($_POST['link_utilidades'], "text"),
                       GetSQLValueString($imgarchivo, "text"),
                       GetSQLValueString($docarchivo, "text"),
                       GetSQLValueString($_POST['video_utilidades'], "text"));

  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
  $Result1 = mysql_query($insertSQL, $centroidiomasuvm) or die(mysql_error());

  $insertGoTo = "utilidades.php?p=listutilidades";
  header(sprintf("Location: %s", $insertGoTo));
}
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/utili.png" />
<h1>Nueva Utilidad</h1>
<table width="60%" border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td colspan="3" align="left"><a href="utilidades.php?p=listutilidades"><img src="imagenes/atras.png" alt="Atrás" width="45" height="46" title="Atrás" /></a></td>
  </tr>
  <tr class="htdata">
    <td>&nbsp;</td>
  </tr>
<tr class="tdata">
<td><form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table align="center">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Titulo:</label></td>
      <td><input type="text" name="titulo_utilidades" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Resumen:</label></td>
      <td><textarea name="resumen_utilidades" cols="52" rows="20"></textarea></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Link:</label></td>
      <td><input type="text" name="link_utilidades" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Imágen:</label></td>
      <td><input type="file" name="img_utilidades" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Documentos:</label></td>
      <td><input type="file" name="doc_utilidades" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
       <td align="right" valign="top" nowrap="nowrap"><label>Video:</label></td>
       <td align="left" valign="top"><p>
        <textarea name="video_utilidades" cols="50" rows="3"></textarea>
        </p>
         <p>Formato permitido:</p>
         <p>http://www.youtube.com/embed/ejemplo</p></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" value="Guardar" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_insert" value="form1" />
</form></td>
</tr>
</table>
</center>
<p>&nbsp;</p>
