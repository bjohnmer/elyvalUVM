<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  
  $prefijo = substr(md5(uniqid(rand())),0,6);
  $change = false;
  $changed = false;
  $changel = false;
  	if (!empty($_FILES["imagen_sub_cat"]['name'])) {		
		$imgtamano = $_FILES["imagen_sub_cat"]['size'];
		$imgtipo = $_FILES["imagen_sub_cat"]['type'];
		$imgarchivo = "uploads/img/".$prefijo."_".$_FILES["imagen_sub_cat"]['name'];
		if ($_FILES['imagen_sub_cat']['name'] != "") {
			if (copy($_FILES['imagen_sub_cat']['tmp_name'],$imgarchivo)) {
				$status = "Archivo subido: <b>".$imgarchivo."</b>";
			} else {
				$status = "Error al subir el archivo";
			}
		} else {
			$status = "Error al subir archivo";
		}
		$change = true;
	} else {
		$imgarchivo = $_POST['imagen_sub_cat1'];
	}

  $updateSQL = sprintf("UPDATE sub_categoria SET nombre_sub_cat=%s, descripcion_sub_cat=%s, imagen_sub_cat=%s, id_categoria=%s WHERE id_sub_cat=%s",
                       GetSQLValueString($_POST['nombre_sub_cat'], "text"),
                       GetSQLValueString($_POST['descripcion_sub_cat'], "text"),
                       GetSQLValueString($imgarchivo, "text"),
                       GetSQLValueString($_POST['id_categoria'], "int"),
                       GetSQLValueString($_POST['id_sub_cat'], "int"));
	if ($change) {
		unlink($_REQUEST['imagen_sub_cat1']);
	}

  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
  $Result1 = mysql_query($updateSQL, $centroidiomasuvm) or die(mysql_error());

  $updateGoTo = "subcategorias.php?p=listsubcat";
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_subcategorias = "-1";
if (isset($_GET['id_sub_cat'])) {
  $colname_subcategorias = $_GET['id_sub_cat'];
}
mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_subcategorias = sprintf("SELECT * FROM sub_categoria WHERE id_sub_cat = %s", GetSQLValueString($colname_subcategorias, "int"));
$subcategorias = mysql_query($query_subcategorias, $centroidiomasuvm) or die(mysql_error());
$row_subcategorias = mysql_fetch_assoc($subcategorias);
$totalRows_subcategorias = mysql_num_rows($subcategorias);

mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_categorias = "SELECT * FROM categorias";
$categorias = mysql_query($query_categorias, $centroidiomasuvm) or die(mysql_error());
$row_categorias = mysql_fetch_assoc($categorias);
$totalRows_categorias = mysql_num_rows($categorias);
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/subcat.png" />
<h1>Modificar Subcategoría</h1>
<table width="60%" border="0" cellpadding="1" cellspacing="1">
   <tr>
    <td colspan="3" align="left"><a href="subcategorias.php?p=listsubcat"><img src="imagenes/atras.png" alt="Atrás" width="45" height="46" title="Atrás" /></a></td>
  </tr>
  <tr class="htdata">
    <td>&nbsp;</td>
  </tr>
  <tr class="tdata">
        <td><form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="form1" id="form1">
          <table width="100%" align="center">
            <tr valign="baseline">
              <td nowrap="nowrap" align="right"><label>Nombre de la Sub-categoria:</label></td>
              <td><input type="text" name="nombre_sub_cat" value="<?php echo htmlentities($row_subcategorias['nombre_sub_cat'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
            </tr>
            <tr valign="baseline">
              <td align="right" valign="middle" nowrap="nowrap"><label>Descripcion:</label></td>
              <td><textarea name="descripcion_sub_cat" cols="32" rows="5"><?php echo htmlentities($row_subcategorias['descripcion_sub_cat'], ENT_COMPAT, 'utf-8'); ?></textarea></td>
            </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right"><label>Imagen:</label></td>
              <td><input type="file" name="imagen_sub_cat" value="<?php echo htmlentities($row_subcategorias['imagen_sub_cat'], ENT_COMPAT, 'utf-8'); ?>" size="32" />
            <p><img src="<?php echo $row_subcategorias['imagen_sub_cat']; ?>" width="169" height="169" /></p></td>
            </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right"><label>Categoria:</label></td>
              <td align="left" valign="top">
                <select name="id_categoria" id="id_categoria">
                  <?php
do {  
?>
                  <option value="<?php echo $row_categorias['id_categoria']?>"<?php if (!(strcmp($row_categorias['id_categoria'], $row_subcategorias['id_categoria']))) {echo "selected=\"selected\"";} ?>><?php echo $row_categorias['nombre_categoria']?></option>
                  <?php
} while ($row_categorias = mysql_fetch_assoc($categorias));
  $rows = mysql_num_rows($categorias);
  if($rows > 0) {
      mysql_data_seek($categorias, 0);
	  $row_categorias = mysql_fetch_assoc($categorias);
  }
?>
                </select></td>
            </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right">&nbsp;</td>
              <td><input type="submit" value="Modificar" /></td>
            </tr>
          </table>
          <input type="hidden" name="MM_update" value="form1" />
          <input type="hidden" name="id_sub_cat" value="<?php echo $row_subcategorias['id_sub_cat']; ?>" />
          <input name="imagen_sub_cat1" type="hidden" id="imagen_sub_cat1" value="<?php echo $row_subcategorias['imagen_sub_cat']; ?>" />
    </form></td>
    </tr>
    </table>
    </center>
    
<p>&nbsp;</p>

<?php
mysql_free_result($subcategorias);

mysql_free_result($categorias);
?>
