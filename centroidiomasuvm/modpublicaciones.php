<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
	
	$prefijo = substr(md5(uniqid(rand())),0,6);
  $change = false;
  $changed = false;
  if (!empty($_FILES["img_pub"]['name'])) {		
		$imgtamano = $_FILES["img_pub"]['size'];
		$imgtipo = $_FILES["img_pub"]['type'];
		$imgarchivo = "";
		if ($_FILES['img_pub']['name'] != "") {
			if (copy($_FILES['img_pub']['tmp_name'],"uploads/img/".$prefijo."_".$_FILES["img_pub"]['name'])) {
				$status = "Archivo subido: <b>".$imgarchivo."</b>";
				$imgarchivo = "uploads/img/".$prefijo."_".$_FILES["img_pub"]['name'];
			} else {
				$status = "Error al subir el archivo";
			}
		} else {
			$status = "Error al subir archivo";
		}
		$change = true;
	} else {
		$imgarchivo = $_POST['img_pub1'];
	}

  $prefijo2 = substr(md5(uniqid(rand())),0,6);
  $change = false;
  $changed = false;
  if (!empty($_FILES["doc_pub"]['name'])) {   
    $doctamano = $_FILES["doc_pub"]['size'];
    $doctipo = $_FILES["doc_pub"]['type'];
    $docarchivo = "";
    if ($_FILES['doc_pub']['name'] != "") {
      if (copy($_FILES['doc_pub']['tmp_name'],"uploads/docs/".$prefijo2."_".$_FILES["doc_pub"]['name'])) {
        $docarchivo = "uploads/docs/".$prefijo2."_".$_FILES["doc_pub"]['name'];
        $status = "Archivo subido: <b>".$docarchivo."</b>";
      } else {
        $status = "Error al subir el archivo";
      }
    } else {
      $status = "Error al subir archivo";
    }
    $change = true;
  } else {
    $docarchivo = $_POST['doc_pub1'];
  }

  $updateSQL = sprintf("UPDATE publicaciones SET titulo_pub=%s, fechahora_pub=%s, img_pub=%s, doc_pub=%s, resumen_pub=%s, id_sub_cat=%s, id_usuario=%s, info_pub=%s WHERE id_pub=%s",
                       GetSQLValueString($_POST['titulo_pub'], "text"),
                       GetSQLValueString($_POST['fechahora_pub'], "date"),
                       GetSQLValueString($imgarchivo, "text"),
                       GetSQLValueString($docarchivo, "text"),
                       GetSQLValueString($_POST['resumen_pub'], "text"),
                       GetSQLValueString($_POST['id_sub_cat'], "int"),
                       GetSQLValueString($_POST['id_usuario'], "int"),
                       GetSQLValueString($_POST['info_pub'], "int"),
                       GetSQLValueString($_POST['id_pub'], "int"));
					   
  	if ($change) {
    unlink($_REQUEST['img_pub1']);
		unlink($_REQUEST['doc_pub1']);
	}

  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
  $Result1 = mysql_query($updateSQL, $centroidiomasuvm) or die(mysql_error());

  $updateGoTo = "publicaciones.php?p=listpublicaciones";
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_publicaciones = "-1";
if (isset($_GET['id_pub'])) {
  $colname_publicaciones = $_GET['id_pub'];
}
mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_publicaciones = sprintf("SELECT * FROM publicaciones WHERE id_pub = %s ORDER BY id_pub ASC", GetSQLValueString($colname_publicaciones, "int"));
$publicaciones = mysql_query($query_publicaciones, $centroidiomasuvm) or die(mysql_error());
$row_publicaciones = mysql_fetch_assoc($publicaciones);
$totalRows_publicaciones = mysql_num_rows($publicaciones);

if ($_SESSION['tipo_user'] == "Profesor") {
	mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
	$query_subcategorias = "SELECT * FROM sub_categoria WHERE id_sub_cat = '".$_SESSION['id_sub_cat']."' ORDER BY id_sub_cat ASC";
	$subcategorias = mysql_query($query_subcategorias, $centroidiomasuvm) or die(mysql_error());
	$row_subcategorias = mysql_fetch_assoc($subcategorias);
	$totalRows_subcategorias = mysql_num_rows($subcategorias);
} else {
	mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
	$query_subcategorias = "SELECT * FROM sub_categoria ORDER BY id_sub_cat ASC";
	$subcategorias = mysql_query($query_subcategorias, $centroidiomasuvm) or die(mysql_error());
	$row_subcategorias = mysql_fetch_assoc($subcategorias);
	$totalRows_subcategorias = mysql_num_rows($subcategorias);
}
if ($_SESSION['tipo_user'] == "Profesor") {
	mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
	$query_usuarios = "SELECT * FROM usuarios WHERE id_usuario = '".$row_publicaciones['id_usuario']."' ORDER BY id_usuario ASC";
	$usuarios = mysql_query($query_usuarios, $centroidiomasuvm) or die(mysql_error());
	$row_usuarios = mysql_fetch_assoc($usuarios);
	$totalRows_usuarios = mysql_num_rows($usuarios);
}
if ($_SESSION['tipo_user'] == "Administrador") {
	$colname_categorias = "-1";
	if (isset($_GET['id_sub_cat'])) {
	  $colname_categorias = $_GET['id_cat'];
	}
	mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
	$query_usuarios = "SELECT * FROM usuarios ORDER BY id_usuario ASC";
	$usuarios = mysql_query($query_usuarios, $centroidiomasuvm) or die(mysql_error());
	$row_usuarios = mysql_fetch_assoc($usuarios);
	$totalRows_usuarios = mysql_num_rows($usuarios);
}

?>
<center>
<p>&nbsp;</p>
<img src="imagenes/public.png" />
<h1>Modificar Publicación</h1>
<table width="60%" border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td colspan="3" align="left"><a href="publicaciones.php?p=listpublicaciones"><img src="imagenes/atras.png" alt="Atrás" width="45" height="46" title="Atrás" /></a></td>
  </tr>
  <tr class="htdata">
    <td>&nbsp;</td>
  </tr>
<tr class="tdata">
<td><form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1"> 
 <table align="center">
        <tr valign="baseline">
          <td nowrap="nowrap" align="right"><label>Titulo:</label></td>
          <td><input type="text" name="titulo_pub" value="<?php echo htmlentities($row_publicaciones['titulo_pub'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
        </tr>
           <tr valign="baseline">
              <td align="right" valign="top" nowrap="nowrap"><label>Imágen:</label></td>
              <td align="left" valign="top">
                <p>
                 <input type="file" name="img_pub" value="<?php echo htmlentities($row_publicaciones['img_pub'], ENT_COMPAT, 'utf-8'); ?>" size="32" />
                </p>
              <p><img src="<?php echo $row_publicaciones['img_pub']; ?>" width="169" height="169"/></p></td>
            </tr>
            <tr valign="baseline">
              <td align="right" valign="top" nowrap="nowrap"><label>Archivo:</label></td>
              <td align="left" valign="top">
                <p>
                 <input type="file" name="doc_pub" value="<?php echo htmlentities($row_publicaciones['doc_pub'], ENT_COMPAT, 'utf-8'); ?>" size="32" />
                </p>
              <p><a href="<?php echo $row_publicaciones['doc_pub']; ?>" target="_blank"><?php echo $row_publicaciones['doc_pub']; ?></a> </p></td>
            </tr>
        <tr valign="baseline">
          <td align="right" valign="top" nowrap="nowrap"><label>Resumen:</label></td>
          <td><textarea name="resumen_pub" cols="50" rows="30"><?php echo htmlentities($row_publicaciones['resumen_pub'], ENT_COMPAT, 'utf-8'); ?></textarea></td>
        </tr>
            <tr valign="baseline">
              <td nowrap align="right"><label>Subcategoria:</label></td>
              <td align="left" valign="top">
              <select name="id_sub_cat" id="id_sub_cat" >
                <?php
do {  
?><option value="<?php echo $row_subcategorias['id_sub_cat']?>" <?php if ($row_publicaciones['id_sub_cat']==$row_subcategorias['id_sub_cat']): 
  echo "selected";
 endif ?>><?php echo $row_subcategorias['nombre_sub_cat']?></option>
                <?php
} while ($row_subcategorias = mysql_fetch_assoc($subcategorias));
  $rows = mysql_num_rows($subcategorias);
  if($rows > 0) {
      mysql_data_seek($subcategorias, 0);
	  $row_subcategorias = mysql_fetch_assoc($subcategorias);
  }
?>
              </select></td>
            </tr>
            <tr valign="baseline">
              <td align="right" valign="top" nowrap="nowrap"><label>Usuario:</label></td>
              <td>
                <select name="id_usuario" id="id_usuario">
                  <?php
do {  
?>
                  <option value="<?php echo $row_usuarios['id_usuario']; ?>"<?php if (!(strcmp($row_usuarios['id_usuario'], $row_publicaciones['id_usuario']))) {echo "selected=\"selected\"";} ?>><?php echo $row_usuarios['nombre_usuario']; ?> <?php echo $row_usuarios['apellido_usuario']; ?></option>
                  <?php
} while ($row_usuarios = mysql_fetch_assoc($usuarios));
  $rows = mysql_num_rows($usuarios);
  if($rows > 0) {
      mysql_data_seek($usuarios, 0);
	  $row_usuarios = mysql_fetch_assoc($usuarios);
  }
?>
              </select></td>
            </tr>
            <tr>
              <td nowrap align="right"><label>Información Institucional:</label></td>
              <td align="left" valign="top">
                <input type="checkbox" name="info_pub" id="info_pub" value="1" <?php if ($row_publicaciones['info_pub']==1): ?>
                  checked                  
                <?php endif ?>>
              </td>
            </tr>
            <tr valign="baseline">
              <td align="right" valign="top" nowrap="nowrap">&nbsp;</td>
              <td align="left" valign="top"><input type="submit" value="Modificar" />
			  <input type="hidden" name="id_pub" value="<?php echo $row_publicaciones['id_pub']; ?>" />
              <input name="img_pub1" type="hidden" id="img_pub1" value="<?php echo $row_publicaciones['img_pub']; ?>" />
              <input name="doc_pub1" type="hidden" id="doc_pub1" value="<?php echo $row_publicaciones['doc_pub']; ?>" />
              <input type="hidden" name="MM_update" value="form1" />
            </tr>
          </table>
      </form></td>
    </tr>
    <tr class="tdata">
      <td>&nbsp;</td>
    </tr>
</table></center>
<p>&nbsp;</p>

<?php
mysql_free_result($publicaciones);

mysql_free_result($subcategorias);

mysql_free_result($usuarios);
?>
