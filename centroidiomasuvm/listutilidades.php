<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_utilidades = 10;
$pageNum_utilidades = 0;
if (isset($_GET['pageNum_utilidades'])) {
  $pageNum_utilidades = $_GET['pageNum_utilidades'];
}
$startRow_utilidades = $pageNum_utilidades * $maxRows_utilidades;

mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_utilidades = "SELECT * FROM utilidades ORDER BY id_utilidades ASC";
$query_limit_utilidades = sprintf("%s LIMIT %d, %d", $query_utilidades, $startRow_utilidades, $maxRows_utilidades);
$utilidades = mysql_query($query_limit_utilidades, $centroidiomasuvm) or die(mysql_error());
$row_utilidades = mysql_fetch_assoc($utilidades);

if (isset($_GET['totalRows_utilidades'])) {
  $totalRows_utilidades = $_GET['totalRows_utilidades'];
} else {
  $all_utilidades = mysql_query($query_utilidades);
  $totalRows_utilidades = mysql_num_rows($all_utilidades);
}
$totalPages_utilidades = ceil($totalRows_utilidades/$maxRows_utilidades)-1;

$queryString_utilidades = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_utilidades") == false && 
        stristr($param, "totalRows_utilidades") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_utilidades = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_utilidades = sprintf("&totalRows_utilidades=%d%s", $totalRows_utilidades, $queryString_utilidades);
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/utili.png" />
<h1>Registro de Utilidades</h1>
<table width="90%" border="0" align="center" cellpadding="1" cellspacing="1">
   <tr>
    <td colspan="3" align="left"><a href="utilidades.php?p=nutilidades"><img src="imagenes/new.png" alt="Agregar una nueva Utilidad" width="45" height="46" title="Agregar una nueva Utilidad" /></a></td>
  </tr>
  <tr class="htdata">
    <td width="5%">&nbsp;</td>
    <td width="5%">&nbsp;</td>
    <td>Titulo</td>
    <td>Resumen</td>
    <td>Link</td>
    <td>Imágen</td>
  </tr>
  <?php do { ?>
    <tr class="tdata">
       <td align="center"><a href="utilidades.php?p=modutilidades&id_utilidades=<?php echo $row_utilidades['id_utilidades']; ?>" onClick="return modificar();"><img src="imagenes/mod.png" width="35" height="35" alt="Modificar" title="Modificar"></a></td>
      <td align="center"><a href="delutilidades.php?id_utilidades=<?php echo $row_utilidades['id_utilidades']; ?>" onClick="return eliminar();"><img src="imagenes/del.png" width="35" height="35" alt="Eliminar" title="Eliminar"></a></td>
      <td><?php echo $row_utilidades['titulo_utilidades']; ?></td>
      <td><?php echo $row_utilidades['resumen_utilidades']; ?></td>
      <td><?php echo $row_utilidades['link_utilidades']; ?></td>
      <td><img src="<?php echo $row_utilidades['img_utilidades']; ?>" width="76" height="81" align="absmiddle"/></td>
    </tr>
    <?php } while ($row_utilidades = mysql_fetch_assoc($utilidades)); ?>
</table>
<table border="0">
  <tr>
    <td><?php if ($pageNum_utilidades > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_utilidades=%d%s", $currentPage, 0, $queryString_utilidades); ?>"><img src="imagenes/First.gif" /></a>
        <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_utilidades > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_utilidades=%d%s", $currentPage, max(0, $pageNum_utilidades - 1), $queryString_utilidades); ?>"><img src="imagenes/Previous.gif" /></a>
        <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_utilidades < $totalPages_utilidades) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_utilidades=%d%s", $currentPage, min($totalPages_utilidades, $pageNum_utilidades + 1), $queryString_utilidades); ?>"><img src="imagenes/Next.gif" /></a>
        <?php } // Show if not last page ?></td>
    <td><?php if ($pageNum_utilidades < $totalPages_utilidades) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_utilidades=%d%s", $currentPage, $totalPages_utilidades, $queryString_utilidades); ?>"><img src="imagenes/Last.gif" /></a>
        <?php } // Show if not last page ?></td>
  </tr>
</table>
</center>
<p>&nbsp;</p>
<?php
mysql_free_result($utilidades);
?>
