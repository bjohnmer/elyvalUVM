<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  
  $prefijo = substr(md5(uniqid(rand())),0,6);
  $change = false;
  $change = false;
  	if (!empty($_FILES["img_utilidades"]['name'])) {		
		$imgtamano = $_FILES["img_utilidades"]['size'];
		$imgtipo = $_FILES["img_utilidades"]['type'];
		$imgarchivo = "";
		if ($_FILES['img_utilidades']['name'] != "") {
			if (copy($_FILES['img_utilidades']['tmp_name'],"uploads/img/".$prefijo."_".$_FILES["img_utilidades"]['name'])) {
				$status = "Archivo subido: <b>".$imgarchivo."</b>";
				$imgarchivo = "uploads/img/".$prefijo."_".$_FILES["img_utilidades"]['name'];
			} else {
				$status = "Error al subir el archivo";
			}
		} else {
			$status = "Error al subir archivo";
		}
		$change = true;
	} else {
		$imgarchivo = $_POST['img_utilidades1'];
	}
	
  if (!empty($_FILES["doc_utilidades"]['name'])) {
		$doctamano = $_FILES["doc_utilidades"]['size'];
		$doctipo = $_FILES["doc_utilidades"]['type'];
		$docarchivo = "";
		if ($_FILES['doc_utilidades']['name'] != "") {
			if (copy($_FILES['doc_utilidades']['tmp_name'],"uploads/docs/".$prefijo."_".$_FILES["doc_utilidades"]['name'])) {
				$status = "Archivo subido: <b>".$docarchivo."</b>";
				$docarchivo = "uploads/docs/".$prefijo."_".$_FILES["doc_utilidades"]['name'];
			} else {
				$status = "Error al subir el archivo";
			}
		} else {
			$change = true;
			$status = "Error al subir archivo";
		}
	} else {
		$docarchivo = $_POST['doc_utilidades1'];
	}

  $updateSQL = sprintf("UPDATE utilidades SET titulo_utilidades=%s, resumen_utilidades=%s, link_utilidades=%s, img_utilidades=%s, doc_utilidades=%s, video_utilidades=%s WHERE id_utilidades=%s",
                       GetSQLValueString($_POST['titulo_utilidades'], "text"),
                       GetSQLValueString($_POST['resumen_utilidades'], "text"),
                       GetSQLValueString($_POST['link_utilidades'], "text"),
                       GetSQLValueString($imgarchivo,"text"),
                       GetSQLValueString($docarchivo,"text"),
                       GetSQLValueString($_POST['video_utilidades'], "text"),
                       GetSQLValueString($_POST['id_utilidades'], "int"));
					   
	if ($change) {
		unlink($_REQUEST['img_utilidades1']);
	}
	if ($change) {
	  	unlink($_REQUEST['doc_utilidades1']);
	}

  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
  $Result1 = mysql_query($updateSQL, $centroidiomasuvm) or die(mysql_error());

  $updateGoTo = "utilidades.php?p=listutilidades";
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_utilidades = "-1";
if (isset($_GET['id_utilidades'])) {
  $colname_utilidades = $_GET['id_utilidades'];
}
mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_utilidades = sprintf("SELECT * FROM utilidades WHERE id_utilidades = %s", GetSQLValueString($colname_utilidades, "int"));
$utilidades = mysql_query($query_utilidades, $centroidiomasuvm) or die(mysql_error());
$row_utilidades = mysql_fetch_assoc($utilidades);
$totalRows_utilidades = mysql_num_rows($utilidades);
?>

<center>
<p>&nbsp;</p>
<img src="imagenes/utili.png" />
<h1>Modificar Utilidad</h1>
<table width="60%" border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td colspan="3" align="left"><a href="utilidades.php?p=listutilidades"><img src="imagenes/atras.png" alt="Atrás" width="45" height="46" title="Atrás" /></a></td>
  </tr>
  <tr class="htdata">
    <td>&nbsp;</td>
  </tr>
<tr class="tdata">
<td><form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table align="center">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Titulo:</label></td>
      <td><input type="text" name="titulo_utilidades" value="<?php echo htmlentities($row_utilidades['titulo_utilidades'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap"><label>Resumen:</label></td>
      <td><textarea name="resumen_utilidades" cols="52" rows="20"><?php echo htmlentities($row_utilidades['resumen_utilidades'], ENT_COMPAT, 'utf-8'); ?></textarea></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Link:</label></td>
      <td><input type="text" name="link_utilidades" value="<?php echo htmlentities($row_utilidades['link_utilidades'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Imágen:</label></td>
      <td><input type="file" name="img_utilidades" value="<?php echo htmlentities($row_utilidades['img_utilidades'], ENT_COMPAT, 'utf-8'); ?>" size="32" />
      <p><img src="<?php echo $row_utilidades['img_utilidades']; ?>" width="169" height="169" /></p></td>
      </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Documento:</label></td>
      <td><input type="file" name="doc_utilidades" value="<?php echo htmlentities($row_utilidades['doc_utilidades'], ENT_COMPAT, 'utf-8'); ?>" size="32" />
      <p><a href="<?php echo $row_utilidades['doc_utilidades']; ?>" target="_blank"><?php echo $row_utilidades['doc_utilidades']; ?></a></p>
      </td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap"><label>Video:</label></td>
      <td><textarea name="video_utilidades" cols="50"><?php echo htmlentities($row_utilidades['video_utilidades'], ENT_COMPAT, 'utf-8'); ?></textarea>
     </p>
     <p>Formato permitido:</p>
     <p>http://www.youtube.com/embed/ejemplo</p></td>
    </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right">&nbsp;</td>
              <td align="left" valign="top"><input type="submit" value="Modificar" />
              <input name="id_utilidades" type="hidden" id="id_utilidades" value="<?php echo $row_utilidades['id_utilidades']; ?>" />
              <input name="img_utilidades1" type="hidden" id="img_utilidades1" value="<?php echo $row_utilidades['img_utilidades']; ?>" />
              <input name="doc_utilidades1" type="hidden" id="doc_utilidades1" value="<?php echo $row_utilidades['doc_utilidades']; ?>" />
            </tr>
          </table>
          <input type="hidden" name="MM_update" value="form1" />
        </form>
</td>
</tr>
</table>
</center>
<p>&nbsp;</p>
<?php
mysql_free_result($utilidades);
?>
