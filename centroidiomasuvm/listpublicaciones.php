<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_publicaciones = 10;
$pageNum_publicaciones = 0;
if (isset($_GET['pageNum_publicaciones'])) {
  $pageNum_publicaciones = $_GET['pageNum_publicaciones'];
}
$startRow_publicaciones = $pageNum_publicaciones * $maxRows_publicaciones;

mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
if ($_SESSION['tipo_user']=="Administrador") {
  $query_publicaciones = "SELECT a.*, b.nombre_sub_cat, c.nombre_usuario, c.apellido_usuario FROM publicaciones as a, sub_categoria as b, usuarios as c WHERE a.id_sub_cat = b.id_sub_cat AND a.id_usuario = c.id_usuario ORDER BY a.id_pub DESC";
}
else
{
  $query_publicaciones = "SELECT a.*, b.nombre_sub_cat, c.nombre_usuario, c.apellido_usuario FROM publicaciones as a, sub_categoria as b, usuarios as c WHERE a.id_usuario =".$_SESSION['id_user']." AND a.id_sub_cat = b.id_sub_cat AND a.id_usuario = c.id_usuario ORDER BY a.id_pub DESC";
}

$query_limit_publicaciones = sprintf("%s LIMIT %d, %d", $query_publicaciones, $startRow_publicaciones, $maxRows_publicaciones);
$publicaciones = mysql_query($query_limit_publicaciones, $centroidiomasuvm) or die(mysql_error());
$row_publicaciones = mysql_fetch_assoc($publicaciones);

if (isset($_GET['totalRows_publicaciones'])) {
  $totalRows_publicaciones = $_GET['totalRows_publicaciones'];
}
else
{
  $all_publicaciones = mysql_query($query_publicaciones);
  $totalRows_publicaciones = mysql_num_rows($all_publicaciones);
}
$totalPages_publicaciones = ceil($totalRows_publicaciones/$maxRows_publicaciones)-1;

$queryString_publicaciones = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_publicaciones") == false && 
        stristr($param, "totalRows_publicaciones") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_publicaciones = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_publicaciones = sprintf("&totalRows_publicaciones=%d%s", $totalRows_publicaciones, $queryString_publicaciones);
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/public.png" />
<h1>Registro de Publicaciones</h1>
<table width="90%" border="0" align="center" cellpadding="1" cellspacing="1">
   <tr>
    <td colspan="3" align="left"><a href="publicaciones.php?p=npublicacion"><img src="imagenes/new.png" alt="Agregar una nueva Publicacion" width="45" height="46" title="Agregar una nueva Publicacion" /></a></td>
  </tr>
  <tr class="htdata">
    <td width="5%">&nbsp;</td>
    <td width="5%">&nbsp;</td>
    <td width="20%">Titulo</td>
    <td width="10%">Fecha y Hora</td>
    <td width="40%">Resumen</td>
    <td width="10%">Subcategoria</td>
    <td width="10%">Usuario</td>
  </tr>
  <?php do { ?>
    <tr class="tdata">
      <td align="center"><a href="publicaciones.php?p=modpublicaciones&id_pub=<?php echo $row_publicaciones['id_pub']; ?>" onClick="return modificar();"><img src="imagenes/mod.png" width="35" height="35" alt="Modificar" title="Modificar"></a></td>
      <td align="center"><a href="delpublicacion.php?id_pub=<?php echo $row_publicaciones['id_pub']; ?>" onClick="return eliminar();"><img src="imagenes/del.png" width="35" height="35" alt="Eliminar" title="Eliminar"></a></td>
      <td><?php echo $row_publicaciones['titulo_pub']; ?></td>
      <td><?php echo $row_publicaciones['fechahora_pub']; ?></td>
      <td><?php echo substr($row_publicaciones['resumen_pub'], 0, 150); ?></td>
      <td><?php echo $row_publicaciones['nombre_sub_cat']; ?></td>
      <td><?php echo $row_publicaciones['nombre_usuario']; ?> <?php echo $row_publicaciones['apellido_usuario']; ?></td>
    </tr>
    <?php } while ($row_publicaciones = mysql_fetch_assoc($publicaciones)); ?>
</table>
<table border="0">
  <tr>
    <td><?php if ($pageNum_publicaciones > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_publicaciones=%d%s", $currentPage, 0, $queryString_publicaciones); ?>"><img src="imagenes/First.gif" /></a>
    <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_publicaciones > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_publicaciones=%d%s", $currentPage, max(0, $pageNum_publicaciones - 1), $queryString_publicaciones); ?>"><img src="imagenes/Previous.gif" /></a>
    <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_publicaciones < $totalPages_publicaciones) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_publicaciones=%d%s", $currentPage, min($totalPages_publicaciones, $pageNum_publicaciones + 1), $queryString_publicaciones); ?>"><img src="imagenes/Next.gif" /></a>
    <?php } // Show if not last page ?></td>
    <td><?php if ($pageNum_publicaciones < $totalPages_publicaciones) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_publicaciones=%d%s", $currentPage, $totalPages_publicaciones, $queryString_publicaciones); ?>"><img src="imagenes/Last.gif" /></a>
    <?php } // Show if not last page ?></td>
  </tr>
</table>
</center>
<p>&nbsp;</p>
<?php
mysql_free_result($publicaciones);
?>
