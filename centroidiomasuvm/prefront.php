<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Centro de Iidiomas UVM</title>
<link rel="shortcut icon" HREF="imagenes/logo.ico" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/funciones.js"></script>



<link rel="stylesheet" type="text/css" href="css/style-slider.css" />

    <script type="text/javascript" src="js-slider/jquery-1.2.6.min.js"></script>
    <script type="text/javascript" src="js-slider/jquery-easing-1.3.pack.js"></script>
    <script type="text/javascript" src="js-slider/jquery-easing-compatibility.1.2.pack.js"></script>
    <script type="text/javascript" src="js-slider/coda-slider.1.1.1.pack.js"></script>

    <script type="text/javascript">
        
            var theInt = null;
            var $crosslink, $navthumb;
            var curclicked = 0;
            
            theInterval = function(cur){
                clearInterval(theInt);
                
                if( typeof cur != 'undefined' )
                    curclicked = cur;
                
                $crosslink.removeClass("active-thumb");
                $navthumb.eq(curclicked).parent().addClass("active-thumb");
                    $(".stripNav ul li a").eq(curclicked).trigger('click');
                
                theInt = setInterval(function(){
                    $crosslink.removeClass("active-thumb");
                    $navthumb.eq(curclicked).parent().addClass("active-thumb");
                    $(".stripNav ul li a").eq(curclicked).trigger('click');
                    curclicked++;
                    if( 6 == curclicked )
                        curclicked = 0;
                    
                }, 3000);
            };
            
            $(function(){
                
                $("#main-photo-slider").codaSlider();
                
                $navthumb = $(".nav-thumb");
                $crosslink = $(".cross-link");
                
                $navthumb
                .click(function() {
                    var $this = $(this);
                    theInterval($this.parent().attr('href').slice(1) - 1);
                    return false;
                });
                
                theInterval();
            });
    </script>


</head>
<body>
<div id="pagina">
	<div id="top">
        <?php require_once("menu.php"); ?>
        <div id="fondoheader">
        	<?php require_once("header.php");?>
        </div>
    </div>
    <div id="cuerpoizq" class="cuerpoizq-sub">
       <?php require_once("preinsfront.php");?>
    </div>
    
    <div id="cuerpoder">
        <?php require_once("redessociales.php");?>
        <?php require_once("linksociales.php");?>
    </div>
    <div id="pie">
        <?php require_once("pie.php");?>
    </div>
</div>  
</body>
</html>