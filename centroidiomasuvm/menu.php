<style type="text/css">
<!--

a, a:hover, a:active, a:focus {
 outline:0; 
 direction:ltr;
}
 
.wrapper {
 position:relative; 
}
 
.mainmenu { 
 font-family:Arial;
 font-weight:normal;
 font-size:80%;
 line-height:35px; 
 width:1100px;
 text-align:center;
} 

ul.menu {
 padding:0; 
 margin:0; 
 list-style:none; 
 width:149px; 
 overflow:hidden; 
 float:left; 
 margin-right:5px;
} 

ul.menu a {
 text-decoration:none; 
 color:#fff;
}
 
ul.menu li.list {
float:left;
width:300px;
margin:-32767px -135px 0px 0px;
background:url(imagenes/top.png) no-repeat left bottom;
}

ul.menu li.list a.category {
position:relative;
display:block;
float:left;
width:150px;
margin-top:32767px;
background:transparent;
}

ul.menu li.list a.category:hover,
ul.menu li.list a.category:focus,
ul.menu li.list a.category:active {
 margin-right:1px;
 background-image:url(imagenes/tophover.png);
 background-repeat:no-repeat;
 background-position:left top;
 color:#000;
}

ul.submenu {
 float:left; 
 padding:35px 0px 0px 0px; 
 margin:0; 
 list-style:none; 
 background-image:url(imagenes/tophover.png);
 background-repeat:no-repeat;
 background-position:left top;
 margin:-35px 0px 0px 0px;
}
 
ul.submenu li a {
float:left;
width:150px;
background: #ACACAC;
clear:left;
color:#fff;
}

ul.submenu li a.endlist {
 background:url(imagenes/bottom.png);
}
 
ul.submenu li a.endlist:hover,
ul.submenu li a.endlist:focus,
ul.submenu li a.endlist:active {
background:url(imagenes/bottomhover.png);
}
 
ul.submenu a:hover,
ul.submenu a:focus,
ul.submenu a:active {
background: #FFF;
margin-right:1px;
 color:#000;
}
-->


</style>


<div  style="margin: 5px auto 35px 50px;" class="wrapper">
	<div class="mainmenu">
		<ul class="menu">
			<li class="list">
 				<a  class="category" href="index.php">INICIO</a>
			</li>
		</ul>
		<ul class="menu">
			<li class="list">
 				<a class="category" href="quienessomos.php">QUIENES SOMOS</a>
         	</li>
        </ul>
		<ul class="menu">
			<li class="list">
 				<a class="category" href="idiomas.php">IDIOMAS</a>
			</li>
		</ul>
		<ul class="menu">
			<li class="list">
 				<a class="category" href="utilidadesfront.php">UTILIDADES</a>
			</li>
 		</ul>
 		<ul class="menu">
			<li class="list">
 				<a class="category" href="info.php">INFORMACIÓN</a>
			</li>
 		</ul>
		<ul class="menu">
			<li class="list">
 				<a class="category" href="contactos.php">CONTACTANOS</a>
			</li>
		</ul>
<!-- end mainmenu --></div>
<!-- end wrapper --></div>