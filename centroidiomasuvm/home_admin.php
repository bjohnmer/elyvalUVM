<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_usuarios = "-1";
if (isset($_SESSION['id_user'])) {
  $colname_usuarios = $_SESSION['id_user'];
}
mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_usuarios = sprintf("SELECT * FROM usuarios WHERE id_usuario = %s", GetSQLValueString($colname_usuarios, "int"));
$usuarios = mysql_query($query_usuarios, $centroidiomasuvm) or die(mysql_error());
$row_usuarios = mysql_fetch_assoc($usuarios);
$totalRows_usuarios = mysql_num_rows($usuarios);
?>

<div id="bloqueizq">
	<div id="sesion">Usted ha Iniciado Sesión como:</br>
    	<p style="color:#424242"><?php echo $row_usuarios['login_usuario']; ?> | <?php echo $row_usuarios['tipo_usuario']; ?></p>
    </div>
    <div>
    	<p><img align="right" style="border: 3px solid #fff; margin-left:15px; margin-right:15px;" src="<?php if (file_exists($row_usuarios['img_usuario'])) { echo $row_usuarios['img_usuario']; } else { echo "imagenes/user.png";}?>" width="152" height="161"></p>
    </div>
    <div id="infopersonal">Informacón Personal</div>
    <div id="personal">
		<?php echo $row_usuarios['nombre_usuario']; ?></br>
        <?php echo $row_usuarios['apellido_usuario']; ?></br></br>
        <?php echo $row_usuarios['ocupacion']; ?></br></br>
    </div>
</div>
<div id="bloqueder">
    <div id="descpersonal">Descripción Personal</div>
    <div id="personal">
		<?php echo $row_usuarios['descripcion_usuario']; ?>
    </div>
    <div id="descpersonal">Información de Contacto</div>
    <div id="personal">
		<?php echo $row_usuarios['email_usuario']; ?></br></br>
        <?php echo $row_usuarios['telefono']; ?>
    </div>
</div>
<?php
mysql_free_result($usuarios);
?>
