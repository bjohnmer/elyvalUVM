<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO categorias (id_categoria, nombre_categoria, descripcion_categoria) VALUES (%s, %s, %s)",
                       GetSQLValueString($_POST['id_categoria'], "int"),
                       GetSQLValueString($_POST['nombre_categoria'], "text"),
                       GetSQLValueString($_POST['descripcion_categoria'], "text"));

  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
  $Result1 = mysql_query($insertSQL, $centroidiomasuvm) or die(mysql_error());

  $insertGoTo = "categorias.php?p=listcat";
  header(sprintf("Location: %s", $insertGoTo));
}
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/cat.png" />
<h1>Nueva Categoría</h1>
<table width="60%" border="0" cellpadding="1" cellspacing="1">
   <tr>
    <td colspan="3" align="left"><a href="categorias.php?p=listcat"><img src="imagenes/atras.png" alt="Atrás" width="45" height="46" title="Atrás" /></a></td>
  </tr>
  <tr class="htdata">
    <td>&nbsp;</td>
  </tr>
  <tr class="tdata">
        <td><form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
          <table align="center">
            <tr valign="baseline">
              <td align="right" valign="middle" nowrap="nowrap"><label>Nombre de la Categoria:</label></td>
              <td align="left"><input type="text" name="nombre_categoria" value="" size="32" /></td>
            </tr>
            <tr valign="baseline">
              <td align="right" valign="middle" nowrap="nowrap"><label>Descripcion de la Categoria:</label></td>
              <td align="left"><textarea name="descripcion_categoria" cols="32" rows="5"></textarea></td>
            </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right">&nbsp;</td>
              <td align="left"><input type="submit" value="Guardar" /></td>
            </tr>
          </table>
          <input type="hidden" name="MM_insert" value="form1" />
        </form></td>
    </tr>
</table>
</center>  
<p>&nbsp;</p>
