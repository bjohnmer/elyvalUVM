<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
 	$prefijo = substr(md5(uniqid(rand())),0,6);
	$imgtamano = $_FILES["img_link"]['size'];
	$imgtipo = $_FILES["img_link"]['type'];
	$imgarchivo = "";
	
	if ($_FILES['img_link']['name'] != "") {
		// guardamos el archivo a la carpeta files
		if (copy($_FILES['img_link']['tmp_name'],"uploads/img/".$prefijo."_".$_FILES["img_link"]['name'])) {
			$status = "Archivo subido: <b>".$imgarchivo."</b>";
			$imgarchivo = "uploads/img/".$prefijo."_".$_FILES["img_link"]['name'];
		} else {
			$status = "Error al subir el archivo";
		}
	} else {
		$status = "Error al subir archivo";
	}

  $insertSQL = sprintf("INSERT INTO links (id_links, nombre_link, enlace_link, img_link) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($_POST['id_links'], "int"),
                       GetSQLValueString($_POST['nombre_link'], "text"),
                       GetSQLValueString($_POST['enlace_link'], "text"),
                       GetSQLValueString($imgarchivo, "text"));

  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
  $Result1 = mysql_query($insertSQL, $centroidiomasuvm) or die(mysql_error());

  $insertGoTo = "links.php?p=listlinks";
  header(sprintf("Location: %s", $insertGoTo));
}
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/utili.png" />
<h1>Nuevo Link</h1>
<table width="60%" border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td colspan="3" align="left"><a href="links.php?p=listlinks"><img src="imagenes/atras.png" alt="Atrás" width="45" height="46" title="Atrás" /></a></td>
  </tr>
  <tr class="htdata">
    <td>&nbsp;</td>
  </tr>
<tr class="tdata">
<td><form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table align="center">
    <tr valign="baseline">
      <td align="right" valign="top" nowrap><label>Nombre del Link:</label></td>
      <td><textarea name="nombre_link" cols="60" rows="2"></textarea></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap><label>Enlace Web:</label></td>
      <td><textarea name="enlace_link" cols="60" rows="2"></textarea></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="top" nowrap><label>Imágen:</label></td>
      <td><input type="file" name="img_link" value="" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">&nbsp;</td>
      <td><input type="submit" value="Guardar"></td>
    </tr>
  </table>
  <input type="hidden" name="MM_insert" value="form1">
</form></td>
</tr>
</table>
</center>
<p>&nbsp;</p>
