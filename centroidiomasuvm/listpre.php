<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_preinscripcion = 10;
$pageNum_preinscripcion = 0;
if (isset($_GET['pageNum_preinscripcion'])) {
  $pageNum_preinscripcion = $_GET['pageNum_preinscripcion'];
}
$startRow_preinscripcion = $pageNum_preinscripcion * $maxRows_preinscripcion;

mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_preinscripcion = "SELECT a.*, b.nombre_sub_cat FROM preinscripcion as a,  sub_categoria as b WHERE a.id_sub_cat = b.id_sub_cat ORDER BY a.id_sub_cat ASC";
$query_limit_preinscripcion = sprintf("%s LIMIT %d, %d", $query_preinscripcion, $startRow_preinscripcion, $maxRows_preinscripcion);
$preinscripcion = mysql_query($query_limit_preinscripcion, $centroidiomasuvm) or die(mysql_error());
$row_preinscripcion = mysql_fetch_assoc($preinscripcion);

if (isset($_GET['totalRows_preinscripcion'])) {
  $totalRows_preinscripcion = $_GET['totalRows_preinscripcion'];
} else {
  $all_preinscripcion = mysql_query($query_preinscripcion);
  $totalRows_preinscripcion = mysql_num_rows($all_preinscripcion);
}
$totalPages_preinscripcion = ceil($totalRows_preinscripcion/$maxRows_preinscripcion)-1;

$queryString_preinscripcion = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_preinscripcion") == false && 
        stristr($param, "totalRows_preinscripcion") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_preinscripcion = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_preinscripcion = sprintf("&totalRows_preinscripcion=%d%s", $totalRows_preinscripcion, $queryString_preinscripcion);
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/prein.png" />
<h1>Registro de Preinscripciones</h1>
<table width="90%" border="0" align="center" cellpadding="1" cellspacing="1">
   <tr>
    <td colspan="3" align="left">
      <a href="preinscripcion.php?p=npre">
        <img src="imagenes/new.png" alt="Agregar una nueva Preinscripcion" width="45" height="46" title="Agregar una nueva Preinscripcion" />
      </a>
      <a onclick="window.print();return false;" href="#">
        IMPRIMIR
      </a>
    </td>
  </tr>
  <tr class="htdata">
    <td width="5%">&nbsp;</td>
    <td width="5%">&nbsp;</td>
    <td>Subcategoria</td>
    <td>Nombre</td>
    <td>Apellido</td>
    <td>Cédula de Identidad</td>
    <td>Email</td>
    <td>Horario</td>
    <td>Carrera</td>
    <td>Fecha</td>
  </tr>
  <?php do { ?>
    <tr class="tdata">
       <td align="center"><a href="preinscripcion.php?p=modpre&id_pre=<?php echo $row_preinscripcion['id_pre']; ?>" onClick="return modificar();"><img src="imagenes/mod.png" width="35" height="35" alt="Modificar" title="Modificar"></a></td>
      <td align="center"><a href="delpre.php?id_pre=<?php echo $row_preinscripcion['id_pre']; ?>" onClick="return eliminar();"><img src="imagenes/del.png" width="35" height="35" alt="Eliminar" title="Eliminar"></a></td>
      <td><?php echo $row_preinscripcion['nombre_sub_cat']; ?></td>
      <td><?php echo $row_preinscripcion['nombre_pre']; ?></td>
      <td><?php echo $row_preinscripcion['apellido_pre']; ?></td>
      <td><?php echo $row_preinscripcion['cedula_pre']; ?></td>
      <td><?php echo $row_preinscripcion['email']; ?></td>
      <td><?php echo $row_preinscripcion['horario']; ?></td>
      <td><?php echo $row_preinscripcion['carrera']; ?></td>
      <td><?php echo $row_preinscripcion['fecha_pre']; ?></td>
    </tr>
    <?php } while ($row_preinscripcion = mysql_fetch_assoc($preinscripcion)); ?>
</table>
<table border="0">
  <tr>
    <td><?php if ($pageNum_preinscripcion > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_preinscripcion=%d%s", $currentPage, 0, $queryString_preinscripcion); ?>"><img src="imagenes/First.gif" /></a>
        <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_preinscripcion > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_preinscripcion=%d%s", $currentPage, max(0, $pageNum_preinscripcion - 1), $queryString_preinscripcion); ?>"><img src="imagenes/Previous.gif" /></a>
        <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_preinscripcion < $totalPages_preinscripcion) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_preinscripcion=%d%s", $currentPage, min($totalPages_preinscripcion, $pageNum_preinscripcion + 1), $queryString_preinscripcion); ?>"><img src="imagenes/Next.gif" /></a>
        <?php } // Show if not last page ?></td>
    <td><?php if ($pageNum_preinscripcion < $totalPages_preinscripcion) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_preinscripcion=%d%s", $currentPage, $totalPages_preinscripcion, $queryString_preinscripcion); ?>"><img src="imagenes/Last.gif" /></a>
        <?php } // Show if not last page ?></td>
  </tr>
</table>
</center>
<p>&nbsp;</p>
<?php
mysql_free_result($preinscripcion);
?>
