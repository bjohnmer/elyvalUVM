-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 12, 2013 at 01:10 PM
-- Server version: 5.5.29
-- PHP Version: 5.3.10-1ubuntu3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `idiomasuvm`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_categoria` varchar(50) NOT NULL,
  `descripcion_categoria` varchar(150) NOT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `categorias`
--

INSERT INTO `categorias` (`id_categoria`, `nombre_categoria`, `descripcion_categoria`) VALUES
(1, 'Idiomas', 'Conforma por todos los niveles de Idiomas'),
(2, 'Novedades', 'Espacio dedicado para publicaciones acerca de noticias de cualquier Ã­ndole '),
(3, 'American Corners', 'Espacio dedicado a publicaciones Ãºnica y exclusivamente relacionadas con American Corners');

-- --------------------------------------------------------

--
-- Table structure for table `imagenes`
--

CREATE TABLE IF NOT EXISTS `imagenes` (
  `id_img` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_img` varchar(250) NOT NULL,
  `descripcion_img` text NOT NULL,
  `archivoimg` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id_img`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `imagenes`
--

INSERT INTO `imagenes` (`id_img`, `nombre_img`, `descripcion_img`, `archivoimg`) VALUES
(2, 'ImÃ¡gen 2', 'ImÃ¡gen 2', 'uploads/img/224e67_back-to-the-future-3-in-1-1024-x-768.jpg'),
(3, 'ImÃ¡gen 3', 'ImÃ¡gen 3', 'uploads/img/a38cd2_cyan.png'),
(4, 'ImÃ¡gen1', 'ImÃ¡gen1', 'uploads/img/ad26c7_harrypotterscar.png'),
(5, 'imÃ¡gen 4', 'imÃ¡gen 4', 'uploads/img/4a4ebb_b&w.png'),
(6, 'ImÃ¡gen 5', 'imÃ¡gen 5', 'uploads/img/112c40_1920x1080p.jpg'),
(7, 'ImÃ¡gen 6', 'imÃ¡gen 6', 'uploads/img/514f10_blurdm_desktop.png'),
(8, 'ImÃ¡gen 7', 'imÃ¡gen 7', 'uploads/img/219b2e_94a3d049ea88f6de_linus2.gif');

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `id_links` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_link` varchar(150) NOT NULL,
  `enlace_link` varchar(300) NOT NULL,
  `img_link` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`id_links`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`id_links`, `nombre_link`, `enlace_link`, `img_link`) VALUES
(6, 'Universidad Valle del Momboy', 'www.uvm.edu.ve', 'uploads/img/6d4081_LOGO UVM COLOR RGB-01.jpg'),
(7, 'Aula Virtual', 'aulavirtual.uvm.edu.ve', 'uploads/img/c73851_1346564514_wlassistant.png'),
(10, 'Hola', 'www.uvm.edu.ve', 'uploads/img/32a1b8_1346564717_iphone 4G headphones shadow.png');

-- --------------------------------------------------------

--
-- Table structure for table `preinscripcion`
--

CREATE TABLE IF NOT EXISTS `preinscripcion` (
  `id_pre` int(11) NOT NULL AUTO_INCREMENT,
  `id_sub_cat` int(11) NOT NULL,
  `nombre_pre` varchar(100) NOT NULL,
  `apellido_pre` varchar(100) NOT NULL,
  `cedula_pre` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `horario` varchar(70) NOT NULL,
  `carrera` varchar(50) DEFAULT NULL,
  `fecha_pre` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pre`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `preinscripcion`
--

INSERT INTO `preinscripcion` (`id_pre`, `id_sub_cat`, `nombre_pre`, `apellido_pre`, `cedula_pre`, `email`, `horario`, `carrera`, `fecha_pre`) VALUES
(2, 1, 'kjh kdjha kdjhsa', 'lkjf sdljflds', '123234435', 'jlaksdjlaskd@kalksjda.com', 'Nocturno: 06:00 pm - 08:00 pm', NULL, '2013-03-12 17:15:49'),
(3, 1, 'kjsdh fkjhsd fkhjdsk', 'ksdjhf ksdjhf kjsdhk', '5142641', 'kjshfksd@sdjhfksd.com', 'Sabatino: 08:00 am - 04:15 pm', NULL, '2013-03-12 17:28:16'),
(4, 1, 'sdfds', 'sdfds', '5435435', 'dsfds@sdfds.com', 'Nocturno: 06:00 pm - 08:00 pm', NULL, '2013-03-12 17:29:18'),
(5, 1, 'TRER', 'ERTRE', '34543', 'dsfds@sdfds.com', 'Vespertino: 02:00 pm - 04:00 pm', NULL, '2013-03-12 17:31:39'),
(6, 1, 'TRERwerewr', 'werewrewr', '43534', 'fdgfd@sdfdsd.com', 'Vespertino: 04:00 pm - 06:00 pm', NULL, '2013-03-12 17:34:48'),
(7, 1, 'cvcxvcx', 'xcvcxv', '45654', 'etretre', 'Vespertino: 02:00 pm - 04:00 pm', NULL, '2013-03-12 17:36:21'),
(8, 1, 'dddd', 'ddddd', '55555', 'sdfdsfs', 'Matutino: 8:00 am - 10:00 am', NULL, '2013-03-12 17:37:03');

-- --------------------------------------------------------

--
-- Table structure for table `publicaciones`
--

CREATE TABLE IF NOT EXISTS `publicaciones` (
  `id_pub` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_pub` varchar(250) NOT NULL,
  `fechahora_pub` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `img_pub` varchar(80) DEFAULT NULL,
  `doc_pub` text,
  `resumen_pub` text NOT NULL,
  `id_sub_cat` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `info_pub` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_pub`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `publicaciones`
--

INSERT INTO `publicaciones` (`id_pub`, `titulo_pub`, `fechahora_pub`, `img_pub`, `doc_pub`, `resumen_pub`, `id_sub_cat`, `id_usuario`, `info_pub`) VALUES
(10, 'Inscripciones Abiertas Idiomas I', '2013-03-12 17:39:48', 'uploads/img/0385da_imagen2.jpg', '', 'Idioma o lengua, es un sistema de comunicaciÃ³n verbal o gestual propio de una comunidad humana. En la prÃ¡ctica, hay idiomas muy similares entre sÃ­, a los que se llama dialectos, o mÃ¡s propiamente variedades lingÃ¼Ã­sticas, mutuamente inteligibles. La determinaciÃ³n de si dos de esas variedades son parte o no del mismo idioma, es mÃ¡s una cuestiÃ³n socio-polÃ­tica que lingÃ¼Ã­stica.\r\nIdioma o lengua, es un sistema de comunicaciÃ³n verbal o gestual propio de una comunidad humana. En la prÃ¡ctica, hay idiomas muy similares entre sÃ­, a los que se llama dialectos, o mÃ¡s propiamente variedades lingÃ¼Ã­sticas, mutuamente inteligibles. La determinaciÃ³n de si dos de esas variedades son parte o no del mismo idioma, es mÃ¡s una cuestiÃ³n socio-polÃ­tica que lingÃ¼Ã­stica.\r\nIdioma o lengua, es un sistema de comunicaciÃ³n verbal o gestual propio de una comunidad humana. En la prÃ¡ctica, hay idiomas muy similares entre sÃ­, a los que se llama dialectos, o mÃ¡s propiamente variedades lingÃ¼Ã­sticas, mutuamente inteligibles. La determinaciÃ³n de si dos de esas variedades son parte o no del mismo idioma, es mÃ¡s una cuestiÃ³n socio-polÃ­tica que lingÃ¼Ã­stica.', 7, 1, 0),
(11, 'Prueba', '2012-11-25 19:31:46', 'uploads/img/a80f12_24738-vl.jpg', '', 'Idioma o lengua, es un sistema de comunicaciÃ³n verbal o gestual propio de una comunidad humana. En la prÃ¡ctica, hay idiomas muy similares entre sÃ­, a los que se llama dialectos, o mÃ¡s propiamente variedades lingÃ¼Ã­sticas, mutuamente inteligibles. La determinaciÃ³n de si dos de esas variedades son parte o no del mismo idioma, es mÃ¡s una cuestiÃ³n socio-polÃ­tica que lingÃ¼Ã­stica.\r\nIdioma o lengua, es un sistema de comunicaciÃ³n verbal o gestual propio de una comunidad humana. En la prÃ¡ctica, hay idiomas muy similares entre sÃ­, a los que se llama dialectos, o mÃ¡s propiamente variedades lingÃ¼Ã­sticas, mutuamente inteligibles. La determinaciÃ³n de si dos de esas variedades son parte o no del mismo idioma, es mÃ¡s una cuestiÃ³n socio-polÃ­tica que lingÃ¼Ã­stica.\r\nIdioma o lengua, es un sistema de comunicaciÃ³n verbal o gestual propio de una comunidad humana. En la prÃ¡ctica, hay idiomas muy similares entre sÃ­, a los que se llama dialectos, o mÃ¡s propiamente variedades lingÃ¼Ã­sticas, mutuamente inteligibles. La determinaciÃ³n de si dos de esas variedades son parte o no del mismo idioma, es mÃ¡s una cuestiÃ³n socio-polÃ­tica que lingÃ¼Ã­stica.', 6, 1, 0),
(12, 'Prueba', '2012-11-25 19:32:39', 'uploads/img/b821b4_banderas-del-mundo.jpg', '', 'Idioma o lengua, es un sistema de comunicaciÃ³n verbal o gestual propio de una comunidad humana. En la prÃ¡ctica, hay idiomas muy similares entre sÃ­, a los que se llama dialectos, o mÃ¡s propiamente variedades lingÃ¼Ã­sticas, mutuamente inteligibles. La determinaciÃ³n de si dos de esas variedades son parte o no del mismo idioma, es mÃ¡s una cuestiÃ³n socio-polÃ­tica que lingÃ¼Ã­stica.\r\nIdioma o lengua, es un sistema de comunicaciÃ³n verbal o gestual propio de una comunidad humana. En la prÃ¡ctica, hay idiomas muy similares entre sÃ­, a los que se llama dialectos, o mÃ¡s propiamente variedades lingÃ¼Ã­sticas, mutuamente inteligibles. La determinaciÃ³n de si dos de esas variedades son parte o no del mismo idioma, es mÃ¡s una cuestiÃ³n socio-polÃ­tica que lingÃ¼Ã­stica.\r\nIdioma o lengua, es un sistema de comunicaciÃ³n verbal o gestual propio de una comunidad humana. En la prÃ¡ctica, hay idiomas muy similares entre sÃ­, a los que se llama dialectos, o mÃ¡s propiamente variedades lingÃ¼Ã­sticas, mutuamente inteligibles. La determinaciÃ³n de si dos de esas variedades son parte o no del mismo idioma, es mÃ¡s una cuestiÃ³n socio-polÃ­tica que lingÃ¼Ã­stica.', 6, 1, 0),
(13, 'Inscripciones Abiertas Idiomas I Inscripciones Abiertas Idiomas I', '2012-11-25 20:38:03', 'uploads/img/dba48a_1346563902_PortableComputer.png', '', 'Su definiciÃ³n sostiene que para conseguir buenos niveles de desarrollo humano en una comunidad, es imperativo que esta se desenvuelva en un clima de confianza entre sus miembros, ademÃ¡s de las aptitudes y el liderazgo de un grupo o conglomerado social para aprovechar los valores y recursos favorables al desarrollo. El capital social incluye a las organizaciones, a las instituciones, los valores de la Ã©tica, la libertad, la democracia, la calidad de la educaciÃ³n, las tecnologÃ­as de la informaciÃ³n y de las comunicaciones, el Estado de Derecho y las muchas otras dimensiones de carÃ¡cter cualitativo. Su definiciÃ³n sostiene que para conseguir buenos niveles de desarrollo humano en una comunidad, es imperativo que esta se desenvuelva en un clima de confianza entre sus miembros, ademÃ¡s de las aptitudes y el liderazgo de un grupo o conglomerado social para aprovechar los valores y recursos favorables al desarrollo. El capital social incluye a las organizaciones, a las instituciones, los valores de la Ã©tica, la libertad, la democracia, la calidad de la educaciÃ³n, las tecnologÃ­as de la informaciÃ³n y de las comunicaciones, el Estado de Derecho y las muchas otras dimensiones de carÃ¡cter cualitativo. ', 1, 1, 0),
(14, 'Inscripciones Abiertas Idiomas XXXXXXX', '2013-03-12 16:07:13', 'uploads/img/dba48a_1346563902_PortableComputer.png', NULL, 'Su definiciÃ³n sostiene que para conseguir buenos niveles de desarrollo humano en una comunidad, es imperativo que esta se desenvuelva en un clima de confianza entre sus miembros, ademÃ¡s de las aptitudes y el liderazgo de un grupo o conglomerado social para aprovechar los valores y recursos favorables al desarrollo. El capital social incluye a las organizaciones, a las instituciones, los valores de la Ã©tica, la libertad, la democracia, la calidad de la educaciÃ³n, las tecnologÃ­as de la informaciÃ³n y de las comunicaciones, el Estado de Derecho y las muchas otras dimensiones de carÃ¡cter cualitativo. Su definiciÃ³n sostiene que para conseguir buenos niveles de desarrollo humano en una comunidad, es imperativo que esta se desenvuelva en un clima de confianza entre sus miembros, ademÃ¡s de las aptitudes y el liderazgo de un grupo o conglomerado social para aprovechar los valores y recursos favorables al desarrollo. El capital social incluye a las organizaciones, a las instituciones, los valores de la Ã©tica, la libertad, la democracia, la calidad de la educaciÃ³n, las tecnologÃ­as de la informaciÃ³n y de las comunicaciones, el Estado de Derecho y las muchas otras dimensiones de carÃ¡cter cualitativo. ', 3, 1, 0),
(15, 'InformaciÃ³n Institucional Horarios', '2013-03-12 16:00:46', NULL, 'uploads/docs/cdee3e_BA0705DesignIdeas22.pdf', 'kjh fkjshdf kjhs fkjhds kjfh ksjhf kjhsd kjfhdskjhfkjsh kfjhs kjjhfkjdsh fkjjhs kjfh sdkjh fkjsh kfjh sdkjhf kjdsjh fkjsh fkjshd kfjhds kfhsd kfjhsdkjh fkjsdh fkjhds kfhsd kfhskd hfksdhfkds fkjdskffkjdshkjfd', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_categoria`
--

CREATE TABLE IF NOT EXISTS `sub_categoria` (
  `id_sub_cat` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_sub_cat` varchar(50) NOT NULL,
  `descripcion_sub_cat` varchar(1000) NOT NULL,
  `imagen_sub_cat` varchar(250) DEFAULT NULL,
  `id_categoria` int(11) NOT NULL,
  PRIMARY KEY (`id_sub_cat`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `sub_categoria`
--

INSERT INTO `sub_categoria` (`id_sub_cat`, `nombre_sub_cat`, `descripcion_sub_cat`, `imagen_sub_cat`, `id_categoria`) VALUES
(1, 'Idiomas I', 'Espacio dedicado a Idiomas I\r\nEl capital social es un concepto amplio que sostiene que para lograr altos niveles de desarrollo humano en una comunidad es importante que esta goce de un buen clima de confianza entre sus integrantes ademÃ¡s de la capacidad o liderazgo especÃ­fico de un grupo o conglomerado social para aprovechar los valores y recursos favorables al desarrollo.. El capital social incluye las organizaciones e instituciones, la Ã©tica, la libertad, la democracia, la calidad de la educaciÃ³n, las tecnologÃ­as de la informaciÃ³n y de las comunicaciones, el Estado de Derecho y muchas otras dimensiones de carÃ¡cter cualitativo.', 'uploads/img/e66121_banderas-del-mundo.jpg', 1),
(2, 'Idiomas II', 'Espacio dedicado a Idiomas II\r\nEl capital social es un concepto amplio que sostiene que para lograr altos niveles de desarrollo humano en una comunidad es importante que esta goce de un buen clima de confianza entre sus integrantes ademÃ¡s de la capacidad o liderazgo especÃ­fico de un grupo o conglomerado social para aprovechar los valores y recursos favorables al desarrollo.. El capital social incluye las organizaciones e instituciones, la Ã©tica, la libertad, la democracia, la calidad de la educaciÃ³n, las tecnologÃ­as de la informaciÃ³n y de las comunicaciones, el Estado de Derecho y muchas otras dimensiones de carÃ¡cter cualitativo.', 'uploads/img/00c33c_idiom.png', 1),
(3, 'Idiomas III', 'Espacio dedicado a Idiomas III\r\nEl capital social es un concepto amplio que sostiene que para lograr altos niveles de desarrollo humano en una comunidad es importante que esta goce de un buen clima de confianza entre sus integrantes ademÃ¡s de la capacidad o liderazgo especÃ­fico de un grupo o conglomerado social para aprovechar los valores y recursos favorables al desarrollo.. El capital social incluye las organizaciones e instituciones, la Ã©tica, la libertad, la democracia, la calidad de la educaciÃ³n, las tecnologÃ­as de la informaciÃ³n y de las comunicaciones, el Estado de Derecho y muchas otras dimensiones de carÃ¡cter cualitativo.', 'uploads/img/117613_idiomas.jpg', 1),
(4, 'Idiomas IV', 'Espacio dedicado a Idiomas IV\r\nEl capital social es un concepto amplio que sostiene que para lograr altos niveles de desarrollo humano en una comunidad es importante que esta goce de un buen clima de confianza entre sus integrantes ademÃ¡s de la capacidad o liderazgo especÃ­fico de un grupo o conglomerado social para aprovechar los valores y recursos favorables al desarrollo.. El capital social incluye las organizaciones e instituciones, la Ã©tica, la libertad, la democracia, la calidad de la educaciÃ³n, las tecnologÃ­as de la informaciÃ³n y de las comunicaciones, el Estado de Derecho y muchas otras dimensiones de carÃ¡cter cualitativo.', 'uploads/img/f5cf71_birrete.png', 1),
(5, 'Idiomas V', 'Espacio dedicado a Idiomas V\r\nEl capital social es un concepto amplio que sostiene que para lograr altos niveles de desarrollo humano en una comunidad es importante que esta goce de un buen clima de confianza entre sus integrantes ademÃ¡s de la capacidad o liderazgo especÃ­fico de un grupo o conglomerado social para aprovechar los valores y recursos favorables al desarrollo.. El capital social incluye las organizaciones e instituciones, la Ã©tica, la libertad, la democracia, la calidad de la educaciÃ³n, las tecnologÃ­as de la informaciÃ³n y de las comunicaciones, el Estado de Derecho y muchas otras dimensiones de carÃ¡cter cualitativo.', 'uploads/img/33add5_mundo.jpg', 1),
(6, 'Noticias Centro Idiomas', 'Espacio dedicado a Noticias del Centro de Idiomas', 'uploads/img/df1914_1311580761_232138090_1-Fotos-de--CURSOS-DE-INGLES-EN-PUERTO-LA-CRUZ.jpg', 2),
(7, 'American Corners', 'Espacio dedicado a American Corners', 'uploads/img/1f725d_1275692988_98291779_1-Fotos-de--PLAN-VACACIONAL-TEMATICO-EN-INGLES-PARA-NInOS-1275692988.jpg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_usuario` varchar(100) NOT NULL,
  `apellido_usuario` varchar(100) NOT NULL,
  `email_usuario` varchar(100) NOT NULL,
  `login_usuario` varchar(20) NOT NULL,
  `clave_usuario` varchar(50) NOT NULL,
  `img_usuario` varchar(80) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `descripcion_usuario` text NOT NULL,
  `ocupacion` varchar(50) NOT NULL,
  `tipo_usuario` varchar(20) NOT NULL,
  `id_sub_cat` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre_usuario`, `apellido_usuario`, `email_usuario`, `login_usuario`, `clave_usuario`, `img_usuario`, `telefono`, `descripcion_usuario`, `ocupacion`, `tipo_usuario`, `id_sub_cat`) VALUES
(1, 'Administrador', 'Centro de idiomas', 'centroidiomas@uvm.edu.ve', 'admin', '81dc9bdb52d04dc20036dbd8313ed055', NULL, '0271-123456', 'Aministrador Principal de este sitio web Aministrador Principal de este sitio web Aministrador Principal de este sitio web Aministrador Principal de este sitio web', 'Administrador', 'Administrador', 6),
(2, 'Usuario 2', 'Usuario 2', 'usuario2@gmail.com', 'usuario', '202cb962ac59075b964b07152d234b70', NULL, '123213123', 'udsyf iusdh fkjshd kjfhsdkjfs', 'jhagd asjgdasj', 'Administrador', 5);

-- --------------------------------------------------------

--
-- Table structure for table `utilidades`
--

CREATE TABLE IF NOT EXISTS `utilidades` (
  `id_utilidades` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_utilidades` varchar(150) NOT NULL,
  `resumen_utilidades` text NOT NULL,
  `link_utilidades` text,
  `img_utilidades` varchar(80) DEFAULT NULL,
  `doc_utilidades` varchar(80) DEFAULT NULL,
  `video_utilidades` text,
  `fecha_utilidades` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_utilidades`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `utilidades`
--

INSERT INTO `utilidades` (`id_utilidades`, `titulo_utilidades`, `resumen_utilidades`, `link_utilidades`, `img_utilidades`, `doc_utilidades`, `video_utilidades`, `fecha_utilidades`) VALUES
(6, 'Material de Apoyo Idiomas I', 'bbmbmnbmnbnbmn', NULL, 'uploads/img/f0d41a_Koala.jpg', NULL, NULL, '2012-03-07 04:30:00'),
(7, 'Prueba', 'hola hola hola hola aaaaaaaaaaaaaaaaaaaaaaa jbfjsjf sdsdhjshdjshdj sdhsjdhjashd asdhsjhdjshd sjdhjshdjs dsjdhjshdjasdh asdshd sdhsdh sdjsd  dsahdjas dsadhjshd sdsdsj dsdhsdh jshd shdjshd jsdhs jdhshd js dsdhjshdjshd hs', NULL, 'uploads/img/213476_BlackBerry-Z10-oficial.jpg', NULL, 'http://www.youtube.com/watch?v=_YOHRaigA8Y', '2012-03-07 04:30:00'),
(8, 'Utilidad 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia similique a magnam facilis veritatis deleniti aliquam itaque eos pariatur nulla perspiciatis minima doloremque vero. Consequuntur voluptates aliquam cumque explicabo corporis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia similique a magnam facilis veritatis deleniti aliquam itaque eos pariatur nulla perspiciatis minima doloremque vero. Consequuntur voluptates aliquam cumque explicabo corporis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia similique a magnam facilis veritatis deleniti aliquam itaque eos pariatur nulla perspiciatis minima doloremque vero. Consequuntur voluptates aliquam cumque explicabo corporis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia similique a magnam facilis veritatis deleniti aliquam itaque eos pariatur nulla perspiciatis minima doloremque vero. Consequuntur voluptates aliquam cumque explicabo corporis.', 'http://www.google.com', 'uploads/img/9ed1d7_1280x800_0.jpg', 'uploads/docs/9ed1d7_webservices.pdf', 'http://www.youtube.com/watch?v=_YOHRaigA8Y', '2013-03-07 21:59:45');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
