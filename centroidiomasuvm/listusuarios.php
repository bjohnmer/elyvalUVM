<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_usuarios = 10;
$pageNum_usuarios = 0;
if (isset($_GET['pageNum_usuarios'])) {
  $pageNum_usuarios = $_GET['pageNum_usuarios'];
}
$startRow_usuarios = $pageNum_usuarios * $maxRows_usuarios;

mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_usuarios = "SELECT a.*, b.nombre_sub_cat FROM usuarios as a,  sub_categoria as b WHERE a.id_sub_cat = b.id_sub_cat ORDER BY a.id_usuario DESC";
$query_limit_usuarios = sprintf("%s LIMIT %d, %d", $query_usuarios, $startRow_usuarios, $maxRows_usuarios);
$usuarios = mysql_query($query_limit_usuarios, $centroidiomasuvm) or die(mysql_error());
$row_usuarios = mysql_fetch_assoc($usuarios);

if (isset($_GET['totalRows_usuarios'])) {
  $totalRows_usuarios = $_GET['totalRows_usuarios'];
} else {
  $all_usuarios = mysql_query($query_usuarios);
  $totalRows_usuarios = mysql_num_rows($all_usuarios);
}
$totalPages_usuarios = ceil($totalRows_usuarios/$maxRows_usuarios)-1;

$queryString_usuarios = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_usuarios") == false && 
        stristr($param, "totalRows_usuarios") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_usuarios = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_usuarios = sprintf("&totalRows_usuarios=%d%s", $totalRows_usuarios, $queryString_usuarios);
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/usua.png" />
<h1>Registro de Usuarios</h1>
<table width="90%" border="0" align="center" cellpadding="1" cellspacing="3">
   <tr>
    <td colspan="3" align="left"><a href="usuarios.php?p=nusuario"><img src="imagenes/new.png" alt="Agregar un nuevo Usuario" width="45" height="46" title="Agregar un nuevo Usuario" /></a></td>
  </tr>
  <tr class="htdata">
    <td width="5%">&nbsp;</td>
    <td width="5%">&nbsp;</td>
    <td>Nombres</td>
    <td>Apellidos</td>
    <td>E-mail</td>
    <td>Descripcion</td>
    <td>Ocupacion</td>
    <td>Tipo de Usuario</td>
    <td>Subcategoria</td>
  </tr>
  <?php do { ?>
    <tr class="tdata">
      <td align="center"><a href="usuarios.php?p=modusuarios&id_usuario=<?php echo $row_usuarios['id_usuario']; ?>" onClick="return modificar();"><img src="imagenes/mod.png" width="35" height="35" alt="Modificar" title="Modificar" /></a></td>
      <td align="center"><a href="delusuario.php?id_usuario=<?php echo $row_usuarios['id_usuario']; ?>" onClick="return eliminar();"><img src="imagenes/del.png" width="35" height="35" alt="Eliminar" title="Eliminar" /></a></td>
      <td align="center" valign="middle"><?php echo $row_usuarios['nombre_usuario']; ?></td>
      <td align="center" valign="middle"><?php echo $row_usuarios['apellido_usuario']; ?></td>
      <td align="center" valign="middle"><?php echo $row_usuarios['email_usuario']; ?></td>
      <td><?php echo substr($row_usuarios['descripcion_usuario'], 0, 50); ?>...</td>
      <td align="center" valign="middle"><?php echo $row_usuarios['ocupacion']; ?></td>
      <td align="center" valign="middle"><?php echo $row_usuarios['tipo_usuario']; ?></td>
      <td align="center" valign="middle"><?php echo $row_usuarios['nombre_sub_cat']; ?></td>
    </tr>
    <?php } while ($row_usuarios = mysql_fetch_assoc($usuarios)); ?>
</table>
<table border="0">
  <tr>
    <td><?php if ($pageNum_usuarios > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_usuarios=%d%s", $currentPage, 0, $queryString_usuarios); ?>"><img src="imagenes/First.gif" /></a>
      <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_usuarios > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_usuarios=%d%s", $currentPage, max(0, $pageNum_usuarios - 1), $queryString_usuarios); ?>"><img src="imagenes/Previous.gif" /></a>
      <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_usuarios < $totalPages_usuarios) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_usuarios=%d%s", $currentPage, min($totalPages_usuarios, $pageNum_usuarios + 1), $queryString_usuarios); ?>"><img src="imagenes/Next.gif" /></a>
      <?php } // Show if not last page ?></td>
    <td><?php if ($pageNum_usuarios < $totalPages_usuarios) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_usuarios=%d%s", $currentPage, $totalPages_usuarios, $queryString_usuarios); ?>"><img src="imagenes/Last.gif" /></a>
      <?php } // Show if not last page ?></td>
  </tr>
</table>
</center>
<p>&nbsp;</p>
<?php
mysql_free_result($usuarios);
?>
