<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
$maxRows_publicaciones = 20;
$pageNum_publicaciones = 0;
if (isset($_GET['pageNum_publicaciones'])) {
  $pageNum_publicaciones = $_GET['pageNum_publicaciones'];
}
$startRow_publicaciones = $pageNum_publicaciones * $maxRows_publicaciones;

mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_publicaciones = "SELECT a.*, b.nombre_sub_cat, c.nombre_usuario, c.apellido_usuario FROM publicaciones as a, sub_categoria as b, usuarios as c WHERE a.id_sub_cat = b.id_sub_cat AND a.id_usuario = c.id_usuario AND a.id_sub_cat <> 6 ORDER BY a.fechahora_pub DESC";
$publicaciones = mysql_query($query_publicaciones, $centroidiomasuvm) or die(mysql_error());
$row_publicaciones = mysql_fetch_assoc($publicaciones);
$totalRows_publicaciones = mysql_num_rows($publicaciones);

if (isset($_GET['totalRows_publicaciones'])) {
  $totalRows_publicaciones = $_GET['totalRows_publicaciones'];
} else {
  $all_publicaciones = mysql_query($query_publicaciones);
  $totalRows_publicaciones = mysql_num_rows($all_publicaciones);
}
$totalPages_publicaciones = ceil($totalRows_publicaciones/$maxRows_publicaciones)-1;

?>
<div id="not">
	<div id="titulon">NOTICIAS</div>
</div>
<div id="noticias">
<div id="noticiasn">
		<?php if ($totalRows_publicaciones > 0) { ?>
			<?php do { ?>                   
				<div id="subtitulon">
					<a href="articulo.php?id_pub=<?php echo $row_publicaciones['id_pub']; ?>">
						<?php echo $row_publicaciones['titulo_pub']; ?>
					</a>
                 	<div><span id="subcategoria">
               		 <a href="idiomas.php?id_sub_cat=<?php echo $row_publicaciones['id_sub_cat']; ?>"><?php echo $row_publicaciones['nombre_sub_cat']; ?></a> | Publicado el <?php echo $row_publicaciones['fechahora_pub']; ?>
               		</span></div>
    </div>
	<div id="resumenn">
	  <p><img align="right" style="border: 3px solid #fff; margin-left:15px;" src="<?php echo $row_publicaciones['img_pub']; ?>" width="149" height="116"><?php echo substr($row_publicaciones['resumen_pub'],0,550); ?>...</p></div>
	<div id="ver"><a href="articulo.php?id_pub=<?php echo $row_publicaciones['id_pub']; ?>"><img src="imagenes/ver.png" width="63" height="17"></a></div>
	<p>
	<?php } while ($row_publicaciones = mysql_fetch_assoc($publicaciones)); } else { echo "Actualmente no hay publicaciones ";}?>
	</p>
</div>
</div>
<br/>
<?php
mysql_free_result($publicaciones);
?>
