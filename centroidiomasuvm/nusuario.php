<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_subcategorias = "SELECT * FROM sub_categoria";
$subcategorias = mysql_query($query_subcategorias, $centroidiomasuvm) or die(mysql_error());
$row_subcategorias = mysql_fetch_assoc($subcategorias);
$totalRows_subcategorias = mysql_num_rows($subcategorias);

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
 $prefijo = substr(md5(uniqid(rand())),0,6);
	$imgtamano = $_FILES["img_usuario"]['size'];
	$imgtipo = $_FILES["img_usuario"]['type'];
	$imgarchivo = "";
	
	if ($_FILES['img_usuario']['name'] != "") {
		// guardamos el archivo a la carpeta files
		if (copy($_FILES['img_usuario']['tmp_name'],"uploads/img/".$prefijo."_".$_FILES["img_usuario"]['name'])) {
			$status = "Archivo subido: <b>".$imgarchivo."</b>";
			$imgarchivo = "uploads/img/".$prefijo."_".$_FILES["img_usuario"]['name'];
		} else {
			$status = "Error al subir el archivo";
		}
	} else {
		$status = "Error al subir archivo";
	}

  $insertSQL = sprintf("INSERT INTO usuarios (id_usuario, nombre_usuario, apellido_usuario, email_usuario, login_usuario, clave_usuario, img_usuario, telefono, descripcion_usuario, ocupacion, tipo_usuario, id_sub_cat) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['id_usuario'], "int"),
                       GetSQLValueString($_POST['nombre_usuario'], "text"),
                       GetSQLValueString($_POST['apellido_usuario'], "text"),
                       GetSQLValueString($_POST['email_usuario'], "text"),
                       GetSQLValueString($_POST['login_usuario'], "text"),
					   GetSQLValueString(md5($_POST['clave_usuario']), "text"),
                       GetSQLValueString($imgarchivo, "text"),
                       GetSQLValueString($_POST['telefono'], "text"),
                       GetSQLValueString($_POST['descripcion_usuario'], "text"),
                       GetSQLValueString($_POST['ocupacion'], "text"),
                       GetSQLValueString($_POST['tipo_usuario'], "text"),
                       GetSQLValueString($_POST['id_sub_cat'], "int"));

  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
  $Result1 = mysql_query($insertSQL, $centroidiomasuvm) or die(mysql_error());

  $insertGoTo = "usuarios.php?p=listusuarios";
  header(sprintf("Location: %s", $insertGoTo));
}

?>
<center>
<p>&nbsp;</p>
<img src="imagenes/usua.png" />
<h1>Nuevo Usuario</h1>
<table width="70%" border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td colspan="3" align="left"><a href="usuarios.php?p=listusuarios"><img src="imagenes/atras.png" alt="Atrás" width="45" height="46" title="Atrás" /></a></td>
  </tr>
  <tr class="htdata">
    <td>&nbsp;</td>
  </tr>
<tr class="tdata">
<td><form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
          <table align="center">
            <tr valign="baseline">
              <td nowrap="nowrap" align="right"><label>Nombres:</label></td>
              <td><input type="text" name="nombre_usuario" value="" size="50" /></td>
            </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right"><label>Apellidos:</label></td>
              <td><input type="text" name="apellido_usuario" value="" size="50" /></td>
            </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right"><label>E-mail:</label></td>
              <td><input type="text" name="email_usuario" value="" size="50" /></td>
            </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right"><label>Nombre de Usuario:</label></td>
              <td><input type="text" name="login_usuario" value="" size="32" /></td>
            </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right"><label>Clave:</label></td>
              <td><input type="password" name="clave_usuario" value="" size="32" /></td>
            </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right"><label>Imagen de Perfil:</label></td>
              <td align="left" valign="top">
               <input type="file" name="img_usuario" id="img_usuario" />
              </br>Esta sera tu imagen de perfil.   Formatos Permitidos: jpeg, gif, png.</td>
            </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right"><label>Telefono:</label></td>
              <td><input type="text" name="telefono" value="" size="32" /></td>
            </tr>
            <tr valign="baseline">
              <td align="right" valign="top" nowrap="nowrap"><label>Descripcion:</label></td>
              <td valign="baseline">
                <textarea name="descripcion_usuario" id="descripcion_usuario" cols="60" rows="7"></textarea>
					<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" type="text/javascript"></script>
                    <script src="js/jquery.textareaCounter.plugin.js" type="text/javascript"></script>
<script type="text/javascript">
                        var info;
                        $(document).ready(function(){
                            var options2 = {
                                    'maxCharacterSize': 220,
                                    'originalStyle': 'originalTextareaInfo',
                                    'warningStyle' : 'warningTextareaInfo',
                                    'warningNumber': 40,
                                    'displayFormat' : '#input/#max caracteres | #words palabras'
                            };
                            $('#descripcion_usuario').textareaCount(options2);
                        });
                    </script>
                          Incluye alguna información biográfica en tu perfil. Podrá mostrarse públicamente.
              </td>
            </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right"><label>Ocupacion:</label></td>
              <td><input type="text" name="ocupacion" value="" size="50" /></td>
            </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right"><label>Tipo de Usuario:</label></td>
              <td><select name="tipo_usuario" id="tipo_usuario">
                  <option value="Administrador" >Administrador</option>
                  <option value="Profesor" >Profesor</option>
              </select></td>
            </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right"><label>Categoria:</label></td>
              <td align="left" valign="top">
              <select name="id_sub_cat" id="id_sub_cat">
                <?php
do {  
?>
                <option value="<?php echo $row_subcategorias['id_sub_cat']?>"><?php echo $row_subcategorias['nombre_sub_cat']?></option>
                <?php
} while ($row_subcategorias = mysql_fetch_assoc($subcategorias));
  $rows = mysql_num_rows($subcategorias);
  if($rows > 0) {
      mysql_data_seek($subcategorias, 0);
	  $row_subcategorias = mysql_fetch_assoc($subcategorias);
  }
?>
              </select></td>

            </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right">&nbsp;</td>
              <td><input type="submit" value="Guardar" /></td>
            </tr>
          </table>
          <input type="hidden" name="MM_insert" value="form1" />
        </form></td>
        </tr>
        </table>
        </center>
<p>&nbsp;</p>
<?php
mysql_free_result($subcategorias);
?>
