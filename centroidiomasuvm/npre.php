<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO preinscripcion (id_pre, id_sub_cat, nombre_pre, apellido_pre, cedula_pre, email, horario, carrera, fecha_pre) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['id_pre'], "int"),
                       GetSQLValueString($_POST['id_sub_cat'], "int"),
                       GetSQLValueString($_POST['nombre_pre'], "text"),
                       GetSQLValueString($_POST['apellido_pre'], "text"),
                       GetSQLValueString($_POST['cedula_pre'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['horario'], "text"),
                       GetSQLValueString($_POST['carrera'], "text"),
                       GetSQLValueString($_POST['fecha_pre'], "date"));

  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
  $Result1 = mysql_query($insertSQL, $centroidiomasuvm) or die(mysql_error());

  $insertGoTo = "preinscripcion.php?p=listpre";
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_subcategorias = "SELECT * FROM sub_categoria";
$subcategorias = mysql_query($query_subcategorias, $centroidiomasuvm) or die(mysql_error());
$row_subcategorias = mysql_fetch_assoc($subcategorias);
$totalRows_subcategorias = mysql_num_rows($subcategorias);
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/prein.png" />
<h1>Nueva Preinscripcion</h1>
<table width="60%" border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td colspan="3" align="left"><a href="preinscripcion.php?p=listpre"><img src="imagenes/atras.png" alt="Atrás" width="45" height="46" title="Atrás" /></a></td>
  </tr>
  <tr class="htdata">
    <td>&nbsp;</td>
  </tr>
<tr class="tdata">
<td><form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table align="center">
              <tr valign="baseline">
              <td nowrap align="right"><label>Subcategoria:</label></td>
              <td align="left" valign="top">
              <select name="id_sub_cat" id="id_sub_cat" title="<?php if (!empty($row_subcategorias)) {echo $row_subcategorias['id_sub_cat'];} ?>">
                <?php
do {  
?><option value="<?php echo $row_subcategorias['id_sub_cat']; ?>"><?php echo $row_subcategorias['nombre_sub_cat']; ?></option>
                <?php
} while ($row_subcategorias = mysql_fetch_assoc($subcategorias));
  $rows = mysql_num_rows($subcategorias);
  if($rows > 0) {
      mysql_data_seek($subcategorias, 0);
	  $row_subcategorias = mysql_fetch_assoc($subcategorias);
  }
?>
              </select></td>
            </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Nombre:</label></td>
      <td><input type="text" name="nombre_pre" value="" size="60" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Apellido:</label></td>
      <td><input type="text" name="apellido_pre" value="" size="60" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Cédula de Identidad:</label></td>
      <td><input type="text" name="cedula_pre" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Email:</label></td>
      <td><input type="text" name="email" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Horario:</label></td>
      <td>
        <select name="horario" id="horario">
          <option value="Matutino: 8:00 am - 10:00 am">Matutino: 8:00 am - 10:00 am</option>
          <option value="Matutino: 10:00 am - 12:00 m">Matutino: 10:00 am - 12:00 m</option>
          <option value="Vespertino: 02:00 pm - 04:00 pm">Vespertino: 02:00 pm - 04:00 pm</option>
          <option value="Vespertino: 04:00 pm - 06:00 pm">Vespertino: 04:00 pm - 06:00 pm</option>
          <option value="Nocturno: 06:00 pm - 08:00 pm">Nocturno: 06:00 pm - 08:00 pm</option>
          <option value="Sabatino: 08:00 am - 04:15 pm">Sabatino: 08:00 am - 04:15 pm</option>
        </select>
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Carrera:</label></td>
      <td><input type="text" name="carrera" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" value="Realizar Preinscripción" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_insert" value="form1" />
</form>
</td>
</tr>
</table>
</center>
<p>&nbsp;</p>

<?php
mysql_free_result($subcategorias);
?>
