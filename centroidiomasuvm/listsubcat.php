<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_subcategorias = 10;
$pageNum_subcategorias = 0;
if (isset($_GET['pageNum_subcategorias'])) {
  $pageNum_subcategorias = $_GET['pageNum_subcategorias'];
}
$startRow_subcategorias = $pageNum_subcategorias * $maxRows_subcategorias;

mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_subcategorias = "SELECT * FROM sub_categoria ORDER BY id_sub_cat ASC";
$query_limit_subcategorias = sprintf("%s LIMIT %d, %d", $query_subcategorias, $startRow_subcategorias, $maxRows_subcategorias);
$subcategorias = mysql_query($query_limit_subcategorias, $centroidiomasuvm) or die(mysql_error());
$row_subcategorias = mysql_fetch_assoc($subcategorias);

if (isset($_GET['totalRows_subcategorias'])) {
  $totalRows_subcategorias = $_GET['totalRows_subcategorias'];
} else {
  $all_subcategorias = mysql_query($query_subcategorias);
  $totalRows_subcategorias = mysql_num_rows($all_subcategorias);
}
$totalPages_subcategorias = ceil($totalRows_subcategorias/$maxRows_subcategorias)-1;

$queryString_subcategorias = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_subcategorias") == false && 
        stristr($param, "totalRows_subcategorias") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_subcategorias = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_subcategorias = sprintf("&totalRows_subcategorias=%d%s", $totalRows_subcategorias, $queryString_subcategorias);
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/subcat.png" />
<h1>Registro de Subcategorías</h1>
<table width="80%" border="0" cellpadding="1" cellspacing="1">
   <tr>
    <td colspan="3" align="left"><a href="subcategorias.php?p=nsubcat"><img src="imagenes/new.png" alt="Agregar una nueva Subcategoria" width="45" height="46" title="Agregar una nueva Subcategoria" /></a></td>
  </tr>
  <tr class="htdata">
    <td width="5%">&nbsp;</td>
    <td width="5%">&nbsp;</td>
    <td width="35%">Nombre de la Subcategoria</td>
    <td width="50%">Descripcion de la Subcategoria</td>
  </tr>
  <?php do { ?>
      <tr class="tdata">
      <td align="center" valign="middle"><a href="categorias.php?p=modsubcat&id_sub_cat=<?php echo $row_subcategorias['id_sub_cat']; ?>" onClick="return modificar();"><img src="imagenes/mod.png" alt="Modificar Categoría" width="25" height="26" align="middle" title="Modificar Categoría" ></a></td>
      <td align="center" valign="middle"><a href="delsubcat.php?id_sub_cat=<?php echo $row_subcategorias['id_sub_cat']; ?>" onClick="return eliminar();"><img src="imagenes/del.png" width="25" height="25" align="middle" alt="Eliminar Categoría" title="Eliminat Categoría" ></a></td>
      <td><?php echo $row_subcategorias['nombre_sub_cat']; ?></td>
      <td><?php echo substr($row_subcategorias['descripcion_sub_cat'], 0,200); ?>...</td>
    </tr>
    <?php } while ($row_subcategorias = mysql_fetch_assoc($subcategorias)); ?>
</table>
<table border="0">
  <tr>
    <td><?php if ($pageNum_subcategorias > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_subcategorias=%d%s", $currentPage, 0, $queryString_subcategorias); ?>"><img src="imagenes/First.gif" /></a>
      <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_subcategorias > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_subcategorias=%d%s", $currentPage, max(0, $pageNum_subcategorias - 1), $queryString_subcategorias); ?>"><img src="imagenes/Previous.gif" /></a>
      <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_subcategorias < $totalPages_subcategorias) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_subcategorias=%d%s", $currentPage, min($totalPages_subcategorias, $pageNum_subcategorias + 1), $queryString_subcategorias); ?>"><img src="imagenes/Next.gif" /></a>
      <?php } // Show if not last page ?></td>
    <td><?php if ($pageNum_subcategorias < $totalPages_subcategorias) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_subcategorias=%d%s", $currentPage, $totalPages_subcategorias, $queryString_subcategorias); ?>"><img src="imagenes/Last.gif" /></a>
      <?php } // Show if not last page ?></td>
  </tr>
</table>
</center>
<p>&nbsp;</p>
<?php
mysql_free_result($subcategorias);
?>
