<?php require_once("sesionfront.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Centro de Iidiomas UVM</title>
<link rel="shortcut icon" HREF="imagenes/logo.ico" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/funciones.js"></script>
<link rel="stylesheet" type="text/css" href="css/style-slider.css" />

    <script type="text/javascript" src="js-slider/jquery-1.2.6.min.js"></script>
    <script type="text/javascript" src="js-slider/jquery-easing-1.3.pack.js"></script>
    <script type="text/javascript" src="js-slider/jquery-easing-compatibility.1.2.pack.js"></script>
    <script type="text/javascript" src="js-slider/coda-slider.1.1.1.pack.js"></script>
    <?php if (isset($_SESSION['6_letters_code'])){ ?>
    <link href="menustyle.css" rel="stylesheet" type="text/css" />
    <!-- <script type='text/javascript' src='js/jquery-1.2.6.min.js'></script> -->
    <script type='text/javascript' src='js/kwicks.js'></script>
    <script type='text/javascript' src='js/custom.js'></script>
    <?php } ?>

    <script type="text/javascript">
        
            var theInt = null;
            var $crosslink, $navthumb;
            var curclicked = 0;
            
            theInterval = function(cur){
                clearInterval(theInt);
                
                if( typeof cur != 'undefined' )
                    curclicked = cur;
                
                $crosslink.removeClass("active-thumb");
                $navthumb.eq(curclicked).parent().addClass("active-thumb");
                    $(".stripNav ul li a").eq(curclicked).trigger('click');
                
                theInt = setInterval(function(){
                    $crosslink.removeClass("active-thumb");
                    $navthumb.eq(curclicked).parent().addClass("active-thumb");
                    $(".stripNav ul li a").eq(curclicked).trigger('click');
                    curclicked++;
                    if( 6 == curclicked )
                        curclicked = 0;
                    
                }, 3000);
            };
            
            $(function(){
                
                $("#main-photo-slider").codaSlider();
                
                $navthumb = $(".nav-thumb");
                $crosslink = $(".cross-link");
                
                $navthumb
                .click(function() {
                    var $this = $(this);
                    theInterval($this.parent().attr('href').slice(1) - 1);
                    return false;
                });
                
                theInterval();
            });
    </script>


</head>
<body>
<div id="pagina">
	<div id="top">
        <?php require_once("menu.php"); ?>
        <div id="fondoheader">
        	<?php require_once("header.php");?>
        </div>
    </div>
    <?php
        if (!empty($_SESSION['user_name'])) { 
            echo '<div id="menucentro">';
            if ($_SESSION['tipo_user']=="Administrador") { 
                include_once("menuadmin.php");
            } 
            if ($_SESSION['tipo_user']=="Profesor") {
                include_once("menuprof.php");
            }
            if ($_SESSION['tipo_user']=="Estudiante") {
                include_once("menuestu.php");
            }
            echo "</div>";
        }
    ?>  
    <div id="cuerpoizq">
       <?php require_once("novedades.php");?>
       <?php require_once("galeria.php");?>
       <?php require_once("noticias.php");?>
    </div>

    <?php 

    if (!isset($_SESSION['6_letters_code'])){
        echo "<div id='cuerpoder1'>";
        include("login-data.php");
        echo "</div>";
    }
 
    ?>
    <div id="cuerpoder">
    	<?php require_once("preins.php");?>
        <?php require_once("american.php");?>
        <?php require_once("redessociales.php");?>
        <?php require_once("linksociales.php");?>
    </div>
    <div id="pie">
        <?php require_once("pie.php");?>
    </div>
</div>  
</body>
</html>