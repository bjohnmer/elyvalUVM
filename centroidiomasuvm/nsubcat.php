<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php

mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_categorias = "SELECT * FROM categorias ORDER BY nombre_categoria ASC";
$categorias = mysql_query($query_categorias, $centroidiomasuvm) or die(mysql_error());
$row_categorias = mysql_fetch_assoc($categorias);
$totalRows_categorias = mysql_num_rows($categorias);

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
	$prefijo = substr(md5(uniqid(rand())),0,6);
	$imgtamano = $_FILES["imagen_sub_cat"]['size'];
	$imgtipo = $_FILES["imagen_sub_cat"]['type'];
	$imgarchivo = "uploads/img/".$prefijo."_".$_FILES["imagen_sub_cat"]['name'];
	
	if ($_FILES['imagen_sub_cat']['name'] != "") {
		// guardamos el archivo a la carpeta files
		if (copy($_FILES['imagen_sub_cat']['tmp_name'],$imgarchivo)) {
			$status = "Archivo subido: <b>".$imgarchivo."</b>";
		} else {
			$status = "Error al subir el archivo";
		}
	} else {
		$status = "Error al subir archivo";
	}

  $insertSQL = sprintf("INSERT INTO sub_categoria (id_sub_cat, nombre_sub_cat, descripcion_sub_cat, imagen_sub_cat, id_categoria) VALUES (%s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['id_sub_cat'], "int"),
                       GetSQLValueString($_POST['nombre_sub_cat'], "text"),
                       GetSQLValueString($_POST['descripcion_sub_cat'], "text"),
					   GetSQLValueString($imgarchivo, "text"),
                       GetSQLValueString($_POST['id_categoria'], "int"));

  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
  $Result1 = mysql_query($insertSQL, $centroidiomasuvm) or die(mysql_error());

  $insertGoTo = "subcategorias.php?p=listsubcat";
  header(sprintf("Location: %s", $insertGoTo));
}
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/subcat.png" />
<h1>Nueva Subcategoría</h1>
<table width="60%" border="0" cellpadding="1" cellspacing="1">
   <tr>
    <td colspan="3" align="left"><a href="subcategorias.php?p=listsubcat"><img src="imagenes/atras.png" alt="Atrás" width="45" height="46" title="Atrás" /></a></td>
  </tr>
  <tr class="htdata">
    <td>&nbsp;</td>
  </tr>
  <tr class="tdata">
        <td><form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
          <table align="center">
            <tr valign="baseline">
              <td align="right" valign="middle" nowrap><label>Nombre de la Subcategoria:</label></td>
              <td><input type="text" name="nombre_sub_cat" value="" size="32"></td>
            </tr>
            <tr valign="baseline">
              <td align="right" valign="middle" nowrap><label>Descripcion:</label></td>
              <td><textarea name="descripcion_sub_cat" cols="32" rows="5"></textarea></td>
            </tr>
            <tr valign="baseline">
              <td align="right" valign="middle" nowrap><label>Imagen:</label></td>
              <td align="left" valign="top">
              <input type="file" name="imagen_sub_cat" id="imagen_sub_cat" /></td>
            </tr>
            <tr valign="baseline">
              <td align="right" valign="middle" nowrap><label>Categoria:</label></td>
              <td align="left" valign="top">
              	<select name="id_categoria">
                <?php
                do {  
				?>
                <option value="<?php echo $row_categorias['id_categoria']; ?>"><?php echo $row_categorias['nombre_categoria']; ?></option>
                  <?php
                  } 
				  while ($row_categorias = mysql_fetch_assoc($categorias));
				    $rows = mysql_num_rows($categorias);
					  if($rows > 0) {
			      mysql_data_seek($categorias, 0);
				   $row_categorias = mysql_fetch_assoc($categorias);
  }
?>
              </select></td>
            </tr>
            <tr valign="baseline">
              <td nowrap align="right">&nbsp;</td>
              <td><input type="submit" value="Guardar"></td>
            </tr>
          </table>
          <input type="hidden" name="MM_insert" value="form1">
        </form></td>
    </tr>
</table>
</center>
<p>&nbsp;</p>
<?php
mysql_free_result($categorias);
?>
