<style>
					#nov{
						height: 35px;
						margin-left: 20px;
						margin-top:25px;
						width: 250px;
						border-radius: 10px;
						padding-bottom: 10px;
						background-color: #090;
						position:relative;
						border: thin solid #fff;
						z-index:100;
					}
					#nov #titulon {
						font-size:22px;
						color: #fff;
						text-align: center;
						padding: 8px;
						font-family:Arial, Helvetica, sans-serif;
						font-weight:bold;
					}
					#novedades {
						height: auto;
						margin-top:-20px;
						margin-left: 20px;
						margin-right: auto;
						margin-bottom:60px;
						}
					#novedadesn {
						width: 650px;
						overflow:auto;
						border: thin solid #090;
						border-bottom-left-radius: 10px;
						moz-border-bottom-left-radius: 10px;
						border-bottom-right-radius: 10px;
						moz-border-bottom-right-radius: 10px;
						border-top-right-radius: 10px;
						moz-border-top-right-radius: 10px;
						background: rgb(153,255,153); /* Old browsers */
						background: -moz-linear-gradient(top,  rgba(153,255,153,1) 0%, rgba(252,254,255,1) 89%); /* FF3.6+ */
						background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(153,255,153,1)), color-stop(89%,rgba(252,254,255,1))); /* Chrome,Safari4+ */
						background: -webkit-linear-gradient(top,  rgba(153,255,153,1) 0%,rgba(252,254,255,1) 89%); /* Chrome10+,Safari5.1+ */
						background: -o-linear-gradient(top,  rgba(153,255,153,1) 0%,rgba(252,254,255,1) 89%); /* Opera 11.10+ */
						background: -ms-linear-gradient(top,  rgba(153,255,153,1) 0%,rgba(252,254,255,1) 89%); /* IE10+ */
						background: linear-gradient(to bottom,  rgba(153,255,153,1) 0%,rgba(252,254,255,1) 89%); /* W3C */
						filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#99ff99', endColorstr='#fcfeff',GradientType=0 ); /* IE6-9 */
					}
					
					#novedadesn #subtitulon {
						font-size: 22px;
						margin-top:35px;
						margin-right: 5px;
						margin-right: 10px;
						text-align:left;
						
					}
					#novedadesn #subtitulon a{
						color: #333;
					}
					#novedadesn #subtitulon a:hover{
						color:#666;
					}
					#novedadesn  #datav {
						font-size: 12px;
						color: #000;
						text-align: right;
						margin-right:10px;
						margin-bottom:10px;
					}
					#novedadesn #resumenn {
						float:left;
						width: 390px;
						margin-right: 5px;
						margin-left: 10px;
						text-align: left;
						text-align: justify;
						font-size:14px;
					}
					#novedadesn #resumenn a {
						color: #C00;
					}
					#novedadesn #resumenn a:hover{
						color: #C50;
					}
					#novedadesn #imagenn {
						width: 200px;
						float: left;
						margin: 20px;
					}
					
					#nav a, #s7 strong { color: #333; margin: 0 5px; padding: 3px 5px; border: 1px solid #ccc; background: #008000; text-decoration: none }
					#nav a.activeSlide { background: #99ff99 }
					
					#nav a:focus { outline: none; }
					
					#nav {text-align: right; margin: -50px 30px 50px 0; }

</style>
			
<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_publicaciones = "SELECT * FROM publicaciones WHERE id_sub_cat = 6 ORDER BY fechahora_pub DESC LIMIT 5";
$publicaciones = mysql_query($query_publicaciones, $centroidiomasuvm) or die(mysql_error());
$row_publicaciones = mysql_fetch_assoc($publicaciones);
$totalRows_publicaciones = mysql_num_rows($publicaciones);
?>
                <div id="nov"> 
                        <div id="titulon">NOVEDADES</div>
                </div>
                <div id="novedades">
                <?php do {?>
                
                	<div id="novedadesn">
                       <div id="imagenn"><img src="<?php echo $row_publicaciones['img_pub']; ?>" width="189" height="198"></div>
                      <div id="subtitulon"><a href="articulo.php?id_pub=<?php echo $row_publicaciones['id_pub']; ?>"><?php echo $row_publicaciones['titulo_pub']; ?></a><hr align="left" width="62%" color="#009933" />
                      </div>
                        <div id="datav">Publicado el <?php echo $row_publicaciones['fechahora_pub']; ?></div>
                        <div id="resumenn"><?php echo substr($row_publicaciones['resumen_pub'],0,300); ?>...</div>                        
                  </div>
               <?php } while($row_publicaciones = mysql_fetch_assoc($publicaciones));?>   
                </div>
                <!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script> -->
    			<script src="js/jquery.cycle.all.js" type="text/javascript"></script>
                <script>
					$('#novedades').after('<div id="nav">').cycle({
					//$('#gen-loultimov').cycle({ 
						fx:      'blindX', 
						speed:    2000, 
						timeout:  2000, 
						pause: 3000,
						pager:  '#nav'
						
						
						/*prevId: 		'prevBtn',
						prevText: 		'Previous',
						nextId: 		'nextBtn',	
						nextText: 		'Next',
						controlsShow:		true,
						controlsBefore:	'',
						controlsAfter:		'',	
						controlsFade:		true,
						firstId: 		'firstBtn',
						firstText: 		'First',
						firstShow:		false,
						lastId: 		'lastBtn',	
						lastText: 		'Last',
						lastShow:		false,				
						vertical:		false,
						speed: 		800,
						auto:			false,
						pause:			2000,
						continuous:		false, 
						numeric: 		false,
						numericId: 		'controls'*/
						
					});
					
					/*
					Efectos
						blindX
						blindY
						blindZ
						cover
						curtainX
						curtainY
						fade
						fadeZoom
						growX
						growY
						scrollUp
						scrollDown
						scrollLeft
						scrollRight
						scrollHorz
						scrollVert
						shuffle
						slideX
						slideY
						toss
						turnUp
						turnDown
						turnLeft
						turnRight
						uncover
						wipe
						zoom
											
					*/
				</script>
                <?php
mysql_free_result($publicaciones);
?>
