<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_GET['id_pub'])) && ($_GET['id_pub'] != "")) {
	
		$colname_publicaciones = "-1";
		if (isset($_GET['id_pub'])) {
		  $colname_publicaciones = $_GET['id_pub'];
		}
		mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
		$query_publicaciones = sprintf("SELECT * FROM publicaciones WHERE id_pub = %s", GetSQLValueString($colname_publicaciones, "int"));
		$publicaciones = mysql_query($query_publicaciones, $centroidiomasuvm) or die(mysql_error());
		$row_publicaciones = mysql_fetch_assoc($publicaciones);
		$totalRows_publicaciones = mysql_num_rows($publicaciones);
		
    if (file_exists($row_publicaciones['img_pub'])) {
      unlink($row_publicaciones['img_pub']) or die ("<h1>Error al eliminar imágen");
    }
    if (file_exists($row_publicaciones['doc_pub'])) {
      unlink($row_publicaciones['doc_pub']) or die ("<h1>Error al eliminar archivo");
    }
	  $deleteSQL = sprintf("DELETE FROM publicaciones WHERE id_pub=%s",
						   GetSQLValueString($_GET['id_pub'], "int"));
	
	  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
	  $Result1 = mysql_query($deleteSQL, $centroidiomasuvm) or die(mysql_error());
	
	  $deleteGoTo = "publicaciones.php?p=listpublicaciones";
	  header(sprintf("Location: %s", $deleteGoTo));
}

?>

<?php
mysql_free_result($publicaciones);
?>

