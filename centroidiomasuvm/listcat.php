<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_categorias = 10;
$pageNum_categorias = 0;
if (isset($_GET['pageNum_categorias'])) {
  $pageNum_categorias = $_GET['pageNum_categorias'];
}
$startRow_categorias = $pageNum_categorias * $maxRows_categorias;

mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_categorias = "SELECT * FROM categorias ORDER BY id_categoria ASC";
$query_limit_categorias = sprintf("%s LIMIT %d, %d", $query_categorias, $startRow_categorias, $maxRows_categorias);
$categorias = mysql_query($query_limit_categorias, $centroidiomasuvm) or die(mysql_error());
$row_categorias = mysql_fetch_assoc($categorias);

if (isset($_GET['totalRows_categorias'])) {
  $totalRows_categorias = $_GET['totalRows_categorias'];
} else {
  $all_categorias = mysql_query($query_categorias);
  $totalRows_categorias = mysql_num_rows($all_categorias);
}
$totalPages_categorias = ceil($totalRows_categorias/$maxRows_categorias)-1;

$queryString_categorias = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_categorias") == false && 
        stristr($param, "totalRows_categorias") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_categorias = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_categorias = sprintf("&totalRows_categorias=%d%s", $totalRows_categorias, $queryString_categorias);
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/cat.png" />
<h1>Registro de Categorías</h1>
<table width="85%" border="0" align="center" cellpadding="3" cellspacing="1">
   <tr>
    <td colspan="3" align="left"><a href="categorias.php?p=ncat"><img src="imagenes/new.png" alt="Agregar una nueva categoria" width="45" height="46" title="Agregar una nueva categoria" /></a></td>
  </tr>
  <tr class="htdata">
    <td width="5%">&nbsp;</td>
    <td width="5%">&nbsp;</td>
    <td width="40%">Nombre de la Categoria</td>
    <td width="50%">Descripcion de la Categoria</td>
  </tr>
  <?php do { ?>
    <tr class="tdata">
      <td align="center" valign="middle"><a href="categorias.php?p=modcat&id_categoria=<?php echo $row_categorias['id_categoria']; ?>" onClick="return modificar();"><img src="imagenes/mod.png" alt="Modificar Categoría" width="25" height="26" align="middle" title="Modificar Categoría" ></a></td>
      <td align="center" valign="middle"><a href="delcat.php?id_categoria=<?php echo $row_categorias['id_categoria']; ?>" onClick="return eliminar();"><img src="imagenes/del.png" width="25" height="25" align="middle" alt="Eliminar Categoría" title="Eliminar Categoría" ></a></td>
      <td><?php echo $row_categorias['nombre_categoria']; ?></td>
      <td><?php echo $row_categorias['descripcion_categoria']; ?></td>
    </tr>
    <?php } while ($row_categorias = mysql_fetch_assoc($categorias)); ?>
</table>
<table border="0">
  <tr>
    <td><?php if ($pageNum_categorias > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_categorias=%d%s", $currentPage, 0, $queryString_categorias); ?>"><img src="imagenes/First.gif"></a>
        <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_categorias > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_categorias=%d%s", $currentPage, max(0, $pageNum_categorias - 1), $queryString_categorias); ?>"><img src="imagenes/Previous.gif"></a>
        <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_categorias < $totalPages_categorias) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_categorias=%d%s", $currentPage, min($totalPages_categorias, $pageNum_categorias + 1), $queryString_categorias); ?>"><img src="imagenes/Next.gif"></a>
        <?php } // Show if not last page ?></td>
    <td><?php if ($pageNum_categorias < $totalPages_categorias) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_categorias=%d%s", $currentPage, $totalPages_categorias, $queryString_categorias); ?>"><img src="imagenes/Last.gif"></a>
        <?php } // Show if not last page ?></td>
  </tr>
</table>
</center>
<p>&nbsp;</p>
<?php
mysql_free_result($categorias);
?>
