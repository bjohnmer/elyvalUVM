<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE categorias SET nombre_categoria=%s, descripcion_categoria=%s WHERE id_categoria=%s",
                       GetSQLValueString($_POST['nombre_categoria'], "text"),
                       GetSQLValueString($_POST['descripcion_categoria'], "text"),
                       GetSQLValueString($_POST['id_categoria'], "int"));

  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
  $Result1 = mysql_query($updateSQL, $centroidiomasuvm) or die(mysql_error());

  $updateGoTo = "categorias.php?p=listcat";
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_categorias = "-1";
if (isset($_GET['id_categoria'])) {
  $colname_categorias = $_GET['id_categoria'];
}
mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_categorias = sprintf("SELECT * FROM categorias WHERE id_categoria = %s ORDER BY id_categoria ASC", GetSQLValueString($colname_categorias, "int"));
$categorias = mysql_query($query_categorias, $centroidiomasuvm) or die(mysql_error());
$row_categorias = mysql_fetch_assoc($categorias);
$totalRows_categorias = mysql_num_rows($categorias);
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/cat.png" />
<h1>Modificar Categoría</h1>
<table width="60%" border="0" cellpadding="1" cellspacing="1">
   <tr>
    <td colspan="3" align="left"><a href="categorias.php?p=listcat"><img src="imagenes/atras.png" alt="Atrás" width="45" height="46" title="Atrás" /></a></td>
  </tr>
  <tr class="htdata">
    <td>&nbsp;</td>
  </tr>
  <tr class="tdata">
        <td><form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
          <table align="center">
            <tr valign="baseline">
              <td align="right" valign="middle" nowrap="nowrap">Nombre de la Categoria:</td>
              <td><input type="text" name="nombre_categoria" value="<?php echo htmlentities($row_categorias['nombre_categoria'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
            </tr>
            <tr valign="baseline">
              <td align="right" valign="middle" nowrap="nowrap">Descripcion de la Categoria:</td>
              <td><textarea name="descripcion_categoria" cols="32" rows="5"><?php echo htmlentities($row_categorias['descripcion_categoria'], ENT_COMPAT, 'utf-8'); ?></textarea></td>
            </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right">&nbsp;</td>
              <td><input type="submit" value="Modificar" /></td>
            </tr>
          </table>
          <input type="hidden" name="MM_update" value="form1" />
          <input type="hidden" name="id_categoria" value="<?php echo $row_categorias['id_categoria']; ?>" />
        </form></td>
        </tr>
</table>
</center>
<p>&nbsp;</p>
<?php
mysql_free_result($categorias);
?>
