<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_GET['id_img'])) && ($_GET['id_img'] != "")) {
	
	$colname_imagenes = "-1";
	if (isset($_GET['id_img'])) {
	  $colname_imagenes = $_GET['id_img'];
	}
	mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
	$query_imagenes = sprintf("SELECT * FROM imagenes WHERE id_img = %s", GetSQLValueString($colname_imagenes, "int"));
	$imagenes = mysql_query($query_imagenes, $centroidiomasuvm) or die(mysql_error());
	$row_imagenes = mysql_fetch_assoc($imagenes);
	$totalRows_imagenes = mysql_num_rows($imagenes);
	
	unlink($row_imagenes['archivoimg']);		

  $deleteSQL = sprintf("DELETE FROM imagenes WHERE id_img=%s",
                       GetSQLValueString($_GET['id_img'], "int"));

  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
  $Result1 = mysql_query($deleteSQL, $centroidiomasuvm) or die(mysql_error());

  $deleteGoTo = "img.php?p=listimg";
  header(sprintf("Location: %s", $deleteGoTo));
}

?>

<?php
mysql_free_result($imagenes);
?>
