<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
if ($_SESSION['tipo_user'] == "Profesor") {
mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_subcategorias = "SELECT * FROM sub_categoria WHERE id_sub_cat = '".$_SESSION['id_sub_cat']."' ORDER BY id_sub_cat ASC";
$subcategorias = mysql_query($query_subcategorias, $centroidiomasuvm) or die(mysql_error());
$row_subcategorias = mysql_fetch_assoc($subcategorias);
$totalRows_subcategorias = mysql_num_rows($subcategorias);
// echo "<pre>";
// print_r($row_subcategorias);
// echo "</pre>";

} else {

mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_subcategorias = "SELECT * FROM sub_categoria";
$subcategorias = mysql_query($query_subcategorias, $centroidiomasuvm) or die(mysql_error());
$row_subcategorias = mysql_fetch_assoc($subcategorias);
$totalRows_subcategorias = mysql_num_rows($subcategorias);

}
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
	$prefijo = substr(md5(uniqid(rand())),0,6);
	$imgtamano = $_FILES["img_pub"]['size'];
	$imgtipo = $_FILES["img_pub"]['type'];
	$imgarchivo = "";
		
	if ($_FILES['img_pub']['name'] != "") {
		// guardamos el archivo a la carpeta files
		if (copy($_FILES['img_pub']['tmp_name'],"uploads/img/".$prefijo."_".$_FILES["img_pub"]['name'])) {
      $imgarchivo = "uploads/img/".$prefijo."_".$_FILES["img_pub"]['name'];
			$status = "Archivo subido: <b>".$imgarchivo."</b>";
		} else {
			$status = "Error al subir el archivo";
		}
	} else {
		$status = "Error al subir archivo";
	}

  $prefijo2 = substr(md5(uniqid(rand())),0,6);
  $doctamano = $_FILES["doc_pub"]['size'];
  $doctipo = $_FILES["doc_pub"]['type'];
  $docarchivo = "";
    
  if ($_FILES['doc_pub']['name'] != "") {
    // guardamos el archivo a la carpeta files
    if (copy($_FILES['doc_pub']['tmp_name'],"uploads/docs/".$prefijo2."_".$_FILES["doc_pub"]['name'])) {
      $docarchivo = "uploads/docs/".$prefijo2."_".$_FILES["doc_pub"]['name'];
      $status = "Archivo subido: <b>".$docarchivo."</b>";
    } else {
      $status = "Error al subir el archivo";
    }
  } else {
    $status = "Error al subir archivo";
  }


  $insertSQL = sprintf("INSERT INTO publicaciones (id_pub, titulo_pub, fechahora_pub, img_pub, doc_pub, resumen_pub, id_sub_cat, id_usuario, info_pub) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['id_pub'], "int"),
                       GetSQLValueString($_POST['titulo_pub'], "text"),
                       GetSQLValueString($_POST['fechahora_pub'], "date"),
                       GetSQLValueString($imgarchivo, "text"),
                       GetSQLValueString($docarchivo, "text"),
                       GetSQLValueString($_POST['resumen_pub'], "text"),
                       GetSQLValueString($_POST['id_sub_cat'], "int"),
                       GetSQLValueString($_POST['id_usuario'], "int"),
                       GetSQLValueString($_POST['info_pub'], "int"));

  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
  $Result1 = mysql_query($insertSQL, $centroidiomasuvm) or die(mysql_error());

  $insertGoTo = "publicaciones.php?p=listpublicaciones";
  header(sprintf("Location: %s", $insertGoTo));
}
?>
<center>  
<p>&nbsp;</p>
<img src="imagenes/public.png" />
<h1>Nueva Publicación</h1>
<table width="60%" border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td colspan="3" align="left"><a href="publicaciones.php?p=listpublicaciones"><img src="imagenes/atras.png" alt="Atrás" width="45" height="46" title="Atrás" /></a></td>
  </tr>
  <tr class="htdata">
    <td>&nbsp;</td>
  </tr>
<tr class="tdata">
<td><form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
         <table align="center">
            <tr valign="baseline">
              <td nowrap align="right"><label>Titulo:</label></td>
              <td><input type="text" name="titulo_pub" value="" size="32"></td>
            </tr>
            <tr valign="baseline">
              <td nowrap align="right"><label>Imagen:</label></td>
              <td><input type="file" name="img_pub" value="" size="32"></td>
            </tr>
            <tr valign="baseline">
              <td nowrap align="right"><label>Archivo:</label></td>
              <td><input type="file" name="doc_pub" value="" size="32"></td>
            </tr>
            <tr valign="baseline">
              <td align="right" valign="top" nowrap><label>Resumen:</label></td>
              <td align="left" valign="top"><textarea name="resumen_pub" cols="50" rows="30"></textarea></td>
            </tr>
            <tr valign="baseline">
              <td nowrap align="right"><label>Subcategoria:</label></td>
              <td align="left" valign="top">
              <select name="id_sub_cat" id="id_sub_cat" title="<?php if (!empty($row_publicaciones)) {echo $row_publicaciones['id_sub_cat'];} ?>">
                <?php
do {  
?><option value="<?php echo $row_subcategorias['id_sub_cat']?>"><?php echo $row_subcategorias['nombre_sub_cat']?></option>
                <?php
} while ($row_subcategorias = mysql_fetch_assoc($subcategorias));
  $rows = mysql_num_rows($subcategorias);
  if($rows > 0) {
      mysql_data_seek($subcategorias, 0);
	  $row_subcategorias = mysql_fetch_assoc($subcategorias);
  }
?>
              </select></td>
            </tr>
            <tr>
              <td nowrap align="right"><label>Información Institucional:</label></td>
              <td align="left" valign="top">
                <input type="checkbox" name="info_pub" id="info_pub" value="1">
              </td>
            </tr>
            <tr valign="baseline">
              <td nowrap align="right">&nbsp;</td>
              <td><input type="submit" value="Guardar"></td>
            </tr>
          </table>
          <input type="hidden" name="id_usuario" value="<?php echo $_SESSION['id_user']; ?>" size="32" />  
          <input type="hidden" name="MM_insert" value="form1">
        </form></td>
        </tr>
        </table>
        </center>
<p>&nbsp;</p>
