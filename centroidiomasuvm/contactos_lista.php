<div style="margin:0 15%">
	
	<div style="width:50%; float:left;">
		<div id="titulobloques"> 
		  <div id="titulos">Redes Sociales</div>
		</div>

		<div id="link">
		        <div id="nombrelink">
			        <p>
			        	<a  target="_blank" href="http://twitter.com/uvmidiomas">
				        	<img align="absmiddle" style="border: 3px solid #fff; margin-left:15px; margin-right:10px;" src="imagenes/twitter.png" width="92" height="88">
				        	Twitter
				        </a>
				    </p>
				</div>
		</div><hr align="center" width="90%" color="#009933" />

		<div id="link">
		        <div id="nombrelink">
			        <p>
			        	<a  target="_blank" href="http://facebook.com/uvmidiomas">
			        		<img align="absmiddle" style="border: 3px solid #fff; margin-left:15px; margin-right:10px;" src="imagenes/facebook.png" width="92" height="88">
			        		Facebook
			        	</a>
			        </p>
		    	</div>
		</div>
	</div>

	<div style="width:50%; float:left;">
		<div id="titulobloques"> 
		  <div id="titulos">Contactos directos</div>
		</div>
		<div id="link">
		        <div id="nombrelink">
			        <p>
			        	<a href="#">
				        	<img align="absmiddle" style="border: 3px solid #fff; margin-left:15px; margin-right:10px;" src="imagenes/phone.png" width="92" height="88">
				        	58(271)416 64 26
				        </a>
				    </p>
				</div>
		</div><hr align="center" width="90%" color="#009933" />

		<div id="link">
		        <div id="nombrelink">
			        <p>
			        	<a  target="_blank" href="mailto:centrodeidiomas@uvm.edu.ve">
			        		<img align="absmiddle" style="border: 3px solid #fff; margin-left:15px; margin-right:10px;" src="imagenes/email.png" width="92" height="88">
			        		centrodeidiomas@uvm.edu.ve
			        	</a>
			        </p>
		    	</div>
		</div>
	</div>

</div>