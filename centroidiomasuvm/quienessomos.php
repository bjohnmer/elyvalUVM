<?php require_once("sesionfront.php");?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8" />
    <title>Centro de Iidiomas UVM</title>
    <link rel="shortcut icon" HREF="imagenes/logo.ico" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/funciones.js"></script>

    <script src="js/jquery-google.js"></script>

    <?php if (isset($_SESSION['6_letters_code'])){ ?>
        <link href="menustyle.css" rel="stylesheet" type="text/css" />
        <!--script type='text/javascript' src='js/jquery-1.2.6.min.js'></script-->
        <script type='text/javascript' src='js/kwicks.js'></script>
        <script type='text/javascript' src='js/custom.js'></script>
    <?php } ?>
    <script src="js/galleria-1.2.9.min.js"></script>
    <style>

            /* Demo styles */
            html,body{background:#fff;margin:0;}
            body{border-top:4px solid #fff;}
            .content{color:#777;font:12px/1.4 "helvetica neue",arial,sans-serif;width:620px;margin:20px auto;}
            h1{font-size:12px;font-weight:normal;color:#ddd;margin:0;}
            p{margin:0 0 20px}
            a {color:#22BCB9;text-decoration:none;}
            .cred{margin-top:20px;font-size:11px;}

            /* This rule is read by Galleria to define the gallery height: */
            #galleria{height:620px}

        </style>
</head>
<body>
    <div id="pagina">
    	<div id="top">
            <?php require_once("menu.php"); ?>
            <div id="fondoheader">
            	<?php require_once("header.php"); ?>
            </div>
        </div>
         <?php
            if (!empty($_SESSION['user_name'])) { 
                echo '<div id="menucentro">';
                if ($_SESSION['tipo_user']=="Administrador") { 
                    include_once("menuadmin.php");
                } 
                if ($_SESSION['tipo_user']=="Profesor") {
                    include_once("menuprof.php");
                }
                if ($_SESSION['tipo_user']=="Estudiante") {
                    include_once("menuestu.php");
                }
                echo "</div>";
            }
        ?>  
        <div id="quienesomos">
        	<div id="quienesomost">
        		<div id="quienesomostitulo">QUIENES SOMOS</div>
                <div id="quienesomosinfo">
                	<p>El Centro de Idiomas de la Universidad Valle del Momboy, es un ente educativo de vanguardia que impulsa la formación óptima e integral tanto de la población estudiantil uvemista como de la comunidad externa.</p>
                    <p align="center"><img src="imagenes/banderas.jpg" width="269" height="199" /></p>
                </div>
            </div>
            <div id="quienesomost">
        		<div id="quienesomostitulo">MISIÓN</div>
                <div id="quienesomosinfo">
                	<p>El Centro de Idiomas de la Universidad Valle del Momboy; tiene como misión formar en cuanto a la adquisición del Inglés u otro idioma como valor agregado altamente competitivo con un perfil académico que lo convierta en el recurso humano idóneo para impulsar el desarrollo sustentable.</p>
                    <p align="center"><img src="imagenes/student.jpg" width="269" height="199" /></p>
                </div>
            </div>
            <div id="quienesomost">
                <div id="quienesomostitulo">VISIÓN</div>
                <div id="quienesomosinfo">
                    <p>Ser un Centro de Idiomas de vanguardia, que liderice la enseñanza del Inglés en el estado, mediante una praxis pedagógica que se caracterice por ser eficiente, eficaz, creativa e innovadora y que satisfaga las necesidades, tanto de la comunidad universitaria como de la comunidad externa, de alcanzar un dominio afianzado del Inglés como lengua extranjera.</p>
                    <p align="center"><img src="imagenes/imagen2.jpg" width="269" height="199" /></p>
                </div>
            </div>            
            <div id="quienesomosc">
                <div id="quienesomostituloc">Galería</div><br><br>
                <?php require_once("galeria_q.php"); ?>
            </div>
            
        </div>
        <div id="pie">
            <?php require_once("pie.php");?>
        </div>
    </div> 
</body>
</html>


