<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_links = 10;
$pageNum_links = 0;
if (isset($_GET['pageNum_links'])) {
  $pageNum_links = $_GET['pageNum_links'];
}
$startRow_links = $pageNum_links * $maxRows_links;

mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_links = "SELECT * FROM links ORDER BY id_links ASC";
$query_limit_links = sprintf("%s LIMIT %d, %d", $query_links, $startRow_links, $maxRows_links);
$links = mysql_query($query_limit_links, $centroidiomasuvm) or die(mysql_error());
$row_links = mysql_fetch_assoc($links);

if (isset($_GET['totalRows_links'])) {
  $totalRows_links = $_GET['totalRows_links'];
} else {
  $all_links = mysql_query($query_links);
  $totalRows_links = mysql_num_rows($all_links);
}
$totalPages_links = ceil($totalRows_links/$maxRows_links)-1;

$queryString_links = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_links") == false && 
        stristr($param, "totalRows_links") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_links = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_links = sprintf("&totalRows_links=%d%s", $totalRows_links, $queryString_links);
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/links.png" />
<h1>Registro de Links</h1>
<table width="90%" border="0" align="center" cellpadding="1" cellspacing="1">
   <tr>
    <td colspan="3" align="left"><a href="links.php?p=nlink"><img src="imagenes/new.png" alt="Agregar un nuevo Link" width="45" height="46" title="Agregar un nuevo Link" /></a></td>
  </tr>
  <tr class="htdata">
    <td width="5%">&nbsp;</td>
    <td width="5%">&nbsp;</td>
    <td>Nombre del Link</td>
    <td>Enlace Web</td>
    <td>Imágen</td>
  </tr>
  <?php do { ?>
    <tr class="tdata">
       <td align="center"><a href="links.php?p=modlink&id_links=<?php echo $row_links['id_links']; ?>" onClick="return modificar();"><img src="imagenes/mod.png" width="35" height="35" alt="Modificar" title="Modificar"></a></td>
      <td align="center"><a href="dellink.php?id_links=<?php echo $row_links['id_links']; ?>" onClick="return eliminar();"><img src="imagenes/del.png" width="35" height="35" alt="Eliminar" title="Eliminar"></a></td>
      <td><?php echo $row_links['nombre_link']; ?></td>
      <td><?php echo $row_links['enlace_link']; ?></td>
      <td><img src="<?php echo $row_links['img_link']; ?>" width="76" height="81" align="absmiddle"/></td>
    </tr>
    <?php } while ($row_links = mysql_fetch_assoc($links)); ?>
</table>
<table border="0">
  <tr>
    <td><?php if ($pageNum_links > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_links=%d%s", $currentPage, 0, $queryString_links); ?>"><img src="imagenes/First.gif" /></a>
        <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_links > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_links=%d%s", $currentPage, max(0, $pageNum_links - 1), $queryString_links); ?>"><img src="imagenes/Previous.gif" /></a>
        <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_links < $totalPages_links) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_links=%d%s", $currentPage, min($totalPages_links, $pageNum_links + 1), $queryString_links); ?>"><img src="imagenes/Next.gif" /></a>
        <?php } // Show if not last page ?></td>
    <td><?php if ($pageNum_links < $totalPages_links) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_links=%d%s", $currentPage, $totalPages_links, $queryString_links); ?>"><img src="imagenes/Last.gif" /></a>
        <?php } // Show if not last page ?></td>
  </tr>
</table>
</center>
<p>&nbsp;</p>
<?php
mysql_free_result($links);
?>
