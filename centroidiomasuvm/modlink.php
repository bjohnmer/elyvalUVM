<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  
  $prefijo = substr(md5(uniqid(rand())),0,6);
  $change = false;
  	if (!empty($_FILES["img_link"]['name'])) {		
		$imgtamano = $_FILES["img_link"]['size'];
		$imgtipo = $_FILES["img_link"]['type'];
		$imgarchivo = "";
		if ($_FILES['img_link']['name'] != "") {
			if (copy($_FILES['img_link']['tmp_name'],"uploads/img/".$prefijo."_".$_FILES["img_link"]['name'])) {
				$status = "Archivo subido: <b>".$imgarchivo."</b>";
				$imgarchivo = "uploads/img/".$prefijo."_".$_FILES["img_link"]['name'];
			} else {
				$status = "Error al subir el archivo";
			}
		} else {
			$status = "Error al subir archivo";
		}
		$change = true;
	} else {
		$imgarchivo = $_POST['img_link1'];
	}

  $updateSQL = sprintf("UPDATE links SET nombre_link=%s, enlace_link=%s, img_link=%s WHERE id_links=%s",
                       GetSQLValueString($_POST['nombre_link'], "text"),
                       GetSQLValueString($_POST['enlace_link'], "text"),
                       GetSQLValueString($imgarchivo, "text"),
                       GetSQLValueString($_POST['id_links'], "int"));
					   
	if ($change) {
		unlink($_REQUEST['img_link1']);
	}
  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
  $Result1 = mysql_query($updateSQL, $centroidiomasuvm) or die(mysql_error());

  $updateGoTo = "links.php?p=listlinks";
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_link = "-1";
if (isset($_GET['id_links'])) {
  $colname_link = $_GET['id_links'];
}
mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_link = sprintf("SELECT * FROM links WHERE id_links = %s", GetSQLValueString($colname_link, "int"));
$link = mysql_query($query_link, $centroidiomasuvm) or die(mysql_error());
$row_link = mysql_fetch_assoc($link);
$totalRows_link = mysql_num_rows($link);
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/utili.png" />
<h1>Modificar Link</h1>
<table width="60%" border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td colspan="3" align="left"><a href="links.php?p=listlinks"><img src="imagenes/atras.png" alt="Atrás" width="45" height="46" title="Atrás" /></a></td>
  </tr>
  <tr class="htdata">
    <td>&nbsp;</td>
  </tr>
<tr class="tdata">
<td><form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table align="center">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Nombre del Link:</label></td>
      <td><input type="text" name="nombre_link" value="<?php echo htmlentities($row_link['nombre_link'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Enlace Web:</label></td>
      <td><input type="text" name="enlace_link" value="<?php echo htmlentities($row_link['enlace_link'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Imágen:</label></td>
      <td><input type="file" name="img_link" value="<?php echo htmlentities($row_link['img_link'], ENT_COMPAT, 'utf-8'); ?>" size="32" />
     <p><img src="<?php echo $row_link['img_link']; ?>" width="169" height="169" /></p></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" value="Modificar" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="id_links" value="<?php echo $row_link['id_links']; ?>" />
  <input name="img_link1" type="hidden" id="img_link1" value="<?php echo $row_link['img_link']; ?>" />
</form>
</td>
</tr>
</table>
</center>
<p>&nbsp;</p>
<?php
mysql_free_result($link);
?>
