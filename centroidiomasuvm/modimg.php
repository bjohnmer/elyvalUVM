<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  
  $prefijo = substr(md5(uniqid(rand())),0,6);
  $change = false;
  	if (!empty($_FILES["archivoimg"]['name'])) {		
		$imgtamano = $_FILES["archivoimg"]['size'];
		$imgtipo = $_FILES["archivoimg"]['type'];
		$imgarchivo = "";
		if ($_FILES['archivoimg']['name'] != "") {
			if (copy($_FILES['archivoimg']['tmp_name'],"uploads/img/".$prefijo."_".$_FILES["archivoimg"]['name'])) {
				$status = "Archivo subido: <b>".$imgarchivo."</b>";
				$imgarchivo = "uploads/img/".$prefijo."_".$_FILES["archivoimg"]['name'];
			} else {
				$status = "Error al subir el archivo";
			}
		} else {
			$status = "Error al subir archivo";
		}
		$change = true;
	} else {
		$imgarchivo = $_POST['archivoimg1'];
	}

  $updateSQL = sprintf("UPDATE imagenes SET nombre_img=%s, descripcion_img=%s, archivoimg=%s WHERE id_img=%s",
                       GetSQLValueString($_POST['nombre_img'], "text"),
                       GetSQLValueString($_POST['descripcion_img'], "text"),
                       GetSQLValueString($imgarchivo, "text"),
                       GetSQLValueString($_POST['id_img'], "int"));
	
	if ($change) {
		unlink($_REQUEST['archivoimg1']);
	}

  mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
  $Result1 = mysql_query($updateSQL, $centroidiomasuvm) or die(mysql_error());

  $updateGoTo = "img.php?p=listimg";
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_imagenes = "-1";
if (isset($_GET['id_img'])) {
  $colname_imagenes = $_GET['id_img'];
}
mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_imagenes = sprintf("SELECT * FROM imagenes WHERE id_img = %s ORDER BY id_img ASC", GetSQLValueString($colname_imagenes, "int"));
$imagenes = mysql_query($query_imagenes, $centroidiomasuvm) or die(mysql_error());
$row_imagenes = mysql_fetch_assoc($imagenes);
$totalRows_imagenes = mysql_num_rows($imagenes);
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/img.jpg" />
<h1>Modificar Imágen</h1>
<table width="60%" border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td colspan="3" align="left"><a href="img.php?p=listimg"><img src="imagenes/atras.png" alt="Atrás" width="45" height="46" title="Atrás" /></a></td>
  </tr>
  <tr class="htdata">
    <td>&nbsp;</td>
  </tr>
<tr class="tdata">
<td><form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table align="center">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Nombre:</label></td>
      <td><input type="text" name="nombre_img" value="<?php echo htmlentities($row_imagenes['nombre_img'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Descripcion:</label></td>
      <td><input type="text" name="descripcion_img" value="<?php echo htmlentities($row_imagenes['descripcion_img'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><label>Archivo:</label></td>
      <td><input type="file" name="archivoimg" value="<?php echo htmlentities($row_imagenes['archivoimg'], ENT_COMPAT, 'utf-8'); ?>" size="32" />
      <p><img src="<?php echo $row_imagenes['archivoimg']; ?>" width="169" height="169" /></p></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" value="Modificar" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="id_img" value="<?php echo $row_imagenes['id_img']; ?>" />
  <input name="archivoimg1" type="hidden" id="archivoimg1" value="<?php echo $row_imagenes['archivoimg']; ?>" />
</form>
</td>
</tr>
</table>
</center>
<p>&nbsp;</p>

<?php
mysql_free_result($imagenes);
?>
