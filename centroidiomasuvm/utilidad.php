<?php 
	session_name("centroidiomasuvm");
	@session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Centro de Iidiomas UVM</title>
<link rel="shortcut icon" HREF="imagenes/logo.ico" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/funciones.js"></script>
</head>
<body vlink="#fff">
<div id="pagina">
	<div id="top">
        <?php require_once("menu.php"); ?>
        <div id="fondoheader">
        	<?php require_once("header.php");?>
        </div>
    </div>
    <div id="subcat">
    	 <?php require_once("subcatutil.php");?>
    </div>
    <div id="articulo">
    	 <?php require_once("infoutilidad.php");?>
    </div>
    <div id="pie">
        <?php require_once("pie.php");?>
    </div>
</div>  
</body>
</html>