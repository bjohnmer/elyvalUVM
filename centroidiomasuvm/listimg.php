<?php require_once('Connections/centroidiomasuvm.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_imagenes = 10;
$pageNum_imagenes = 0;
if (isset($_GET['pageNum_imagenes'])) {
  $pageNum_imagenes = $_GET['pageNum_imagenes'];
}
$startRow_imagenes = $pageNum_imagenes * $maxRows_imagenes;

mysql_select_db($database_centroidiomasuvm, $centroidiomasuvm);
$query_imagenes = "SELECT * FROM imagenes ORDER BY id_img ASC";
$query_limit_imagenes = sprintf("%s LIMIT %d, %d", $query_imagenes, $startRow_imagenes, $maxRows_imagenes);
$imagenes = mysql_query($query_limit_imagenes, $centroidiomasuvm) or die(mysql_error());
$row_imagenes = mysql_fetch_assoc($imagenes);

if (isset($_GET['totalRows_imagenes'])) {
  $totalRows_imagenes = $_GET['totalRows_imagenes'];
} else {
  $all_imagenes = mysql_query($query_imagenes);
  $totalRows_imagenes = mysql_num_rows($all_imagenes);
}
$totalPages_imagenes = ceil($totalRows_imagenes/$maxRows_imagenes)-1;

$queryString_imagenes = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_imagenes") == false && 
        stristr($param, "totalRows_imagenes") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_imagenes = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_imagenes = sprintf("&totalRows_imagenes=%d%s", $totalRows_imagenes, $queryString_imagenes);
?>
<center>
<p>&nbsp;</p>
<img src="imagenes/img.jpg" />
<h1>Registro de Imágenes</h1>
<table width="85%" border="0" align="center" cellpadding="3" cellspacing="1">
   <tr>
    <td colspan="3" align="left"><a href="img.php?p=nimagenes"><img src="imagenes/new.png" alt="Agregar una nueva Imágen" width="45" height="46" title="Agregar una nueva Imágen" /></a></td>
  </tr>
  <tr class="htdata">
    <td width="5%">&nbsp;</td>
    <td width="5%">&nbsp;</td>
    <td>Nombre</td>
    <td>Descripcion</td>
    <td>Imágen</td>
  </tr>
  <?php do { ?>
        <tr class="tdata">
    	<td align="center" valign="middle"><a href="img.php?p=modimg&id_img=<?php echo $row_imagenes['id_img']; ?>" onClick="return modificar();"><img src="imagenes/mod.png" alt="Modificar Imágen" width="25" height="26" align="middle" title="Modificar Imágen" ></a></td>
   		<td align="center" valign="middle"><a href="delimg.php?id_img=<?php echo $row_imagenes['id_img']; ?>" onClick="return eliminar();"><img src="imagenes/del.png" width="25" height="25" align="middle" alt="Eliminar Imágen" title="Eliminar Imágen" ></a></td>
      <td><?php echo $row_imagenes['nombre_img']; ?></td>
      <td><?php echo $row_imagenes['descripcion_img']; ?></td>
      <td><img src="<?php echo $row_imagenes['archivoimg']; ?>" width="76" height="81" align="absmiddle"/></td>
    </tr>
    <?php } while ($row_imagenes = mysql_fetch_assoc($imagenes)); ?>
</table>
<table border="0">
  <tr>
    <td><?php if ($pageNum_imagenes > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_imagenes=%d%s", $currentPage, 0, $queryString_imagenes); ?>"><img src="imagenes/First.gif" /></a>
        <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_imagenes > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_imagenes=%d%s", $currentPage, max(0, $pageNum_imagenes - 1), $queryString_imagenes); ?>"><img src="imagenes/Previous.gif" /></a>
        <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_imagenes < $totalPages_imagenes) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_imagenes=%d%s", $currentPage, min($totalPages_imagenes, $pageNum_imagenes + 1), $queryString_imagenes); ?>"><img src="imagenes/Next.gif" /></a>
        <?php } // Show if not last page ?></td>
    <td><?php if ($pageNum_imagenes < $totalPages_imagenes) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_imagenes=%d%s", $currentPage, $totalPages_imagenes, $queryString_imagenes); ?>"><img src="imagenes/Last.gif" /></a>
        <?php } // Show if not last page ?></td>
  </tr>
</table>
<?php
mysql_free_result($imagenes);
?>
