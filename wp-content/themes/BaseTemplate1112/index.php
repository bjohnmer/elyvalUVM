	<?php get_header(); ?>
	<section class="row">
	    <div class="span12 al_center">
	    	<h1>Bienvenidos</h1>
	    </div>
	</section> <!-- row -->

	<section class="row">
		<article class="span12">
			<div class="row">
				<?php if (have_posts()) : ?>
					<?php $index = new WP_Query( array ("category_name" => "index", "showposts"=>3)); while($index->have_posts()) : $index->the_post();?>
					<div class="span4">
						<div class="thumbnail">
							<?php the_post_thumbnail(); ?>
							<div class="caption">
								<h3><a href="<?php the_permalink();?>"><?php the_title(); ?></a> </h3>
							    <p class="muted"><?php the_time() ?> | <?php the_author(); ?> </p>
							    <p><?php the_content("Leer más..."); ?> </p>
						    </div>
						</div>
					</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
			<hr>
			<div class="row">
				<div class="span8">
					<h2>Artículos Recientes</h2>
					<?php if (have_posts()) : ?>
						<?php $vrecents = new WP_Query( array ("category_name" => "articulos", "showposts"=>3)); while($vrecents->have_posts()) : $vrecents->the_post();?>
							<h3><a href="<?php the_permalink();?>"><?php the_title(); ?></a> </h3>
						    <p class="muted"><?php the_time() ?> | <?php the_author(); ?> </p>
						    <p><?php the_content("Leer más..."); ?> </p>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
				<article class="span4">
					<?php get_sidebar(); ?>
				</article>
			</div>
		</article>
	</section>
	<?php get_footer(); ?>