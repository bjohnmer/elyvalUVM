	<?php get_header(); ?>

	<section class="row">
		<article class="span8">
			<div class="row">
				<div class="span8">
					<?php if (is_category()) :  ?>
						<h2><?php single_cat_title(); ?></h2>
					<?php elseif (is_month()) : ?>
						<h2><?php the_time('F, Y') ?></h2>
					<?php endif; ?>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<?php if (have_posts()) : ?>
						<?php while(have_posts()) : the_post();?>
							<h3><a href="<?php the_permalink();?>"><?php the_title(); ?></a> </h3>
						    <p class="muted"><?php the_time() ?> | <?php the_author(); ?> </p>
						    <p><?php the_content("Leer más..."); ?> </p>					
						<?php endwhile; ?>
					<?php else: ?>
						<div class="alert alert-error">No hay posts registrados</div>
					<?php endif; ?>
				</div>
			</div>
		</article>

		<article class="span4">
			<?php get_sidebar(); ?>
		</article>
	</section>
	<?php get_footer(); ?>