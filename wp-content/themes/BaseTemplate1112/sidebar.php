<aside>
		<div class="well">
			<p>Suscríbete a nuestro Canal RSS</p>
			<a class="btn btn-success" href="<?php bloginfo("rss2_url"); ?>" >Suscríbete Ahora!</a>
		</div>
		<ul>
			<?php wp_list_pages("title_li=") ?>
		</ul>
		<h4>Buscar</h4>
		<form class="well form-search" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
			<input type="text" name="s" id="s" class="input-big search-query" placeholder="Texto a buscar" />
			<input type="submit" id="searchsubmit" value="Buscar" class="btn"/>
		</form>
		<?php /*wp_nav_menu( array( 'theme_location' => 'principal' ) ); */ ?>
	<?php if ( !dynamic_sidebar() ) : ?>
	<?php endif; ?>
</aside>