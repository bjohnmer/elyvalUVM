<?php get_header(); ?>
	<section class="row">
		<article class="span8">
			<div class="row">
				<div class="span8">
					<div class="page-header">
						<h1>¡OOPS!</h1>
					</div>
					<div class="alert alert-error">Entraste al sitio equivocado</div>
				</div>
			</div>
		</article>

		<article class="span4">
			<?php get_sidebar(); ?>
		</article>
	</section>
	<?php get_footer(); ?>