<!DOCTYPE HTML>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title><?php wp_title( " | ", true, "right" ); ?> <?php bloginfo("name"); ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo("stylesheet_url"); ?>">

    <?php if ( is_singular() ) wp_enqueue_script("comment_reply") ?>

    <link rel="shortcut icon" href="<?php bloginfo("template_url") ?>/favicon.ico">

    <?php wp_head(); ?>

    <style>
        body
        {
            padding-top: 70px; 
        }
    </style>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <!-- <link rel="shortcut icon" href="http://localhost:49558/assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://localhost:49558/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://localhost:49558/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://localhost:49558/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="http://localhost:49558/assets/ico/apple-touch-icon-57-precomposed.png"> -->

</head>
<body data-spy="scroll" data-target=".subnav" data-offset="50" screen_capture_injected="true">
	<div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <a class="brand" href=" <?php echo get_option("home") ?> "> <?php bloginfo("name") ?> </a>

                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <?php foreach((get_categories()) as $cat) : ?>
                            <?php if (!($cat->cat_name=='index')) : ?> 
                                <li><a href="<?php home_url(); ?>/?cat=<?php echo $cat->cat_ID; ?>"><?php echo $cat->cat_name; ?></a></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
	  