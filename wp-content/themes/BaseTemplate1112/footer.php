		<footer class="footer span10">
	    	<p>&copy; Company 2012</p>
	    </footer>
	    <?php wp_footer(); ?>
	</div><!-- container -->

	<!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo("template_url") ?>/js/jquery-1.8.0.js"></script>
	<script type="text/javascript" src="<?php bloginfo("template_url") ?>/js/bootstrap-alert.js"></script>
	<script type="text/javascript" src="<?php bloginfo("template_url") ?>/js/bootstrap-button.js"></script>
	<script type="text/javascript" src="<?php bloginfo("template_url") ?>/js/bootstrap-carousel.js"></script>
	<script type="text/javascript" src="<?php bloginfo("template_url") ?>/js/bootstrap-collapse.js"></script>
	<script type="text/javascript" src="<?php bloginfo("template_url") ?>/js/bootstrap-dropdown.js"></script>
	<script type="text/javascript" src="<?php bloginfo("template_url") ?>/js/bootstrap-modal.js"></script>
	<script type="text/javascript" src="<?php bloginfo("template_url") ?>/js/bootstrap-popover.js"></script>
	<script type="text/javascript" src="<?php bloginfo("template_url") ?>/js/bootstrap-scrollspy.js"></script>
	<script type="text/javascript" src="<?php bloginfo("template_url") ?>/js/bootstrap-tab.js"></script>
	<script type="text/javascript" src="<?php bloginfo("template_url") ?>/js/bootstrap-tooltip.js"></script>
	<script type="text/javascript" src="<?php bloginfo("template_url") ?>/js/bootstrap-transition.js"></script>
	<script type="text/javascript" src="<?php bloginfo("template_url") ?>/js/bootstrap-typeahead.js"></script>
	<script type="text/javascript" src="<?php bloginfo("template_url") ?>/js/jquery.unobtrusive-ajax.js"></script>
	<script type="text/javascript" src="<?php bloginfo("template_url") ?>/js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php bloginfo("template_url") ?>/js/jquery.validate.unobtrusive.js"></script>
</body>
</html>