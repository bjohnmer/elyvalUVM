	<?php get_header(); ?>
	<section class="row">
		<article class="span8">
			<div class="row">
				<div class="span8">
					<?php if (have_posts()) : ?>
						<?php while(have_posts()) : the_post();?>
							<h2><?php the_title(); ?></h2>
							<?php the_post_thumbnail(); ?>
						    <p class="muted"><?php the_time() ?> | <?php the_author(); ?> </p>
						    <p><?php the_content(); ?> </p>					
						<?php endwhile; ?>
					<?php else: ?>
						<div class="alert alert-error"><strong>Error:</strong> No se encuentra la página</div>
					<?php endif; ?>
				</div>
			</div>
		</article>

		<article class="span4">
			<?php get_sidebar(); ?>
		</article>
	</section>
	<?php get_footer(); ?>