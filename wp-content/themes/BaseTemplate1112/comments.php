<?php 
	//No Borre éstas líneas
	// if ( !empty($_SERVER["SCRIPT_FILENAME"] && 'comments.php' == basename($_SERVER["SCRIPT_FILENAME"])) ) {
	// 	die ("<h3>Error:</h3> <p>Por pavor no trate de cargar ésta página directamente</p>");
	// }
?>
<?php  
// if (post_password_required()) { ?>
		<!-- <div class="alert alert-error"><strong>Error:</strong> Éste post está protegido con contraseña. Ingrese la clave para verlo</div> -->
<?php 
		// 	return;
		// }
?>

<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to twentyten_comment which is
 * located in the functions.php file.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

			<div id="comments">
<?php if ( post_password_required() ) : ?>
				<p class="nopassword"><?php _e( 'Éste post está protegido con contraseña. Ingrese la clave para verlo', bloginfo("name") ); ?></p>
			</div><!-- #comments -->
<?php
		/* Stop the rest of comments.php from being processed,
		 * but don't kill the script entirely -- we still have
		 * to fully load the template.
		 */
		return;
	endif;
?>

<?php
	// You can start editing here -- including this comment!
?>

<?php if ( have_comments() ) : ?>
			<h3 id="comments-title"><?php
			printf( _n( 'Un comentario para %2$s', '%1$s Comentarios para %2$s', get_comments_number() ),
			number_format_i18n( get_comments_number() ), '<em>' . get_the_title() . '</em>' );
			?></h3>

<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="navigation">
				<div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Comentarios más antíguos' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( __( 'Comentarios más recientes <span class="meta-nav">&rarr;</span>' ) ); ?></div>
			</div> <!-- .navigation -->
<?php endif; // check for comment navigation ?>

			<ol class="commentlist">
				<?php
					/* Loop through and list the comments. Tell wp_list_comments()
					 * to use BaseTemplate_comment() to format the comments.
					 * If you want to overload this in a child theme then you can
					 * define BaseTemplate_comment() and that will be used instead.
					 * See BaseTemplate_comment() in twentyten/functions.php for more.
					 */
					wp_list_comments( array( 'callback' => 'BaseTemplate_comment' ) );
				?>
			</ol>

<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="navigation">
				<div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Comentarios más antíguos' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( __( 'Comentarios más recientes <span class="meta-nav">&rarr;</span>' ) ); ?></div>
			</div><!-- .navigation -->
<?php endif; // check for comment navigation ?>

<?php else : // or, if we don't have comments:

	/* If there are no comments and comments are closed,
	 * let's leave a little note, shall we?
	 */
	if ( ! comments_open() ) :
?>
	<p class="nocomments"><?php _e( 'Los comentarios están cerrados para éste post' ); ?></p>
<?php endif; // end ! comments_open() ?>

<?php endif; // end have_comments() ?>

<?php

$comments_args = comment_custom_default();

comment_form($comments_args); 

?>

</div><!-- #comments -->


