<?php 

/**
 * Habilita el uso de Imágenes en los Posts de éste Tema
 * Enables Post Thumbnails support for this Theme
 **/

// add_theme_support( 'post-thumbnails' ); 
add_theme_support( 'post-thumbnails', array( 'post', 'page' , 'movie' ) ); // Posts and Movies


/**
 * Habilita el uso de widgets en el sidebar de éste Tema
 * Enables widgets support for this Theme
 **/
if ( function_exists ('register_sidebar')) { 
    register_sidebar(array(
	        'before_widget' => '',
	        'after_widget' => '',
	        'before_title' => '<div class="title">',
	        'after_title' => '</div>',
	));
} 

/**
 * Registros de todos los menús del tema
 **/
function mis_menus() {
  register_nav_menus(
    array( 'principal' => __( 'Menú Principal' ) )
  );
}
add_action( 'init', 'mis_menus' );

/**
 * Callback para listar loa comentarios
 **/
if ( ! function_exists( 'BaseTemplate_comment' ) ) :
/**
 * Basado en twentyten_comment function
 **/
function BaseTemplate_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
		<div class="comment-author vcard">
			<?php echo get_avatar( $comment, 40 ); ?>
			<?php printf( __( '%s <span class="says">dice:</span>' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
		</div><!-- .comment-author .vcard -->
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em class="comment-awaiting-moderation"><?php _e( ' | Tu comentario está esperando por ser aprobado por moderación.' ); ?></em>
			<br />
		<?php endif; ?>

		<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
			<?php
				/* translators: 1: date, 2: time */
				printf( __( ' %1$s | %2$s '), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Modificar)'), ' ' );
			?>
		</div>

		<div class="comment-body"><?php comment_text(); ?></div>

		<div class="reply">
			<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => 'Responder' ) ) ); ?>
		</div>
	</div>

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Modificar)' ), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}
endif;

/**
 * Opciones predeterminadas para el formulario de comentarios (Traducción al Español)
 **/
if ( ! function_exists( 'comment_custom_default' ) ) :

	function comment_custom_default()
	{
		return array(
		        'title_reply'=>'<h3>Deje un Comentario</h3>',
		        'comment_notes_before' => 'Su dirección de email no será publicada<br>Los Campos obligatorios están marcados con un <span class="required">*</span>',
		        'comment_notes_after' => '',
		        'fields' => array (
		        				'author' => '<p class="comment-form-author"><label for="author">' . _x( 'Nombre', 'noun' ) . '</label><span class="required">*</span><input id="author" name="author" type="text" value="" size="30" aria-required="true"></p>',
		        				'email' => '<p class="comment-form-email"><label for="email">' . _x( 'Correo Electrónico', 'noun' ) . '</label><span class="required">*</span><input id="email" name="email" type="text" value="" size="30" aria-required="true"></p>',
		        				'website' => '<p class="comment-form-url"><label for="url">' . _x( 'Website', 'noun' ) . '</label><span>&nbsp;</span><input id="url" name="url" type="text" value="" size="30"></p>'
		        			),
		        'title_reply_to' =>  'De una respuesta a %s',
		        'cancel_reply_link'    => 'Cancelar Respuesta',
		        'comment_field' => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comentario', 'noun' ) . '</label><span class="required">*</span><textarea id="comment" name="comment" aria-required="true"></textarea></p>',
		        'logged_in_as' => '<p class="logged-in-as">' . sprintf( __( 'Logueado como <a href="%1$s">%2$s</a>. <a href="%3$s" title="Cerrar sesión">¿Cerrar Sesión?</a>' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
		        'label_submit' => 'Enviar Comentario'
		);
	}

endif;


?>

