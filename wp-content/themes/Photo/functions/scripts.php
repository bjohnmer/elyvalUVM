<?php
/**
 * This file loads the CSS and Javascript used for the theme.
 * @package Photo WordPress Theme
 * @since 1.0
 * @author AJ Clarke : http://wpexplorer.com
 * @copyright Copyright (c) 2012, AJ Clarke
 * @link http://wpexplorer.com
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
 
add_action('wp_enqueue_scripts','wpex_load_scripts');
function wpex_load_scripts() {
	
	
	/*******
	*** CSS
	*******************/
	
	// Main
	wp_enqueue_style( 'style', get_stylesheet_uri() );
	
	// Responsive
	if( of_get_option('responsive') == '1') wp_enqueue_style('wpex-responsive', WPEX_CSS_DIR . '/responsive.css');
		
	// Google Fonts
	wp_enqueue_style('droid-serif-gfont','http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic', 'style');
	
	
	/*******
	*** jQuery
	*******************/
	
	// Main Scripts
	wp_enqueue_script('hoverIntent', WPEX_JS_DIR .'/hoverintent.js', array('jquery'), 'r6', true);
	wp_enqueue_script('superfish', WPEX_JS_DIR .'/superfish.js', array('jquery'), '1.4.8', true);
	
	// Responsive
	wp_enqueue_script('fitvids', WPEX_JS_DIR .'/fitvids.js', array('jquery'), 1.0, true);
	wp_enqueue_script('uniform', WPEX_JS_DIR .'/uniform.js', array('jquery'), '1.7.5', true);
	wp_enqueue_script('wpex-responsive', WPEX_JS_DIR .'/responsive.js', array('jquery'), '', true);
	
	// Comment replies
	if(is_single() || is_page()) {
		wp_enqueue_script('comment-reply');
	}
	
	// Localize responsive nav
	$nav_params = array(
		'text' => __('Menu','wpex'),
	);
	wp_localize_script( 'wpex-responsive', 'responsiveLocalize', $nav_params );
	
	// Initialize
	wp_enqueue_script('wpex-global-init', WPEX_JS_DIR .'/initialize.js', false, '1.0', true);

	
} //end wpex_load_scripts()
?>